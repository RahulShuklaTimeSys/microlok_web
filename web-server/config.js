var config = {};
config.HTTP_PORT = process.env.HTTP_PORT || 8082;
config.HTTPS_PORT = process.env.HTTPS_PORT || 8443;
config.MAX_SESSION_COUNT = 10;
config.TITLE = 'Microlok';
config.CONFIG_SESSION_TIME_OUT = 30; // Minutes

//  variables for user data log.
config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME = "configParams.json";
module.exports = config;
