var exec = require('child_process').execFile;
var execCommand = require('child_process').exec;
var execSync = require('child_process').execSync;
var fs = require('fs');
var path = require('path');
var ss = require('socket.io-stream');
var server = require('./server');
var config = require('./config');
var scheduler;
//var ViproConfig = "/etc/vipro/ViProConfig.json"; // Board Environment.
var ViproConfig = "./static_data_json/ViProConfig.json"; // Development Environment.

var all_configuration_data = ViproConfig;
var configuration_data = fs.readFileSync(all_configuration_data);
var configuration_data_list = JSON.parse(configuration_data);

var db_operations = require("./utils/db_operations");
var sys_info_content = fs.readFileSync("./static_data_json/system_information.json");
var sys_info = JSON.parse(sys_info_content);

var bitstore_file_name =  "./static_data_json/bitstore.json";
var bit_store_content = fs.readFileSync(bitstore_file_name);
var bit_store = JSON.parse(bit_store_content);

var group_file_name = "./static_data_json/groupbits.json"
var group_bits_content = fs.readFileSync(group_file_name);
var group_bits = JSON.parse(group_bits_content);

var general_config_file_name =  "./static_data_json/general_config_list.json";
var general_config_content = fs.readFileSync(general_config_file_name);
var general_config = JSON.parse(general_config_content);

var timer_config_file_name =  "./static_data_json/timer_config_list.json";
var timer_config_content = fs.readFileSync(timer_config_file_name);
var timer_config = JSON.parse(timer_config_content);

var users_file_name = "./static_data_json/users.json";
var users_content = fs.readFileSync(users_file_name);
var users = JSON.parse(users_content);

function arrayElemMove(array, elem_index, location){
    var element = array[elem_index];
    array.splice(elem_index, 1);
    if ( location == 'up'){
        array.splice(elem_index - 1, 0, element);
    }
    else {
      array.splice(elem_index + 1, 0, element);
    }
}

exports = module.exports = function(io){
  io.sockets.on('connection', function (socket) {

    ss(socket).on('file-upload', function(stream, data) {
      var filename = path.basename(data.name);
      var baseDirectory = "./tmp/download/";
      if (!fs.existsSync(baseDirectory)){
        fs.mkdirSync(baseDirectory);
      }
      var destination = path.join(baseDirectory, filename);
      stream.pipe(fs.createWriteStream(destination));
      stream.on('error', function() {
        fs.unlink(destination);
      });
    });

    // event to delete cancelled file in conf_upload.
    socket.on('delete_cancelled_file', function(data){
      var filename = path.basename(data.name);
      var baseDirectory = "./tmp/download/";
      var destination = path.join(baseDirectory, filename);
      fs.unlink(destination);
   });

   socket.on('scheduler', function(){
     setInterval(function(){
       socket.emit('scheduler_info', { Board : configuration_data_list
       });
    }, 1000);
   });

   socket.on('scheduler_on_select', function(){
     socket.emit('scheduler_info_on_select', { Board : configuration_data_list
     });

   });


    socket.on('router', function(){
      exec('./system_bash/network_routing.sh', function(error, stdout, stderr) {
      // command output is in stdout
      socket.emit('router_info', { status: '0',
                                   route: stdout
                                 });
                             });
                        });

    // socket.on('check_tar_file', function(data){
    //   // console.log(data);
    //   // console.log(data.name);
    //   // console.log(data.name.split("\\")[2]);
    //   var cmdStr = './system_bash/tar_content.sh';
    //   exec(cmdStr, function(error, stdout, stderr) {
    //     if (error !== null) {
    //     console.log(error);
    //   }
    //   else{
    //     console.log(stdout);
    //   socket.emit('check_tar', { status: '0',result: stdout});
    //   }
    //   });
    // });

    socket.on('cmd', function(data){
    var cinput = data.message;
    execCommand(cinput , function(error, stdout, stderr) {
    if(error)
     socket.emit('err_cmd');
    else {
     socket.emit('router_info', { status: '0',
                                  route: stdout
                                });
          }
        });
      });

  socket.on('get_system_info', function(){
  var system_info_data = {
    "sysInfo" : {
            "Program" : configuration_data_list["MICROLOK_II_PROGRAM"],
            "ExecVer" : configuration_data_list["EXECUTIVE.VERSION"],
            "ExecCRC" : configuration_data_list["EXECUTIVE.CRC"],
            "CompVer" : configuration_data_list["COMPILER.VERSION"],
            "AppVersion" : configuration_data_list["APPLICATION.VERSION"],
            "AppCRC" : configuration_data_list["APPLICATION.CRC"],
            "SiteId" : configuration_data_list["SITE.ID.STRING"],
            "DateStamp" : configuration_data_list["DATE.STAMP"],
            "Maintenance-Tool-Version" : "",
            "PC-Date" : "",
          },
    "events" : []
  }
  socket.emit('system_info', { status: '0',
                               sysinfo : system_info_data
                              });
  });

  socket.on('disconnect', function(){
    console.log('Client disconnected');
  });

  // socket event to fetch eventlog from db.
    socket.on('get_system_logs', function(data){
    var recordsPerPage = data.recordsPerPage;
    var pageNumber = data.pageNumber;
    var offset = recordsPerPage * (pageNumber - 1);
    var totalRecordsQuery = "select count(*) Total from events;";
    db_operations.getEventlogs("SELECT id EventID, substr(strftime('%m/%d %H:%M:%S.%f', datetime(time, 'unixepoch')), 1, 16) EventTime, description Event, source Source, primaryid EventCode, type AS EventType FROM events ORDER BY id DESC LIMIT " + recordsPerPage + " OFFSET " + offset, totalRecordsQuery ).then(function(data){
        socket.emit('event_logs', { status: '1',
                                    eventLogs : data.queryResult,
                                    total : data.totalRecords[0].Total
                                  });
        });
    });

    // socket event to fetch eventlog from db.
      socket.on('get_system_error_logs', function(data){
      var recordsPerPage = data.recordsPerPage;
      var pageNumber = data.pageNumber;
      var offset = recordsPerPage * (pageNumber - 1);
      var totalRecordsQuery = "select count(*) Total from events where type = 2;"
      db_operations.getEventlogs("SELECT id EventID, substr(strftime('%m/%d %H:%M:%S.%f', datetime(time, 'unixepoch')), 1, 16) EventTime, description Event, source Source, primaryid EventCode, type AS EventType FROM events where type = 2 ORDER BY id DESC LIMIT " + recordsPerPage + " OFFSET " + offset, totalRecordsQuery).then(function(data){
          socket.emit('error_logs', { status: '1',
                                      eventLogs : data.queryResult,
                                      total : data.totalRecords[0].Total
                                    });
          });
      });

    //  event to get all logs from database for save full logs.
    socket.on('get_all_data', function(){
      db_operations.gatAllData().then(function(data){
        socket.emit('all_data', { status: '1',
                                    data : data ? data : ""
                                  });
        });
    });

    //  event to get all error logs from database for save full logs.
    socket.on('get_all_data_error_log', function(){
      db_operations.gatAllDataErrorLog().then(function(data){
        socket.emit('all_data', { status: '1',
                                    data : data ? data : ""
                                  });
        });
    });

    //  event to remove all logs from database.
    socket.on('remove_logs', function(){
      db_operations.deleteRecords().then(function(data){
        socket.emit('logs_removed', { status: '1',
                                    data : "Records deleted successfully."
                                  });
        });
    });

    // event to get data for user data log screen.
    socket.on('get_user_data_logs_list', function(callback){
        var objAllData;
        var baseDirectory = './tmp';
        var bitSettingsFileName = config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME;
        var data = { bitList : "", selectedBits : ""};
          data.bitList = configuration_data_list.Log;
          fs.readFile( baseDirectory + '/' + bitSettingsFileName, 'utf8', function (error, settingData) {
            if (error) {
              callback(undefined, data);
            }
            else {
              objAllData = JSON.parse(settingData);
              data.selectedBits = objAllData.hasOwnProperty("objUserDataLogConfig") ?  objAllData.objUserDataLogConfig : "";
              callback(undefined, data);
            }
          })
    });

    // event to get data for general configuration screen.
    socket.on('get_general_config_list', function(callback){
        var objAllData;
        var baseDirectory = './tmp';
        var settingObjectFileName = config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME;
        var data = { configData : "", configSettings : "", last_vital_configuration : "", last_non_vital_configuration : ""};
          // data.configData = configuration_data_list["CONFIGURATION.SYSTEM"];
          data.configData = configuration_data_list["VitalProcessorConfiguration"];
          fs.readFile( baseDirectory + '/' + settingObjectFileName, 'utf8', function (error, settingData) {
            if (error) {
            	callback(undefined, data);
            }
            else {
              objAllData = JSON.parse(settingData);
              data.configSettings = objAllData.hasOwnProperty("objGeneralConfig") ?  objAllData.objGeneralConfig : "";
            	callback(undefined, data);
          }
        });
    });

    // event to get data for timer configuration screen.
    socket.on('get_timer_config_list', function(callback){
        var objAllData;
        var baseDirectory = './tmp';
        var settingObjectFileName = config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME;
        var data = { configData : "", configSettings : ""};
          data.configData = configuration_data_list['TIMER.BITS'];
          fs.readFile( baseDirectory + '/' + settingObjectFileName, 'utf8', function (error, settingData) {
            if (error) {
              callback(undefined, data);
            }
            else {
              objAllData = JSON.parse(settingData);
              data.configSettings = objAllData.hasOwnProperty("objTimerConfig") ?  objAllData.objTimerConfig : "";
              callback(undefined, data);
          }
        });
    });

    // event to get data for serial ports.
    socket.on('get-serial-port-data', function(callback){
        var objAllData;
        var baseDirectory = './tmp';
        var settingObjectFileName = config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME;
        var data = { configData : "", configSettings : ""};
          data.configData = configuration_data_list['SerialPorts'];
          fs.readFile( baseDirectory + '/' + settingObjectFileName, 'utf8', function (error, settingData) {
            if (error) {
              callback(undefined, data);
            }
            else {
              objAllData = JSON.parse(settingData);
              data.configSettings = objAllData.hasOwnProperty("objSerialPortConfig") ?  objAllData.objSerialPortConfig : "";
              callback(undefined, data);
          }
        });
    });

    // event to get all data for submit screen.
    socket.on('get_all_config_list', function(callback){
        var objAllGeneralData;
        var objAllTimerData;
        var objAllUserDataLogData;
        var objAllBoardData;
        var objCPUCommData;
        var AllBoardData = [];
        var CpuBoardData = [];
        var AllBoardDataDict = {}
        var objAllData = configuration_data_list;
        var baseDirectory = './tmp';
        var settingObjectFileName = config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME;
        var generaldata = { configData : "", configSettings : "", last_vital_configuration : "", last_non_vital_configuration : ""};
        var userdatalog = { bitList : "", selectedBits : ""};
        var timerdata = { configData : "", configSettings : ""};
        // var cpucommdata = { configData : "", configSettings : ""};
          generaldata.configData = configuration_data_list["VitalProcessorConfiguration"];
          userdatalog.configData = configuration_data_list.Log;
          timerdata.configData = configuration_data_list['TIMER.BITS'];
          Object.keys(objAllData.VitalProcessorConfiguration).forEach(function(key){
            if (key.indexOf("Board.") > -1) {
              AllBoardData.push({key : key, value : configuration_data_list.VitalProcessorConfiguration[key]})
            }
          });
          Object.keys(objAllData.VitalProcessorConfiguration).forEach(function(key){
            if (key.indexOf("Board.09.CPU_BASE") > -1) {
              CpuBoardData.push({key : key, value : configuration_data_list.VitalProcessorConfiguration[key]})
            }
          });
          fs.readFile( baseDirectory + '/' + settingObjectFileName, 'utf8', function (error, settingData) {
            if (error) {
              console.log(CpuBoardData);
              callback(undefined, generaldata,userdatalog,timerdata,AllBoardData,CpuBoardData);
            }
            else {
              objAllGeneralData = JSON.parse(settingData);
              objAllTimerData = JSON.parse(settingData);
              objAllUserDataLogData = JSON.parse(settingData);
              objAllBoardDataNew = JSON.parse(settingData);
              objCPUCommData = JSON.parse(settingData);
              generaldata.configSettings = objAllGeneralData.hasOwnProperty("objGeneralConfig") ?  objAllGeneralData.objGeneralConfig : "";
              userdatalog.configSettings = objAllUserDataLogData.hasOwnProperty("objUserDataLog") ?  objAllUserDataLogData.objUserDataLog : "";
              timerdata.configSettings = objAllTimerData.hasOwnProperty("objTimerConfig") ?  objAllTimerData.objTimerConfig : "";
              AllBoardDataDict = objAllBoardDataNew.hasOwnProperty("objBoardConfig") ?  objAllBoardDataNew.objBoardConfig : "";
              cpucommdata = objCPUCommData.hasOwnProperty("objCPUComm") ?  objCPUCommData.objCPUComm : "";              
              callback(undefined, generaldata,userdatalog,timerdata,AllBoardDataDict,cpucommdata);
          }
        });
    });

    socket.on('get-board-conf-data', function(){
      var baseDirectory = './tmp';
      var AllBoardData = []
      var objAllData = configuration_data_list;
      var settingObjectFileName = config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME;
      var AllBoardDataDict = {};
      // var cpucommdata = { configData : "", configSettings : ""};
      Object.keys(objAllData.VitalProcessorConfiguration).forEach(function(key){
        if (key.indexOf("Board.") > -1) {
          // AllBoardData.push({key : key, value : configuration_data_list[key]})
          AllBoardData.push({key : key, value : configuration_data_list.VitalProcessorConfiguration[key]})
        }
      });
      fs.readFile( baseDirectory + '/' + settingObjectFileName, 'utf8', function (error, settingData) {
        if (error) {
          if(AllBoardData.length > 0){
            socket.emit('board-conf-data',{status : "ok", data : AllBoardData});
          }
          else {
            socket.emit('board-conf-data',{status : "error", data : "No data found for board configuration."})
          }
        }
        else {
          AllBoardDataNew = JSON.parse(settingData);
          objCPUCommData = JSON.parse(settingData);
          AllBoardDataDict = AllBoardDataNew.hasOwnProperty("objBoardConfig") ?  AllBoardDataNew.objBoardConfig : "";
          cpucommdata = objCPUCommData.hasOwnProperty("objCPUComm") ?  objCPUCommData.objCPUComm : "";
          socket.emit('board-conf-data',{status : "ok", new_data : AllBoardDataDict,data : AllBoardData,cpu_data:cpucommdata});
      }
    });
    });

    // event to get all data for submit screen.
    socket.on('ntpserver', function(callback){
      var message = configuration_data_list["NTP"];
      callback(undefined, message);
    });
    socket.on('timezone', function(callback){
      var message = configuration_data_list["TimeZone"];
      callback(undefined, message);
    });

    socket.on('get-snmp-conf-data', function(callback){
      if (configuration_data_list.hasOwnProperty("SNMP")) {
        var snmp_data = configuration_data_list.SNMP;
        callback({status : "ok", data : snmp_data});
      }
      else {
        callback({status : "error", data : "No data found for snmp configuration."})
      }
    });

    socket.on('get-link-conf-data', function(){
      sendLinkData();
      if (scheduler !== undefined)
        clearInterval(scheduler);
      scheduler = setInterval(function(){
        sendLinkData();
       }, 1000);
    });

    function sendLinkData(){
      var AllLinkData = []
      var objAllData = configuration_data_list;
      // Object.keys(objAllData).forEach(function(key){
      Object.keys(objAllData.VitalProcessorConfiguration).forEach(function(key){
        if (key.indexOf("Link.") > -1) {
          AllLinkData.push({key : key, value : configuration_data_list.VitalProcessorConfiguration[key]})
        }
      });
      if(AllLinkData.length > 0){
        socket.emit('link-conf-data',{status : "ok", data : AllLinkData});
      }
      else {
        socket.emit('link-conf-data',{status : "error", data : "No data found for link configuration."})
      }
    }

    socket.on('get-communication-conf-data', function(){
      var AllLinkData = []
      var objAllData = configuration_data_list;
      Object.keys(objAllData.VitalProcessorConfiguration).forEach(function(key){
        if (key.indexOf("Link.") > -1) {
          AllLinkData.push({key : key, value : configuration_data_list.VitalProcessorConfiguration[key]})
        }
      });
      if(AllLinkData.length > 0){
        socket.emit('communication-conf-data',{status : "ok", data : AllLinkData});
      }
      else {
        socket.emit('communication-conf-data',{status : "error", data : "No data found for link configuration."})
      }
    });



    // event to clear configuration data.
    socket.on('clear_configurations', function(callback){
      var configurationFile = config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME;
      var filename = path.basename(configurationFile);
      var baseDirectory = './tmp';
      var destination = path.join(baseDirectory, filename);
        fs.unlink(destination, function(err){
          if(err)
            callback(err.message);
          else {
            callback(undefined);
          }
        });
    });

    socket.on('validate_current_password', function(currentPass, newPass, callback){
      if(server.user.pass == currentPass && users.hasOwnProperty(currentPass)){
        if(users.hasOwnProperty(newPass))
          callback(false);
        else
          callback(true);
      }
      else {
        callback(false);
      }
    });

    socket.on('change_password', function(currentPass, newPass, callback){
      if(server.user.pass == currentPass && users.hasOwnProperty(currentPass)){
        delete users[currentPass];
        users[newPass]= { 'pass' : newPass };
        var strObjUsers = JSON.stringify(users);
        fs.writeFile('./static_data_json/users.json', strObjUsers, function (err) {
          if (err) {
            callback(err.message);
          }
          else {
            callback(undefined);
          }
        });
      }
      else{
        callback("FAILURE: Current password is not accepted.");
      }
    });

    socket.on('validate_symbol_group_exist', function(name, callback){
      var index = 0;
      if(group_bits.groups.length>=10){
        callback(true);
      }
      else{
        for(index=0;index<group_bits.groups.length;index++){
          if(group_bits.groups[index].name == name) {
            callback(true);
          }
        }
        callback(false);
      }
      });

    // socket event to save local storage changes to exit confirmation json file.
    socket.on('save_local_storage_data', function(jsonData, callback){
      var baseDirectory = './tmp';
      var bitSettingsFileName = config.EXIT_CONFIRMATION_SAVED_DATA_FILE_NAME;
      var objAllData;
        fs.readFile( baseDirectory + '/' + bitSettingsFileName, 'utf8', function (error, allData) {
          if (error) {
            fs.writeFile(baseDirectory + '/' + bitSettingsFileName, jsonData, function (err) {
              if (err) {
                callback(err);
              }
              else {
                callback(undefined);
              }
            });
          }
          else {
            objAllData = JSON.parse(allData);
            objToBeSave = JSON.parse(jsonData);
            Object.keys(objToBeSave).forEach(function(key) {
              objAllData[key] = objToBeSave[key];
            });
            objAllData = JSON.stringify(objAllData);
            fs.writeFile(baseDirectory + '/' + bitSettingsFileName, objAllData, function (err) {
              if (err) {
                callback(err);
              }
              else {
                callback(undefined);
              }
            });
          }
      });
    });

    // event to get all the available json fles to download.
      socket.on('get_json_file_list', function(callback){
      var error = false;
      var fileList = [];
      var baseDirectory = "./tmp/download/";
      var exists = fs.existsSync(baseDirectory);
      fs.readdir(baseDirectory, function(err, items) {
        try{
          if(exists && items.length == 0){
            error = 'No component exist to download.'
          }
          else if(exists && items.length > 0){
            // here we are making assumption that we will have only one file for each formate --> .tar, .mlp, & .json
            for (var i = 0; i < items.length; i++){
              if (items[i].substring(items[i].lastIndexOf('.') + 1).toLowerCase() == 'json'){
                fileList.push({"value" : items[i], "text" : "Configuration Download"})
              }
            }
          }
        }
        catch(err){
          console.log(err);
          error = 'Board generated error.'
        }
        callback(error, JSON.stringify(fileList));
      });
    });

    // event to get all the available tar fles to download.
      socket.on('get_tar_file_list', function(callback){
      var error = false;
      var fileList = [];
      var baseDirectory = "./tmp/download/";
      var exists = fs.existsSync(baseDirectory);
      fs.readdir(baseDirectory, function(err, items) {
        try{
          if(exists && items.length == 0){
            error = 'No component exist to download.'
          }
          else if(exists && items.length > 0){
            // here we are making assumption that we will have only one file for each formate --> .tar, .mlp, & .json
            for (var i = 0; i < items.length; i++){
              if (items[i].substring(items[i].lastIndexOf('.') + 1).toLowerCase() == 'tar'){
                fileList.push({"value" : items[i], "text" : "Configuration Download"})
              }
            }
          }
        }
        catch(err){
          console.log(err);
          error = 'Board generated error.'
        }
        callback(error, JSON.stringify(fileList));
      });
    });

    // catch the time set button and set the system time;
    socket.on('timeset', function(bdata) {
        var cmd = 'date ' + bdata.message;
        // console.log(cmd);
        var child = exec(cmd, function (error, stdout, stderr) {
            try {
                var result = stdout;
                delete result;
                delete cmd;

                execSync('/usr/bin/rtc set');
            }
            catch (e) {
                console.log(e);
            }
        });
        delete child;

    });

    // event for download application functionality.
    ss(socket).on('file_download', function (stream, name, callback) {
      var baseDirectory = "./tmp/download/";
      var exists = fs.existsSync(baseDirectory + name.trim());
      var error = false;
      var stats;
      // check if file exist or not
      if(!exists){
        error = "requested file does not exist.";
      }
      else{
        stats = fs.statSync(baseDirectory + name.trim());
      }
      callback(error, {
          name : name,
          size : exists ? stats["size"] : 0
      });
      var MyFileStream = fs.createReadStream(baseDirectory + name.trim());
      MyFileStream.pipe(stream);
  });

  // socket event to fetch board details
    socket.on('get_board_names', function(){
      socket.emit('get_board_names_response', { Board : configuration_data_list
      });
    });

    socket.on('get_board_names_for_file', function(){
      socket.emit('get_board_names_response_for_file', { Board : configuration_data_list
      });
    });

    socket.on('init-var-monitor', function(){
      console.log("Got Event init-var-monitor===========");
     socket.emit('init-var-monitor-response', { status: '0',
                                                // Bit : bit_store,
                                                Bit : configuration_data_list.Log,
                                                Group : group_bits,
                                                descr : "Board Error : Data Fetching error"
                                              });
     });
  var myVar;
  function get_bit_status(bit_id){
    //var bitStore = bit_store.bits;
    var bitStore = configuration_data_list.Log;
    //var set_bits = configuration_data_list.set_bits;
    var set_bits = bit_store.set_bits;
    //var unset_bits = configuration_data_list.unset_bits;
    var unset_bits = bit_store.unset_bits;
    //var clear_bits = configuration_data_list.clear_bits;
    var clear_bits = bit_store.clear_bits;
    var bitID = parseInt(bit_id);
      if (unset_bits.indexOf(bitID) != -1){
          console.log(" I am UNSET BIT");
        return { 'msg' : 0 , 'txt' : bitStore[bit_id -1 ].Bits + " is UNSET \n"}; // bit is unset
      }
      else {
        if(set_bits.indexOf(bitID) != -1){
          console.log(" I am SET BIT");
          return { 'msg' : 1 , 'txt' : bitStore[bit_id -1 ].Bits + " is SET \n"}; // bit is SET
          } else {
          console.log(" I am CLEAR BIT");
          return { 'msg' : 2 , 'txt' : bitStore[bit_id -1 ].Bits + " is CLEAR \n"};  // bit is CLEAR
        }
      }
    }

    socket.on('bit_status' , function(bit_info){
      console.log(" Received bit Id to add " + bit_info.bit_id);
      socket.emit('received_bit_status' , get_bit_status(bit_info.bit_id));
    });

      socket.on('get-varmonitor', function(){
        function monitor_stream(){
          var date = new Date();
          var timestamp = date.getSeconds();
          socket.emit('received_mon_data', {'timestamp' : timestamp });
        }
        myVar = setInterval(monitor_stream, 1000);
      });

      socket.on('stop-varmonitor', function(){
        console.log("Clearing the myVar");
        clearInterval(myVar);
      });

      function get_msg_info(grp_id){
        var bit_arr = group_bits.groups[grp_id].bitarray;
        var set_bits = bit_store.set_bits;
        var unset_bits = bit_store.unset_bits;
        var clear_bits = bit_store.clear_bits;
        //var set_bits = configuration_data_list.set_bits;
        //var unset_bits = configuration_data_list.unset_bits;
        //var clear_bits = configuration_data_list.clear_bits;
        var msg_list = [];
        for(var i=0; i < bit_arr.length; i++){
          if (unset_bits.indexOf(bit_arr[i]) != -1){
            msg_list.push(0); // bit is unset
          }
          else {
            if(set_bits.indexOf(bit_arr[i]) != -1){
              msg_list.push(1);  // bit is SET
            } else {
              msg_list.push(2);  // bit is CLEAR
            }
          }
        }
        return msg_list;
      }

      socket.on('all_bit_status' , function (data){
        if (data.oper == "ALL"){
            group_bits.groups[data.grp_id].bitarray = group_bits.groups[data.grp_id].bitarray.concat(group_bits.groups[data.grp_id].inactive_bits);
          group_bits.groups[data.grp_id].inactive_bits.splice(0,group_bits.groups[data.grp_id].inactive_bits.length);
          group_bits.groups[data.grp_id].msg = get_msg_info(data.grp_id);
          socket.emit('all_received_bit_status', { 'msg' : group_bits.groups[data.grp_id].msg });
        } else {
          group_bits.groups[data.grp_id].inactive_bits = group_bits.groups[data.grp_id].bitarray.concat(group_bits.groups[data.grp_id].inactive_bits);
    			group_bits.groups[data.grp_id].bitarray.splice(0,group_bits.groups[data.grp_id].bitarray.length);
    			group_bits.groups[data.grp_id].msg.splice(0,group_bits.groups[data.grp_id].msg.length);
        }
        bits_updated();
      });

      socket.on('edit-bitgroup', function(mod_group_info){
        var grp_id = mod_group_info.group_index;
        group_bits.groups[grp_id].bitarray = mod_group_info.active_bits;
        group_bits.groups[grp_id].inactive_bits = mod_group_info.inactive_bits;
        if (mod_group_info.msg){
          group_bits.groups[grp_id].msg = mod_group_info.msg;
        }
        bits_updated();
        });

      socket.on('add-bitgroup', function(new_group_info){
        group_bits.groups.push(new_group_info.new_grp);
        bits_updated();
       });

       socket.on('remove-bitgroup', function(group_info){
         group_bits.groups.splice(group_info.group_index, 1);
         bits_updated();
       });

    socket.on('visible-bitgroup', function(group_info){
      group_bits.groups[group_info.group_index].visible = group_info.visible;
      bits_updated();
    });

    socket.on('expire-logged-in-session', function(callback){
      try {
        // socket.handshake.session.user_mode = "view_mode";
        server.edit_mode = '0';
        socket.handshake.session.vopstatus = 'CPS Up';
        socket.handshake.session.enableSessionTimeout = 'false';
        if(server.user)
          delete server.user;
        server.enableSessionTimeout = socket.handshake.session.enableSessionTimeout;
        socket.handshake.session.save();
        callback({status : '1'});
      }
      catch(error) {
        callback({status : "0"});
      }
    });

    function bits_updated()
    {
      fs.writeFile(group_file_name, JSON.stringify(group_bits), function(err) {
       if(err) {
      console.log(err);
      }
      /*var group_bits_content = fs.readFileSync(group_file_name);
      //group_bits = JSON.parse(group_bits_content);
      */
     });
    //group_bits = JSON.parse(fs.readFileSync(group_file_name, 'utf8'));
    }
 });
}
