var express = require('express');
var session = require('express-session');
var authenticate = require('./../lib/login').authenticate;
var router = express.Router();
var util = require('util');
var FileStore = require('session-file-store')(session);
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var config = require('./../config');
var path = require('path');
var fs = require('fs')
var rootDirectory = path.resolve('.');
var Q = require('q');
var loggedinSession;
var routerList = [];
var configFile = './../static_data_json/ViProConfig.json';

localSessionList = [];
var filestore_sessions_list = [];
var sess_options = {
    path: path.join(require('os').tmpdir(), 'sessions'),
    reapAsync : false,
    logFn : function(){},
    reapInterval : 10,
    ttl : 60 * config.CONFIG_SESSION_TIME_OUT
}

function requestRestart () {
    setTimeout(function() {
        execSync('touch /tmp/bootweb');
    }, 7000);
}

function genEvent (primaryid, secid, message) {
    var cmd = UIev.cmd + " "
        + UIev.source + " "
        + primaryid + " "
        + secid + " "
        + UIev.level + " "
        + UIev.errtype + " "
        + message;

    var stat = execSync(cmd);
    // console.log(cmd);

    delete stat;
    delete cmd;
}
// Function to set the Time Zone string for TZ
function setZone (index) {
    var tzHash = {};

    tzHash['-12']  = '<WAKT>-12';
    tzHash['-11']  = 'SST11';
    tzHash['-10']  = 'HST10';
    tzHash['-9']   = '<AKST>9<AKDT>,M4.1.0,M10.5.0';
    tzHash['-8']   = 'PST8PDT,M4.1.0,M10.5.0';
    tzHash['-7']   = 'MST7MDT,M4.1.0,M10.5.0';
    tzHash['-6']   = 'CST6CDT,M4.1.0,M10.5.0/3';
    tzHash['-5']   = 'EST5EDT,M4.1.0,M10.5.0';
    tzHash['-4']   = 'AST4ADT,M4.1.0,M10.5.0';
    tzHash['-3.5'] = 'NST3:30NDT,M4.1.0/0:01,M10.5.0/0:01';
    tzHash['-3']   = 'BRT3<BRST>,M10.3.0/0,M2.3.0/0';
    tzHash['-2']   = 'AST4ADT,M4.1.0,M10.5.0';
    tzHash['-1']   = '<AZOT>1<AZOST>,M3.5.0/0,M10.5.0/0';
    tzHash['0']    = 'GMT0';
    tzHash['1']    = 'CET-1<CEST>,M3.5.0,M10.5.0';
    tzHash['2']    = '<SAST>-2';
    tzHash['3']    = 'AST-3ADT,J91/3,J274/4';
    tzHash['3.5']  = 'AST-3:30';
    tzHash['4']    = 'GST-4';
    tzHash['4.5']  = 'AFT-4:30';
    tzHash['5']    = 'PKT-5';
    tzHash['5.5']  = 'IST-5:30';
    tzHash['5.45'] = 'KMD-5:45';
    tzHash['6']    = 'BDT-6';
    tzHash['7']    = 'ICT-7';
    tzHash['8']    = 'WST-8';
    tzHash['9']    = 'JST-9';
    tzHash['9.5']  = 'CST-9:30';
    tzHash['10']   = 'EST-10EST,M10.5.0,M3.5.0/3';
    tzHash['11']   = '<MAGT>-11<MAGST>,M3.5.0,M10.5.0/3';
    tzHash['12']   = '<NZST>-12<NZDT>,M10.1.0,M3.3.0/3';
    tzHash['13']   = 'TOT-13';

    // console.log('TZ environment variable is ' + tzHash[index]);
    return tzHash[index];
}
// End of Function to set the Time Zone string for TZ

// Function to get the Time Zone for the GUI
function getZone (index) {
    var tzHash = {};

    tzHash['-12']  = '(GMT -12:00) Eniwetok';
    tzHash['-11']  = '(GMT -11:00) Midway Island';
    tzHash['-10']  = '(GMT -10:00) Hawaii';
    tzHash['-9']   = '(GMT -9:00) Alaska';
    tzHash['-8']   = '(GMT -8:00) Pacific Time';
    tzHash['-7']   = '(GMT -7:00) Mountain Time';
    tzHash['-6']   = '(GMT -6:00) Central Time';
    tzHash['-5']   = '(GMT -5:00) Eastern Time';
    tzHash['-4']   = '(GMT -4:00) Atlantic Time';
    tzHash['-3.5'] = '(GMT -3:30) Newfoundland';
    tzHash['-3']   = '(GMT -3:00) Brazil';
    tzHash['-2']   = '(GMT -2:00) Mid-Atlantic';
    tzHash['-1']   = '(GMT -1:00) Azores';
    tzHash['0']    = '(GMT) Western Europe Time';
    tzHash['1']    = '(GMT +1:00) Brussels';
    tzHash['2']    = '(GMT +2:00) Kalingrad';
    tzHash['3']    = '(GMT +3:00) Baghdad';
    tzHash['3.5']  = '(GMT +3:30) Tehran';
    tzHash['4']    = '(GMT +4:00) Abu Dhabi';
    tzHash['4.5']  = '(GMT +4:30) Kabul';
    tzHash['5']    = '(GMT +5:00) Ekaterinburg';
    tzHash['5.5']  = '(GMT +5:30) Bombay';
    tzHash['5.45'] = '(GMT +5:45) Kathmandu';
    tzHash['6']    = '(GMT +6:00) Almaty';
    tzHash['7']    = '(GMT +7:00) Bangkok';
    tzHash['8']    = '(GMT +8:00) Beijing';
    tzHash['9']    = '(GMT +9:00) Tokyo';
    tzHash['9.5']  = '(GMT +9:30) Adelaide';
    tzHash['10']   = '(GMT +10:00) Eastern Australia';
    tzHash['11']   = '(GMT +11:00) Magadan';
    tzHash['12']   = '(GMT +12:00) Auckland';
    tzHash['13']   = '(GMT +13:00) Nuku\'alofa';

    // console.log('TZ environment variable is ' + tzHash[index]);
    return tzHash[index];
}

// End of Function to get the Time Zone for the GUI

// -------------- Function to sync localSessionList with file store sessions. ---------------------
function getFileStoreSessionList(callback){
	var deferred = Q.defer();
      file_store.list(function (a, session_list) {
	    filestore_sessions_list = session_list;
      callback(filestore_sessions_list);
      deferred.resolve("resolve");
	});
	return deferred.promise;
}
function refresh_localSessionList(session_list){
	for (var i=0; i < localSessionList.length; i++){
		if(session_list.indexOf(localSessionList[i] +'.json') < 0){
		   localSessionList.pop(localSessionList[i]);
       i--;
		}
	}
}
// -------------- END of Function to sync localSessionList with file store sessions. ---------------

var file_store = new FileStore(sess_options);

  var sessionMiddleware = session({
  	store : file_store,
  	resave : false,
  	saveUninitialized : true,
  	secret : 'asts123',
    rolling : true,
    cookie : {	httpOnly: false,
  	secure : false,
  	}
  });

router.use(sessionMiddleware);

function getAllRouteList(){
  var list = [];
  router.stack.forEach(function(Layer){
    if(Layer.route)
    list.push(Layer.route.path);
  });
  return list;
}

router.all('*', function(req, res, next){
  if (req.secure) {
      return next();
  };
  res.redirect('https://'+req.hostname+':'+ config.HTTPS_PORT +req.url );
});

    router.use(function(req, res, next) {
  if(req.session){
    req.session.cookie.expires = false;
    req.session.cookie.maxAge = 1000 * 60 * config.CONFIG_SESSION_TIME_OUT + 10000; // + 10 seconds
	if (typeof req.session.user_mode === 'undefined') {
		req.session.user_mode = 'view_mode';
		res.app.locals.page_nav = 1;
		req.session.vopstatus = 'CPS Up';
		req.session.enableSessionTimeout = 'false';
	}
	else {
            res.app.locals.page_nav = req.session.user_mode == 'view_mode' ? '1' : '2';
  }
  res.app.locals.enableSessionTimeout = req.session.enableSessionTimeout;

	// ----------------- Check for session limit ----------------------------
	getFileStoreSessionList(refresh_localSessionList).then(function(){
    if(loggedinSession && localSessionList.indexOf(loggedinSession) < 0){
      res.app.locals.edit_mode = '0';
      res.app.locals.enableSessionTimeout;
      delete res.app.locals.user;
      loggedinSession = undefined;
    }
		if (localSessionList.length >= parseInt(config.MAX_SESSION_COUNT)){
		   if (localSessionList.indexOf(req.session.id) > -1){
			      res.app.locals.page_nav = req.session.user_mode == 'view_mode' ? '1' : '2';
		    }
		   else {
    			res.app.locals.page_nav = 9;
          if(routerList.indexOf(req.url)> 0) {
            res.sendFile(rootDirectory +'/views/block.html');
            return;
          }
		    }
		}
		else if(localSessionList.length < parseInt(config.MAX_SESSION_COUNT)){
			       res.app.locals.page_nav = req.session.user_mode == 'view_mode' ? '1' : '2';
		        if(localSessionList.indexOf(req.session.id) < 0){
				localSessionList.push(req.session.id);
			}
		}
        next();
	});
  // ----------------- END of Check for session limit ----------------------

	}
    });

    router.get('/', function (req, res) {
      req.session.save();
      res.app.locals.page_load = '1';
      res.render('index');
    });

    router.post('/look-accept', urlencodedParser, function(req, res){
      if (!req.body) return res.sendStatus(400)
        var status = '0';
        var result = '1';
        var data = { 'status_code': status, 'result': result  };
        res.send(data);
    });

    router.post('/wait-confirm', urlencodedParser, function(req, res){
      if (!req.body) return res.sendStatus(400)
        var status = '0';
        req.session.vopstatus = 'Reset Wait';
        var data = { 'status_code': status };
        res.send(data);
    });

    router.post('/NTP', function(req, res) {
        // console.log(request.body);

        // Read the config file and update the values
        fs.readFile(configFile, 'utf8', function (err,thedata) {
            if (err) {
                return console.log(err);
            }

            data = JSON.parse(thedata);

            try {
                if (data.time_source != req.body.Time_Type) {
                    msg = "\'NTP time source changed from "+data.time_source+" to "+req.body.Time_Type+".\'";
                    // genEvent (16, 26, msg);
                    console.log(msg);
                }
                data.time_source = req.body.Time_Type;

                if (data.time_source == 'Manual') {
                    data.time_zone = 0; // set to GMT
                }
                else {
                    if (data.ntp_server != req.body.NTP_Address) {
                        msg = "\'NTP server changed from "+data.ntp_server+" to "+req.body.NTP_Address+".\'";
                        // genEvent (16, 27, msg);
                        console.log(msg);
                    }
                    data.ntp_server = req.body.NTP_Address;

                    if (data.time_zone != req.body.TimeZone) {
                        msg = "\'NTP time zone from "+getZone(data.time_zone)+" to "+getZone(req.body.TimeZone)+".\'";
                        genEvent (16, 28, msg);
                    }
                    data.time_zone = req.body.TimeZone;
                }

                process.env.TZ = setZone(data.time_zone);

                // console.log(data);
                fs.writeFile(configFile, JSON.stringify(data));
                var serverList = data.ntp_server.split(',');
                var len = serverList.length;
                var cmdStr = '../setntp.sh ' + data.time_source;
                while (len--)
                    cmdStr += ' ' + serverList[len];

                // console.log(cmdStr);
                var child = exec(cmdStr, function (error, stdout, stderr) {
                    try {
                        var result = stdout;
                        //console.log(result);
                        delete result;
                    }
                    catch (e) {
                        console.log(e);
                    }
                });

                cmdStr = '../settz.sh ' + setZone(data.time_zone);

                var child = exec(cmdStr, function (error, stdout, stderr) {
                    try {
                        var result = stdout;
                        //console.log(result);
                        delete result;
                    }
                    catch (e) {
                        console.log(e);
                    }
                });
            }
            catch (e) {
                console.log(e);
            }
        });

        //NEW SESSION QUICKFIX
        //request.session.userLevel = 2;
        requestRestart();
    });

    router.post('/login', urlencodedParser, function(req, res){
      // debugger;
      var status = '';
      if (!req.body) return res.sendStatus(400)
      authenticate(req.body.password , function(err, user){
      if (user) {
        if (typeof res.app.locals.user === 'undefined'){
						// NEW USER LOGIN -- NEW SESSION
            //req.session.cookie.maxAge = config.CONFIG_SESSION_TIME_OUT;
            //req.session.user = user;
            res.app.locals.user = user;
            req.session.user_mode = "edit_config";
            res.app.locals.edit_mode = '1';
            res.app.locals.page_nav = '2';
            req.session.enableSessionTimeout = 'true';
            res.app.locals.enableSessionTimeout = req.session.enableSessionTimeout;
            loggedinSession = req.session.id;
            status = '0';
            var data = { 'status_code': status };
            res.send(data);
           }
          else {
             // USER ALREADY LOGGED IN -- SESSION ACTIVE
             status = '2';
             var data = { 'status_code' : status };
             res.send(data);
           }
        } else {
           // PASSWORD ERROR
         status = '3';
         var data = { 'status_code': status };
         res.send(data);
          }
        });
        // }
      });
/* system-info AJAX call needs to Go  As these values are now sent on socket but can not remove it as sysinfo file is still using it*/
	 router.get('/system-info',function (req, res) {
     fs.readFile(path.join(rootDirectory, 'tmp/swversion'), 'utf8', function(err, data) {
       if (err) throw err;
       var SW_Version = data;
       var data = { 'SWVer': SW_Version,
                    'vopstatus': req.session.vopstatus,
                    'appname': 'DigiSafe_Integration_Test'
                  };
       res.send(data);
     });
	 });

  router.get('/reset-mlk',function (req, res) {
    var reset_id = '0';
    var cps_status = '';
    var data = { 'status_code': '0',
                 'reset_id': reset_id ,
                 'cps_status' : cps_status
               };
    res.send(data);
  });

  router.post('/reset-mlk',function (req, res) {
    var data = { 'status_code': '0',
                 'reason': "Successful"
               };
    res.send(data);
  });

  router.get('/apply-cfg',function (req, res) {
    var data = { 'status_code': '0'
               };
    res.send(data);
  });

  router.get('/discard-cfg',function (req, res) {
    var data = { 'status_code': '0'
               };
    res.send(data);
  });

    router.get('/linkinfo', function (req, res) {
      res.render('linkinfo');
    });

    router.get('/boardinfo', function (req, res) {
      res.app.locals.page_nav = '3';
      res.render('boardinfo');
    });

    router.get('/sysinfo', function (req, res) {
      res.render('sysinfo');
    });

    router.get('/messagemon', function (req, res) {
      res.render('messagemon');
    });

    router.get('/bitandvars', function (req, res) {
      res.render('bitandvars');
    });

    router.get('/userdatalog', function (req, res) {
      res.render('userdatalog');
    });

    router.get('/eventlog', function (req, res) {
      res.render('eventlog');
    });

    router.get('/errorlog', function (req, res) {
      res.render('errorlog');
    });

    router.get('/systemlog', function (req, res) {
      res.render('systemlog');
    });

    router.get('/systime', function (req, res) {
      res.render('systime');
    });

    router.get('/memorydump', function (req, res) {
      res.render('memorydump');
    });
    router.get('/application_download', function (req, res) {
      res.render('application_download');
    });
    router.get('/conf_download', function (req, res) {
      res.render('conf_download');
    });

    router.get('/conf_general', function (req, res) {
      res.app.locals.page_nav = '3';
      res.render('conf_general');
    });

    router.get('/conf_links', function (req, res) {
      res.app.locals.page_nav = '3';
      res.render('conf_links');
    });

    router.get('/conf_route', function (req, res) {
      res.app.locals.page_nav = '3';
      res.render('conf_route');
    });

    router.get('/conf_timers', function (req, res) {
      res.app.locals.page_nav = '3';
      res.render('conf_timers');
    });

    router.get('/conf_userdatalog', function (req, res) {
      res.app.locals.page_nav = '3';
      res.render('conf_userdatalog');
    });

    router.get('/conf_variables', function (req, res) {
      res.app.locals.page_nav = '3';
      res.render('conf_variables');
    });

    router.get('/conf_boards', function (req, res) {
      res.app.locals.page_nav = '3';
      res.render('conf_boards');
    });

    router.get('/conf_upload', function (req, res) {
      res.render('conf_upload');
    });

    router.get('/config', function (req, res) {
      res.render('config');
    });
    router.get('/submit_data', function (req, res) {
      res.render('submit_data');
    });

    (function(){
      routerList = getAllRouteList();
    })();

module.exports = {
  router : router,
  sessionMiddleware : sessionMiddleware
}
