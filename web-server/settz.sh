#!/bin/bash

if [ $# -lt 1 ]; then 
   echo "Usage: $0 <Timezone_String>"
   exit 1
fi

echo $1 > /etc/TZ
TZ=`cat /etc/TZ`
export TZ

