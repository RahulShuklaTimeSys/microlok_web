#!/bin/bash

if [ $# -lt 1 ]; then 
   echo "Usage: $0 <TimeSource Manual/NTP> <SERVER> ... <SERVER>"
   exit 1
fi

if [ $1 == "Manual" ]; then
    /etc/init.d/ntpd stop
    exit 0
fi

if [ $1 == "NTP" ]; then
    echo "rm /files/etc/ntp.conf/server" > nn
    for var in "$@"
    do
	if [ $var != "NTP" ]; then
            echo "ins server after /files/etc/ntp.conf/#comment[9]" >> nn
            echo "set /files/etc/ntp.conf/server[1] $var" >> nn 
	fi
    done
    echo "save" >> nn 

    augtool -f nn
    /etc/init.d/ntpd restart
fi

