'use strict';
var fs = require('fs');
var express = require('express');
var socketIO = require('socket.io');
var sharedsession = require("express-socket.io-session");
var config = require('./config');
var http = require("http");
var https = require('https');
var routes = require('./routes/routes');
var logger = require("./utils/logger");
var database = require("./utils/db_operations");
logger.debug("Overriding 'Express' logger");
var app = express();
app.use(require('morgan')('combined',{"stream": logger.stream }));
app.use('/', routes.router);
app.set('view engine', 'ejs');
app.set('view options', { layout: false });
app.use('/public', express.static('public'));
app.locals = {
              title : config.TITLE,
              edit_mode : '1',
              page_nav : '1',
              vopstatus : 'CPS Up',
              enableSessionTimeout : 'false',
              session_limit_reached : 'false'
            };
module.exports = app.locals;
var secureServer = https.createServer({
  key: fs.readFileSync('./certificates/key.pem'),
  cert: fs.readFileSync('./certificates/cert.pem')
}, app).listen(config.HTTPS_PORT , function () {
    console.log('Secure Server listening on port ' + config.HTTPS_PORT);
});
var insecureServer = http.createServer(app).listen(config.HTTP_PORT, function() {
  console.log('Insecure Server listening on port ' + config.HTTP_PORT);
});
var io = socketIO(secureServer);

io.use(sharedsession(routes.sessionMiddleware, {
    autoSave:true
}));

var socket_events = require('./socket_events')(io);
setInterval (function(){
  io.emit('time', new Date());
}, 1000);
module.exports = app;
