/* Setup actions for submit buttons */
(function() {
  var socket = io();
  var file_list = new Array();
  var IsUploadRunning = false;
  var confirmPopup = new Popup('upload-confirmation-popup');
  var confirmMsg2 = '';
	confirmMsg2 += '	<div class="field-group clearfix">';
	confirmMsg2 += '		<div class="field-container text clearfix">';
	confirmMsg2 += '		    <label for="signin-description">You need to confirm presence at Microlok System.</label>';
	confirmMsg2 += '		    <label for="signin-description">Press the ENTER button on the front panel to begin upload process:</label>';
	confirmMsg2 += '	    </div>';
	confirmMsg2 += '		<div class="field-container text clearfix" style="text-align: center;">';
	confirmMsg2 += '		    <label for="signin-description">CNFG</label>';
	confirmMsg2 += '		    <label for="signin-description">MODE</label>';
	confirmMsg2 += '	    </div>';
	confirmMsg2 += '	</div>';
	confirmMsg2 += '<div class="clearfix" id="login-loading-info" style="text-align:center">';
	confirmMsg2 += '	<img src="public/img/ajax-load.gif" width="16" height="16" />Waiting...';
	confirmMsg2 += '</div>';

  var confirmMsgLoad = '';
	confirmMsgLoad += '	<div class="clearfix" id="login-loading-info" style="text-align:center">';
	confirmMsgLoad += '		<img src="public/img/ajax-load.gif" width="16" height="16" /> Please wait...';
	confirmMsgLoad += '	</div>';

  var confirmMsgError = '';
	confirmMsgError += '<div class="field-group clearfix" id="login-panel">';
	confirmMsgError += '	<div class="field-container text clearfix" id="password_failure">';
	confirmMsgError += '		<label style="color: #AA3333;">FAILURE: Connection or server error. Refresh the page and try again.</label>';
	confirmMsgError += '	</div>';
	confirmMsgError += '</div>';

	var statusPopup = new Popup('upload-status');
  statusPopup.hide();

	function showError(message) {
	    statusPopup.show({
	            title : 'Error',
	            message : message,
	            buttons : [ {
	                title : 'Dismiss',
	            action : function() {
                window.onbeforeunload = null;
                location.reload();
	                        }
	                    } ]
	                });
	}

	function checkFileTypes(fileinput, mask) {
		if (!fileinput || fileinput.blank()) {
 			return false;
 		}
 		var ext = fileinput.substring(fileinput.lastIndexOf('.'));
 		ext = ext.toLowerCase();
 		mask = mask.toLowerCase();
 		if (ext != mask) {
			return false;
		}
 		return true;
	}

  function lookForConfigStatus() {
    return SendConfigurationFiles();
  }

  function lookForAcceptButton() {
    // TODO: AJAX for button state periodically
    new Ajax.Request('/look-accept', {
      method: 'post',
      onSuccess : function(transport) {
        /*resultErrorCode*/
        var status = transport.responseJSON.status_code;
        if (status == "0") {
          var result = transport.responseJSON.result;
          if (result == "1") {
            confirmPopup.show({
                  title : 'Presence confirmation',
                  message : confirmMsgLoad,
                  buttons : [ ]
                });
            lookForConfigStatus.delay(2);
          } else {
            lookForAcceptButton.delay(2);
          }
        } else {
          lookForAcceptButton.delay(2);
        }
      },
      onFailure : function() {
        lookForAcceptButton.delay(2);
      }
    });
  }

  function lookForWaitStatus() {
    confirmPopup.show({
          title : 'Presence confirmation',
          message : confirmMsg2,
          buttons : [ ]
        });
    lookForAcceptButton.delay(2);

    }

  function showWaitConfirmation() {
    confirmPopup.show({
          title : 'Presence confirmation',
          message : confirmMsgLoad,
          buttons : [ ]
        });
    var nocache = new Date().getTime();
    new Ajax.Request('/wait-confirm', {
      method: 'post',
      parameters: {
        'nocache': nocache
      },
      onSuccess : function(transport) {
        var status = transport.responseJSON.status_code;
        if (status == "0") {
          lookForWaitStatus.delay(2);
        } else {
          confirmPopup.show({
            title : 'FAILURE:',
            message : confirmMsgError,
            buttons : [ {
              title : 'Close'
            } ]
          });
        }
      },
      onFailure : function() {
        confirmPopup.show({
          title : 'FAILURE:',
          message : confirmMsgError,
          buttons : [ {
            title : 'Close'
          } ]
        });
      }
    });
  }

	var UploadAll = function(event) {
		event.stop();
    var ExecFile = $F('exec-filename');
    var ConfFile = $F('conf-filename');
    if ( !ExecFile && !ConfFile )
		{
			showError('No files selected. Please select at least one file.');
			return;
		}
    var uploadExec = checkFileTypes(ExecFile, '.tar');
    if ( ExecFile ){
    if(!uploadExec){
         showError('Executive File Type is not correct. Please choose .tar extension');
            return;
          }
  //   socket.emit('check_tar_file', { name : ExecFile});
  //   socket.on('check_tar', function(result){
  //     debugger;
  //     if(result.status == '0'){
  //       alert("Correct");
  //   }
  //   else{
  //     alert("InCorrect")
  //   }
  // });
  }
    var uploadConf = checkFileTypes(ConfFile, '.json');
    if ( ConfFile ){
    if(!uploadConf){
      showError('Configuration File Type is not correct. Please choose .json extension');
        return;
      }
      }
    showWaitConfirmation();
		// return SendConfigurationFiles();
	}

	var UploadNew = function(event) {
		event.stop();
    var ExecFile = $F('exec-filename');
    var ConfFile = $F('conf-filename');
    if ( !ExecFile && !ConfFile )
		{
			showError('No files selected. Please select at least one file.');
			return;
		}
    var uploadExec = checkFileTypes(ExecFile, '.tar');
    if ( ExecFile ){
    if(!uploadExec){
         showError('Executive File Type is not correct. Please choose .tar extension');
            return;
          }
    }
    var uploadConf = checkFileTypes(ConfFile, '.json');
    if ( ConfFile ){
    if(!uploadConf){
      showError('Configuration File Type is not correct. Please choose .json extension');
        return;
      }
      }
    showWaitConfirmation();
		// return SendConfigurationFiles();
	}

	var UploadAllFiles = function (file_list, list_index) {
	    //file_list.forEach(function(file_name) {
	      try {
          var file_name = file_list[list_index];
	        var stream = ss.createStream();
	        var UploadFile = document.getElementById(file_name).files[0];
	        ss(socket).emit('file-upload', stream, {name: UploadFile.name , size: UploadFile.size});
	        var blobStream = ss.createBlobReadStream(UploadFile);
	        var size = 0;
          showUploadProgressPopUp(UploadFile.name, stream, file_list, list_index);
          IsUploadRunning = true;
	        blobStream.on('data', function(chunk) {
	        size += chunk.length;
    		  var progress = Math.floor(size / UploadFile.size * 100);
          updateProgress(progress);
        	 });

        	blobStream.pipe(stream);

        	blobStream.on('end', function() {
            IsUploadRunning = false;
            list_index++;
            if(list_index < file_list.length){
              UploadAllFiles(file_list, list_index);
            }
            else {
              statusPopup.show({
                    title   : 'Uploaded Success',
                    message : ('<p style="text-align:center">'
                              + 'Files Uploaded successfully. Board will Reboot Now.</p>'),
                    buttons : [ {
                                    title : 'Continue',
                                    action : function() {
                                      alert("Board will Reboot..Add Reboot functionality");
                                    }
                                },
                                {
                                  title : 'Discard',
                                  action : function() {
                                    window.onbeforeunload = null;
                                    location.reload();
                                  }
                                }
                              ]
              });
            }
          });

        	blobStream.on('error', function() {
          IsUploadRunning = false;
          blobStream.end();
        	statusPopup.show({
                    title : 'Board Generated Errors Encountered',
                    message : '<p style="text-align:center">' + progressMessage + '</p>',
                    buttons : [ {
                        title : 'Dismiss',
                        action : function() {
                        location.reload();
                        window.onbeforeunload = null;
                    }
                  } ]
              });
          });
      }
  catch(err) {
    window.onbeforeunload = null;
    showError('File uploading error. Please retry.');
  }
//});
}

socket.on('disconnect', function(){
  if(IsUploadRunning){
  statusPopup.hide();
  statusPopup.show({
            title : 'Network Error',
            width : '370px',
            message : '<p style="text-align:center">The server is taking too long to respond or something is wrong with your internet connection. Please try again later.</p>',
            buttons : [ {
                title : 'OK',
                action : function() {
                statusPopup.hide();
                location.reload();
            }
          } ]
      });
    }
  });

	var SendConfigurationFiles = function() {
	    var ExecFile = $F('exec-filename');
	    var ConfFile = $F('conf-filename');
	    var file_list = new Array();
		if ( ExecFile )
		{
      file_list.push('exec-filename');
		}
		if ( ConfFile )
		{
	      file_list.push('conf-filename');
	  }
  UploadAllFiles(file_list, 0);

  }

  function showUploadProgressPopUp(fileName, stream, file_list, list_index){
    statusPopup.show({
          title : 'Please Wait .. Uploading ' + fileName,
          message : ('<p style="text-align:center">'
              + '<div id="myProgress"><div id="myBar"></div></div></p>'
              + '<p style="margin-top:5px" id="demo">0%</p>'),
             buttons : [{
                     title : 'Cancel',
                     action : function() {
                       stream.destroy();
                       IsUploadRunning = false;
                       socket.emit('delete_cancelled_file', { name : fileName});
                       list_index++;
                       if(list_index < file_list.length){
                         UploadAllFiles(file_list, list_index);
                       }
                       else{
                         statusPopup.hide();
                         confirmPopup.hide();
                      }
                   }
             }]
           });
  }

  function updateProgress(progress){
    document.getElementById('myBar').style.width = progress + "%";
    document.getElementById("demo").innerHTML = progress  + '%';
  }

document.observe("dom:loaded", function() {
  var element = document.getElementById("smlist");
	element.classList.add("active");
  ClearExecForm();
  // ClearAppForm();
  ClearConfigForm();
  var edit_mode = location.search.split('edit_mode=')[1];
  if (!$('edit_mode')){
    statusPopup.show({
          title : 'Authenticated User Error',
          message : '<p style="text-align:center">' + 'This feature is only available to Authenticated User. Please login to Upload Software.' + '</p>',
          buttons : [ {
            title : 'Dismiss',
            action : function() {
              window.location.href = '/';
              window.onbeforeunload = null;
            }
          } ]
        });
  return;
  }

  function ClearExecForm() {
		$('params-exec').innerHTML = '<input type="file" id="exec-filename" name="datafile" accept = ".tar" class="fchooser-file">';
		stylizeUpload($('exec-filename'));
	}

  // function ClearAppForm() {
	// 	$('params-app').innerHTML = '<input type="file" id="app-filename" name="datafile" accept = ".mlp" class="fchooser-file">';
	// 	stylizeUpload($('app-filename'));
	// }
  function ClearConfigForm() {
		$('params-conf').innerHTML = '<input type="file" id="conf-filename" name="datafile" accept = ".json" class="fchooser-file">';
		stylizeUpload($('conf-filename'));
	}


	if ($('upload_new_btn')) {
		$('upload_new_btn').observe('click', UploadNew);
	}

  	if ($('upload_all_btn')) {
		$('upload_all_btn').observe('click', UploadAll);
	}

	if ($('clear-exec-filename')) {
		$('clear-exec-filename').observe('click', ClearExecForm);
	}
  //
	// if ($('clear-app-filename')) {
	// 	$('clear-app-filename').observe('click', ClearAppForm);
	// }

	if ($('clear-conf-filename')) {
		$('clear-conf-filename').observe('click', ClearConfigForm);
	}
  });
})();
