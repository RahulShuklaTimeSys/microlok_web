var socket = io();
var last_transport = null;
var sortcolumn = 5; /* 1 - time, 2 - event, 3 - source, 4 - code, 5 - number */
var sortdirect = 1; /* 1 - asc, -1 - desc */
var messagePopup = new Popup('message');
var perpage = 25;
var page = 1;
var maxpage = 1;
var totalcount = 0;
var pageDataToSave = [];
var saveAsComressed = false;
var system_Info;
messagePopup.hide();

$("chooseperpage").value = 25;
$("choosepage").value = 1;
messagePopup.hide();

// method to refresh event log data.
function refreshData(){
  var _recordsPerPage = $("chooseperpage").value;
  var _pageNumber = $("choosepage").value;
  socket.emit('get_system_error_logs', { recordsPerPage: _recordsPerPage, pageNumber : _pageNumber });
}

function updateArrow() {
		var uptext = (sortdirect > 0) ? "" : "up";
		var arrow = '<img src="public/img/arrow' + uptext + '.png">'
		for (var i = 1; i <=5; i++) {
			var elem = $('sortflag_palceholder_'+i);
			if (i == sortcolumn) {
				elem.innerHTML = arrow;
			} else {
				elem.innerHTML = "";
			}
		}
	}

	function setSort(colnum) {
		if (sortcolumn == colnum) {
			sortdirect *= -1;
		} else {
			sortcolumn = colnum;
			sortdirect = 1;
		}
		bindData(last_transport);
		updateArrow();
	}

  function checkPerPage() {
		var new_perpage = $('chooseperpage').value;
		var min_val = $('chooseperpage').readAttribute("min");
		//var max_val = $('chooseperpage').readAttribute("max");
    var max_val = 100;
		var intperPage = parseInt(new_perpage);
		if ( (intperPage.toString() != new_perpage) || intperPage < min_val || intperPage > max_val) {
			ShowAlert ("Events per page should be integer value in range " + min_val + " to " + max_val);
			$('chooseperpage').value = perpage;
			return false;
		}
		perpage = intperPage;
		if ((page-1)*perpage >= totalcount) {
			page = Math.ceil(totalcount/perpage);
			$('choosepage').value = page;
		}
		return true;
	}

	function checkPage() {
		var new_page = $('choosepage').value;
		var intPage = parseInt(new_page);
		if ( (intPage.toString() != new_page) || intPage < 1 || intPage > maxpage) {
			ShowAlert ("Page should be integer value in range 1 to " + maxpage);
			$('choosepage').value = page;
			return false;
		}
		page = intPage;
		return true;
	}

  socket.on('system_info', function(systemInfo) {
      system_Info = systemInfo;
  });

//listen to socket events.
socket.on('error_logs', function(event_logs_data) {
    event_logs_data.eventLogs = JSON.parse(event_logs_data.eventLogs);
    for(var i = 0; i < event_logs_data.eventLogs.length; i++){
      event_logs_data.eventLogs[i].Index = perpage * (page - 1) + (i + 1);
    }
    last_transport = event_logs_data;
    totalcount = event_logs_data.total;
    maxpage = Math.ceil(totalcount/perpage);
    $('choosepage').setAttribute("max",maxpage);
    $('total-page-num').innerHTML = maxpage;
    if( $('choosepage').value > maxpage ){
      $('choosepage').value = maxpage;
      page = maxpage;
      refreshData();
    }
    bindData(event_logs_data);
});

socket.on('all_data', function(result) {
  if(result.data.length <= 0 || JSON.parse(result.data).length <= 0){
    ShowAlert('No data to save. Try again after receving data.');
  }
  else {
    var txtString = '';
    txtString +=  'Error Log \n' +
                  'Program: ' + system_Info.sysinfo.sysInfo.Program + '  \n'+
                  'Executive Version: ' + system_Info.sysinfo.sysInfo.ExecVer + ' \n'+
                  'Executive CRC: ' + system_Info.sysinfo.sysInfo.ExecCRC + '\n'+
                  'Compiler Version: ' + system_Info.sysinfo.sysInfo.CompVer + '\n'+
                  'Application Version: ' + system_Info.sysinfo.sysInfo.AppVersion + '\n'+
                  'Application CRC: ' + system_Info.sysinfo.sysInfo.AppCRC + '\n'+
                  'Site ID: ' + system_Info.sysinfo.sysInfo.SiteId + '\n'+
                  'Date Stamp: ' + system_Info.sysinfo.sysInfo.DateStamp + '\n'+
                  'Maintenance Tool Version: ' + $('tool-ver').innerText +' \n'+
                  'PC Date: ' + getDateString(new Date()) +' \n \n';

    var data = JSON.parse(result.data);
    data.forEach(function(obj){
      txtString += '[' +  'error' + '] ' + ((parseInt(obj.EventTime.split(' ')[1].split(':')[0]) > 12) ? (obj.EventTime + ' PM') : (obj.EventTime +' AM')) + ' ' + obj.Event + ' ' + obj.EventCode + '\n';
    });
    if(saveAsComressed){
      saveAsComressed = false;
      DownloadCompressedData(txtString, 'error.txt', 'application/zip')
    }
    else{
      DownloadData(txtString, 'error.txt', 'application/text');
    }
  }
});

function bindData(event_logs_data) {
      drawTable(event_logs_data.eventLogs);
}

function drawTable(data) {
  var table = $("error-log-tbody-1");
  while(table.rows.length > 0) {
  table.deleteRow(0);
  }
  /* add sorting by sortcolumn and sortdirect */
          data.sort(function(a,b) {
          var item1 = a[Object.keys(a)[sortcolumn]];
          var item2 = b[Object.keys(b)[sortcolumn]];
          var num1 = parseInt(a[Object.keys(a)[6]]);
          var num2 = parseInt(b[Object.keys(b)[6]]);
          if (sortcolumn == 5) {
            item1 = num1;
            item2 = num2;
          }
          if (item1 > item2) return sortdirect;
          if (item1 < item2) return -1*sortdirect;
          return 0;
        });
  pageDataToSave = (JSON.parse(JSON.stringify(data))); // to clone the object.
  for (var i = 0; i < data.length; i++) {
      drawRow(data[i]);
  }
}

function drawRow(rowData) {
      var image_src = "./public/img/exclamation.png";

      var table = document.getElementById("error-log-tbody-1");
      var row = table.insertRow();
      var cell;

      cell = row.insertCell(0);25
      cell.innerHTML= rowData.EventID;
      cell.className = 'hide';

      cell = row.insertCell(1);
      cell.innerHTML= '<img class="event-log-icon" src="'+ image_src +'" alt="">';
      cell.className = 'logpage-img';

      cell = row.insertCell(2);
      cell.innerHTML= rowData.Index;
      cell.className = 'logpage-num';

      cell = row.insertCell(3);
      cell.innerHTML= ((parseInt(rowData.EventTime.split(' ')[1].split(':')[0]) > 12) ? (rowData.EventTime + ' PM') : (rowData.EventTime +' AM'));
      cell.className = 'logpage-time';

      cell = row.insertCell(4);
      cell.innerHTML= rowData.Event;
      cell.className = 'logpage-event';

      cell = row.insertCell(5);
      cell.innerHTML= rowData.Source;
      cell.className = 'logpage-source';

      cell = row.insertCell(6);
      cell.innerHTML= rowData.EventCode;
      cell.className = 'logpage-codes';
}

function getDateString(date) {
  return (date.getMonth()+1) + "/" +
      date.getDate() + "/" +
      date.getFullYear() + " " +
      (date.getHours()<10?"0":"") + date.getHours() + ":" +
      (date.getMinutes()<10?"0":"") + date.getMinutes() + ":" +
      (date.getSeconds()<10?"0":"") + date.getSeconds();
}

(function() {
  var questionPopup = new Popup('question-popup');
  questionPopup.hide();

  var informationPopup = new Popup('information-popup');
  informationPopup.hide();
	document.observe("dom:loaded", function() {
    var element = document.getElementById("sdlist");
  	element.classList.add("active");
    refreshData();
    socket.emit('get_system_info');
    $('sort_num').observe('click',function() {
			setSort(5);
		});
		$('sort_time').observe('click',function() {
			setSort(1);
		});
		$('sort_event').observe('click',function() {
			setSort(2);
		});
		$('sort_source').observe('click',function() {
			setSort(3);
		});
		$('sort_codes').observe('click',function() {
			setSort(4);
		});
    $('chooseperpage').observe('change',function() {
			if (checkPerPage()) {
				  refreshData();
			}
		});
    $('info_refresh_btn').observe('click',function() {
			refreshData();
		});

		$('choosepage').observe('change',function() {
			if (checkPage()) {
				  refreshData();
			}
		});
    $('eventlog_first_btn').observe('click', function() {
			if (page > 1) {
				page = 1;
				$('choosepage').value = page;
				refreshData();
			}
		});
		$('eventlog_previous_btn').observe('click', function() {
			if (page > 1) {
				page = page - 1;
				$('choosepage').value = page;
				refreshData();
			}
		});
		$('eventlog_next_btn').observe('click', function() {
			if (page < (Math.ceil(totalcount/perpage))) {
				page = page + 1;
				$('choosepage').value = page;
				refreshData();
			}
		});
		$('eventlog_last_btn').observe('click', function() {
			if (page < (Math.ceil(totalcount/perpage))) {
				page = Math.ceil(totalcount/perpage);
				$('choosepage').value = page;
				refreshData();
			}
		});

    $('error-log-tbody-1').observe('click', function(e) {
      e = e || window.event;
      var EventID;
      var EventCode;
      var EventInfo;
      var target = e.srcElement || e.target;
      while (target && target.nodeName !== "TR") {
          target = target.parentNode;
      }
      if (target) {
          var cells = target.getElementsByTagName("td");
          EventID = cells[0].innerHTML;
          EventCode = cells[6].innerHTML;
          EventInfo = cells[4].innerHTML;
      }
      informationPopup.show({
          title : 'Help for code :  ' + EventCode,
          message : '<div class="information-popup-innercontainer">' + EventInfo + '</div>',
          width: '600px',
          buttons : [ {
            title : 'Close'
          }]
      });
    });

    $('info_save_btn').observe('click', function() {
      if(pageDataToSave.length <= 0){
        ShowAlert('No data to save. Try again after receving data.');
      }
      else {
        for(var i=0; i < pageDataToSave.length; i++){
          delete pageDataToSave[i].EventID;
          delete pageDataToSave[i].Index;
        }
        var txtString = '';
        txtString +=  'Event Log \n' +
                      'Program: ' + system_Info.sysinfo.sysInfo.Program + '  \n'+
                      'Executive Version: ' + system_Info.sysinfo.sysInfo.ExecVer + ' \n'+
                      'Executive CRC: ' + system_Info.sysinfo.sysInfo.ExecCRC + '\n'+
                      'Compiler Version: ' + system_Info.sysinfo.sysInfo.CompVer + '\n'+
                      'Application Version: ' + system_Info.sysinfo.sysInfo.AppVersion + '\n'+
                      'Application CRC: ' + system_Info.sysinfo.sysInfo.AppCRC + '\n'+
                      'Site ID: ' + system_Info.sysinfo.sysInfo.SiteId + '\n'+
                      'Date Stamp: ' + system_Info.sysinfo.sysInfo.DateStamp + '\n'+
                      'Maintenance Tool Version: ' + $('tool-ver').innerText +' \n'+
                      'PC Date: ' + getDateString(new Date()) +' \n \n';

        pageDataToSave.forEach(function(obj){
          txtString += '[' +  'error' + '] ' + ((parseInt(obj.EventTime.split(' ')[1].split(':')[0]) > 12) ? (obj.EventTime + ' PM') : (obj.EventTime +' AM')) + ' ' + obj.Event + ' ' + obj.EventCode + '\n';
        });
        DownloadData(txtString, 'error.txt', 'application/text');
      }
		});

    $('info_saveall_btn').observe('click', function() {
      socket.emit('get_all_data_error_log');
    });

    $('info_savecompressed_btn').observe('click', function() {
      saveAsComressed = true;
      socket.emit('get_all_data_error_log');
    });

	});
})();
