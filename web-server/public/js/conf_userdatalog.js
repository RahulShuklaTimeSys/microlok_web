var checkboxIDList = [];
var last_transport;
var socket = io();

(function() {
  // Wait until dom loded

  function BindData(){
    socket.emit('get_user_data_logs_list', function(error, data) {

      if(error){
        ShowAlert("Board generated error.");
      }
      else{
        var CheckedItems = [];
        var CheckedNames = [];
        var CheckedNamesDict = {};
        var bits = {};
        last_transport = data.bitList;
        var objUserDataLogConfig = GetFromLocalStorage('objUserDataLogConfig')? GetFromLocalStorage('objUserDataLogConfig') : (data.selectedBits != "" ? data.selectedBits : false);
        var checkedItemList = data.selectedBits;
        for(var i =0;i< data.bitList.length;i++){
          checkboxIDList.push('check' + (i+1));
          var container = document.getElementById('options_list');
          var node = document.createElement('div');
          // if(window.localStorage['objUserDataLogConfig.objUserDataLog']){
          //   checkedItemList = JSON.parse(window.localStorage['objUserDataLogConfig.objUserDataLog']);
          // }
          if (objUserDataLogConfig){
            checkedItemList = JSON.parse(objUserDataLogConfig.objUserDataLog);
          }
          var checked = checkedItemList != "" ? (parseInt(i + 1) in checkedItemList ? "checked" : "") : "";
          node.id = "div" + (i + 1);
          node.classList.add("user-data-log-dynamic-checkbox-wrapper");
          if(data.bitList[i].hasOwnProperty('Bits')){
            node.innerHTML = '<input type="checkbox" id="check' + (i + 1) + '" name="check' + data.bitList[i].Bits + '" style="margin-right:5px;margin-bottom:2px;"' + checked + '><label style="font-size:12px;font-weight:100;"for="check' + (i+1) + '">'+ data.bitList[i].Bits +'</label>';
            container.appendChild(node);
            document.getElementById('check' + (i + 1) + '').disabled = document.getElementById('conf_save_changes_btn') ? false : true;
            CheckedNamesDict[i+1]= data.bitList[i].Bits
            var checkbits = document.getElementById('check' + (i+1));
            checkbits.addEventListener("change", function(e){
              checkboxIDList.forEach(function(id){
                if (document.getElementById(id) && document.getElementById(id).checked){
                  CheckedItems.push(id.replace("check", ""));
                  // CheckedNames.push(data.bitList[id].Bits);
                }
              });
              // var objTimerConfig = CheckedItems;
              if(CheckedItems){
                CheckedItems.forEach(function(id){
                  bits[id] = "1";
                })
                var formmodified="1";
                // SaveToLocalStorage('objUserDataLog', JSON.stringify(bits));
                var objUserDataLogConfig = {"objUserDataLog" : JSON.stringify(bits), "CheckedNamesDict":CheckedNamesDict, "type":"non-vital", "name":"User Data Log Bit Selection", "formmodified":formmodified};
                // window.localStorage.setItem('objUserDataLogConfig', objUserDataLogConfig);
                SaveToLocalStorage("objUserDataLogConfig", objUserDataLogConfig);
                CheckedItems.length=0;
                bits = {};
              }
            });
          }
        }
      }
    });
  }

  function FilterData(){
    var userInput = document.getElementById('start_adr').value.trim().toLowerCase();
    last_transport.map(function (obj) {
      if(obj.Bits.toLowerCase().indexOf(userInput) < 0)
          document.getElementById("div" + (last_transport.indexOf(obj) + 1)).style.display = 'none';
      else
          document.getElementById("div" + (last_transport.indexOf(obj) + 1)).style.display = 'block';
    });
  }

  document.observe("dom:loaded", function() {
    var element = document.getElementById("aclist");
  	element.classList.add("active");
    // RemoveFromLocalStorage('objUserDataLog');
    BindData();
    var checkbutton = document.getElementById('conf_save_changes_btn') ? false : true;
    if(checkbutton){
    }
    else{
      $('conf_save_changes_btn').observe('click',function(){
        document.location = "submit_data";
      });
    }
      $('search-btn').observe('click',function(){
      FilterData();
    });
  });
})();
