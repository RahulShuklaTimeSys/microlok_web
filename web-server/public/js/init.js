var socket = io();
/* Add trim() for IE8*/
if(typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function() {
		return this.replace(/^\s+|\s+$/g, '');
	}
}
/* Setup reset popup */
(function setupResetPopup() {
	if (!$('reset_menu_item')) {
		return;
	}
	var resetPopup = new Popup('reset-popup');
	var reset_html = '';
	reset_html += '<div class="field-group clearfix">';
	reset_html += '	<div class="field-container text clearfix">';
	reset_html += '		<label>Are you sure you want to reset the MicroLok system?</label>';
	reset_html += '	</div>';
	reset_html += '</div>';
	reset_html += '<div class="clearfix" id="reset-loading-info" style="text-align:center">';
	reset_html += '	<img src="public/img/ajax-load.gif" width="16" height="16" /> Please wait...';
	reset_html += '</div>';

	var error_html_1 = '';
	var error_html_2 = '';
	error_html_1 += '<div class="field-group clearfix">';
	error_html_1 += '	<div class="field-container text clearfix">';
	error_html_1 += '		<label>';
	error_html_2 += '		</label>';
	error_html_2 += '	</div>';
	error_html_2 += '</div>';

	var cps_html = '';
	cps_html += '<div class="field-group clearfix">';
	cps_html += '	<div class="field-container text clearfix">';
	cps_html += '		<label>Do you want to reset the CPS?</label>';
	cps_html += '	</div>';
	cps_html += '</div>';

	function doReset() {
		function getResetState() {
			var nocache = new Date().getTime();
			new Ajax.Request('/reset-mlk',{
					    method: 'get',
						parameters: {
							'nocache': nocache
						},
						onSuccess : function(transport) {
							if (transport.responseJSON != null) {
								/* respose format
									<response>
										<status>0</status>
										<id>9a3f700b359c28f5769dbaab04e1b3c3</id>
										<vopstatus>CPS Down</vopstatus>
									</response>
									OR
									<response>
										<status>-1</status>
										<reason>reason text</reason>
									</response>
								*/
								var status = transport.responseJSON.status_code;
								if (status == 0) {
									var resetid = transport.responseJSON.reset_id;
									var cps_status = transport.responseJSON.cps_status;
									resetPopup.show({
										title : 'System Reset',
										message : reset_html,
										buttons : [ {
											title : 'No'
										}, {
											title : 'Yes, Reset System',
											action : function () {
														var reset_cps = 0;
														function submitReset() {
															var nocache = new Date().getTime();
															new Ajax.Request('/reset-mlk',{
																		method: 'post',
																		parameters: {
																			id: resetid,
																			cps: reset_cps,
																			nocache: nocache
																		},
																		onSuccess: function(transport){
																			if (transport.responseJSON != null) {
																				/* respose format
																					<response>
																						<status>0</status>
																						<id>9a3f700b359c28f5769dbaab04e1b3c3</id>
																						<vopstatus>CPS Down</vopstatus>
																					</response>
																					OR
																					<response>
																						<status>-1</status>
																						<reason>reason text</reason>
																					</response>
																				*/
																				var status = transport.responseJSON.status_code;
																				if (status == 0) {
																					location.reload();
																				} else {
																					var reason = transport.responseJSON.reason;
																					resetPopup.show({
																						title : 'Reset Error',
																						message : error_html_1 + reason + error_html_2,
																						buttons : [ {
																							title : 'Ok'
																						} ]
																					});
																				}
																			} else {
																				alert('Reset error, try again');
																			}
																	    },
																		onFailure: function(transport){
																			alert('Reset error, try again');
																	    }
																	});
														}

														if (cps_status != 'CPS Up') {
															//ask if cps is 0 or 1;
															resetPopup.show({
																title : 'CPS Reset',
																message : cps_html,
																buttons : [ {
																	title : 'No',
																	action : function () {
																				submitReset();
																			}
																}, {
																	title : 'Yes, Reset CPS',
																	action : function () {
																				reset_cps = 1;
																				submitReset();
																			}
																} ]
															});
														} else if (cps_status == 'CPS Up') {
															submitReset();
														} else {
															// TODO: process error if it is possible to have any other cps state here
														}
													}
										} ]
									});

									$('reset-loading-info').hide();
									Popup.frobIEElements();
								} else {
									var reason = transport.responseJSON.reason;
									resetPopup.show({
										title : 'Reset Error',
										message : error_html_1 + reason + error_html_2,
										buttons : [ {
											title : 'Ok'
										} ]
									});
								}
							} else {
								//TODO: process error
							}
						},
						onFailure : function() {
							//TODO: process error
						}
					});
			resetPopup.close();
		}
		var lAdjustmentMenu = $('adjustment_menu');
		var lAdjustmentPanel = $('adjustment_panel');
		var lAdjustmentLi = $('adjustment_li');
		var lAdjustmentArrow = $('adjustment_arrow');
		if (lAdjustmentPanel != null) {
			lAdjustmentPanel[lAdjustmentPanel.visible() ? 'blindUp' : 'blindDown']({
				duration : 0.1
			});
		};

		getResetState();
	}
	$('reset_menu_item').observe('click', doReset);

})();

/* Setup login popup */
(function setupLoginPopup() {
	if (!$('setup_menu')) {
		return;
	}
	var pwdtmp = "";

	var loginPopup = new Popup('login-popup');
	var html = '';
	html += '<div class="field-group clearfix" id="login-panel">';
	html += '	<div class="field-container text clearfix" style="display: none;" id="password_failure">';
	html += '		<label style="color: #AA3333;">FAILURE: Connection or server error. Refresh the page and try again.</label>';
	html += '	</div>';
	html += '	<div class="field-container text clearfix" style="display: none;" id="another_session">';
	html += '		<label style="color: #AA3333;">FAILURE: Another user authenticated. Please try again later.</label>';
	html += '	</div>';
	html += '	<div class="field-container text clearfix" style="display: none;" id="password_alert">';
	html += '		<label style="color: #AA3333;">FAILURE: Password not accepted. Please try again.</label>';
	html += '	</div>';
	html += '	<div class="field-container radio clearfix">';
	// html += '		<label for="no_login_true">Examine settings / Upload Software';
	// html += '			<input id="no_login_true" class="cfgproc fix-ibox-padding" type="radio" checked="" value="0" name="no_login">';
	// html += '		</label>';
	html += '		<label for="no_login_false">Modify vital and non-vital settings';
	// html += '			<input id="no_login_false" class="cfgproc fix-ibox-padding" type="radio" value="1" name="no_login">';
	html += '		</label>';
	html += '	</div>';
	html += '	<div class="field-container text clearfix">';
	html += '		<label for="signin-password">Password:</label>';
	html += '		<div id="password_container" class="iw">';
	html += '			<input value="" class=""  type="password" id="signin-password" name="password">';
	html += '		</div>';
	html += '	</div>';
	html += '</div>';
	html += '<div class="clearfix" id="login-loading-info" style="text-align:center">';
	html += '	<img src="public/img/ajax-load.gif" width="16" height="16" /> Please wait...';
	html += '</div>';

	var loginPopup2 = new Popup('login-err-popup');
	var authErrMsg = '';
	authErrMsg += ' <div id="login-error">Another user authenticated.</div>';
	authErrMsg += '	<div class="field-group clearfix">';
	authErrMsg += '		<div class="field-container text clearfix">';
	authErrMsg += '		    <label for="signin-description">Do you want to logout current user?</label>';
	authErrMsg += '	    </div>';
	authErrMsg += '	</div>';

	var confirmPopup = new Popup('login-confirmation-popup');
	var confirmMsg = '';
	confirmMsg += '	<div class="field-group clearfix">';
	confirmMsg += '		<div class="field-container text clearfix">';
	confirmMsg += '		    <label for="signin-description">You need to confirm presence at Microlok System</label>';
	confirmMsg += '	    </div>';
	confirmMsg += '	</div>';

	var confirmMsg2 = '';
	confirmMsg2 += '	<div class="field-group clearfix">';
	confirmMsg2 += '		<div class="field-container text clearfix">';
	confirmMsg2 += '		    <label for="signin-description">You need to confirm presence at Microlok System.</label>';
	confirmMsg2 += '		    <label for="signin-description">Toggle ACCEPT switch on front panel and wait for front panel to display:</label>';
	confirmMsg2 += '	    </div>';
	confirmMsg2 += '		<div class="field-container text clearfix" style="text-align: center;">';
	confirmMsg2 += '		    <label for="signin-description">CNFG</label>';
	confirmMsg2 += '		    <label for="signin-description">MODE</label>';
	confirmMsg2 += '	    </div>';
	confirmMsg2 += '	</div>';
	confirmMsg2 += '<div class="clearfix" id="login-loading-info" style="text-align:center">';
	confirmMsg2 += '	<img src="public/img/ajax-load.gif" width="16" height="16" />Waiting...';
	confirmMsg2 += '</div>';

	var confirmMsgLoad = '';
	confirmMsgLoad += '	<div class="clearfix" id="login-loading-info" style="text-align:center">';
	confirmMsgLoad += '		<img src="public/img/ajax-load.gif" width="16" height="16" /> Please wait...';
	confirmMsgLoad += '	</div>';

	var confirmMsgError = '';
	confirmMsgError += '<div class="field-group clearfix" id="login-panel">';
	confirmMsgError += '	<div class="field-container text clearfix" id="password_failure">';
	confirmMsgError += '		<label style="color: #AA3333;">FAILURE: Connection or server error. Refresh the page and try again.</label>';
	confirmMsgError += '	</div>';
	confirmMsgError += '</div>';


	function doSignIn() {
		function cancelConfirmation() {
			confirmPopup.hide();
		}


		function loginWait(toggle) {
			$('signin-form')[toggle ? 'hide' : 'show']();
			$('login-loading-info')[toggle ? 'show' : 'hide']();
			$$('#login-popup .button').invoke(toggle ? 'hide' : 'show');
			Popup.frobIEElements();
		}

		var loginAttempts = 0;
		function submitLoginForm() {
			// var mode;
			$('login-popup').getElementsByClassName('popup-buttons')[0].hide();
			$('login-panel').hide();
			$('login-loading-info').show();
			var temp_pwd = document.getElementById('signin-password').value;
			var nocache = new Date().getTime();
			new Ajax.Request('/login',{
					    method: 'post',
							withCredentials : true,
						parameters: {
							'password': temp_pwd,
							//  'mode': 'edit',
							'nocache': nocache
						},
						onSuccess : function(transport) {
							if (transport.responseJSON != null) {
				        var status = transport.responseJSON.status_code;
								if (status == "100") {
									document.location = "";
								} else {
									if (status == "0") {
										pwdtmp = temp_pwd;
										document.location = "";
									} else {
										$('login-popup').getElementsByClassName('popup-buttons')[0].show();
										$('login-panel').show();
										$('login-loading-info').hide();
										$('password_failure').hide();
										if (status == "2") {
											$('another_session').show();
											$('password_alert').hide();
										} else {
											$('another_session').hide();
											$('password_alert').show();
										}
									}
								}
							} else {
								$('another_session').hide();
								$('password_alert').hide();
								$('password_failure').show();
							}
						},
						onFailure : function() {
							$('login-panel').show();
							$('login-loading-info').hide();
							$('another_session').hide();
							$('password_alert').hide();
							$('password_failure').show();
						}
					});
			return;
		}

		function checkEmptyAndSubmit(el) {
			if ($F(el) === '') {
				el.focus();
			} else {
				submitLoginForm();
			}
		}

		loginPopup.show({
			title : 'Sign In',
			message : html,
			buttons : [ {
				title : 'Cancel'
			}, {
				title : 'Sign In',
				action : submitLoginForm
			} ]
		});

		$('login-loading-info').hide();
		$('password_failure').hide();
		$('another_session').hide();
		$('password_alert').hide();
		$('login-popup').getElementsByClassName('popup-buttons')[0].show();

		Popup.frobIEElements();

		$('signin-password').focus();
		$('signin-password').observe('keypress', function(evt) {
			if (evt.keyCode == 13) {
				checkEmptyAndSubmit($('signin-password'));
			}
		});
	}

	$('setup_menu').observe('click', doSignIn);

})();

/* process Enter for number fields*/
(function() {
	var wrapperDivs = $$('.userinput');
	wrapperDivs.each(function(el) {
		el.observe('keypress', function(e) {
			if(e.which == 13) { // Checks for the enter key
				e.preventDefault(); // Stops IE from triggering the button to be clicked
				el.blur();
				el.focus();
			}
		});
	});
})();

/* Full height containers :( */
(function() {
	var wrapperDivs = $$('.fullheight');
	wrapperDivs.each(function(el) {
		el.origHeight = el.getHeight();
		el.offset = el.cumulativeOffset().top + 30;
		el.innerEl = el.select('.fullheight-inner');
		el.innerEl2 = el.select('.fullheight-inner2');
		//el.innerEl = el.select('.fullheight-inner')[0];
		if (el.innerEl) {
			el.innerEl.each(function(elem) {
				el.innerOffset = elem.cumulativeOffset().top - el.offset + 80;
			});
			//el.innerOffset = el.innerEl.cumulativeOffset().top - el.offset + 70;
		}
	});

	function adjustHeight() {
		var vDim = document.viewport.getDimensions();
		wrapperDivs.each(function(el) {
			var newHeight = vDim.height - el.offset;
			if (newHeight > el.origHeight) {
				document.body.setStyle({
					overflowY : 'hidden'
				});
			} else {
				newHeight = el.origHeight;
				document.body.setStyle({
					overflowY : 'auto'
				});
			}
			el.setStyle({
				height : newHeight + 'px'
			});
			if (el.innerEl) {
				el.innerEl.each(function(elem) {
					if (elem.hasClassName('bigheight')) {
						elem.setStyle({
							height : (newHeight - el.innerOffset + 32) + 'px'
						});
					} else {
						if (elem.hasClassName('littleheight')) {
							elem.setStyle({
								height : (newHeight - el.innerOffset - 32) + 'px'
							});
						} else {
							elem.setStyle({
								height : (newHeight - el.innerOffset) + 'px'
							});
						}
					}
				});
			}
			if (el.innerEl2) {
				el.innerEl2.each(function(elem2) {
					elem2.setStyle({
						height : (newHeight - el.innerOffset + 55) + 'px'
					});
				});
			}
		});
	}
	adjustHeight();
	Event.observe(document.onresize ? document : window, "resize", adjustHeight);
})();

/* Setup stylized upload forms */
window.stylizeUpload = function(el) {
	var iw = el.up();

	el.setStyle({
		zIndex : 200,
		cursor : 'pointer !important'
	}).absolutize();

	iw.addClassName('suffix');
	var fNameBox = new Element('input', {
		readonly : true
	}).setStyle({
		width : '90%'
	});
	fNameBox.value = "Please Select File";

	var sButton = new Element('a', {
		href : '#choose-file'
	}).addClassName('button').setStyle({
		'marginTop' : Prototype.Browser.WebKit ? '-2px' : '-1px',
		'marginRight' : '-5px',
		'border' : 'none',
		'border-radius' : '0 0 0 0 !important',
		'zIndex' : '100',
		'height' : '18px',
		'lineHeight' : '18px'
	}).update('Choose File');

	iw.insert({
		top : fNameBox,
		bottom : sButton.wrap(new Element('span'))
	});

	sButton.on('click', function(evt) {
		evt.stop();
	});

	el.on('change', function() {
		fNameBox.value = $F(el).match(/[^\/\\]+$/)[0];
	});

	el.on('mouseover', function() {
		sButton.addClassName('active');
	});

	el.on('mouseout', function() {
		sButton.removeClassName('active');
	});

	el.clonePosition(sButton, {
		offsetTop : -4
	}).setOpacity(0);
};

/* Stylize file uploads */
if (!Prototype.Browser.IE) {
	$$('input[type=file]').each(stylizeUpload);
}

function getCurrentTime() {
	var currentTime = new Date();
	var seconds = currentTime.getSeconds();
	return seconds;
}

var overlay_html = '';
overlay_html += '<div class="clearfix " style="height: 100%; text-align: center; vertical-align: middle; background-image: none; background-attachment: scroll; background-repeat: repeat; background-position-x: 0%; background-position-y: 0%; background-size: auto; background-origin: padding-box; background-clip: border-box; background-color: rgba(232, 238, 241, 0.7);">';
overlay_html += '	<span style="display: inline-block; height: 100%; vertical-align: middle;"></span>';
overlay_html += '	<img width="32" height="32" src="public/img/ajax-load.gif" style="vertical-align: middle;">';
overlay_html += '</div>';
var ajax_overlay = $('dataloading');
if (!ajax_overlay) {
	ajax_overlay = new Element('div', {
		id : 'dataloading'
	});
	ajax_overlay.innerHTML = overlay_html;
	$(document.body).insert(ajax_overlay);
	ajax_overlay.setOpacity(0.7);
	ajax_overlay.hide();
}

function fireEvent(element,event){
    if (document.createEventObject){
        // dispatch for IE
        var evt = document.createEventObject();
        return element.fireEvent('on'+event,evt)
    }
    else{
        // dispatch for firefox + others
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
        return !element.dispatchEvent(evt);
    }
}
