var localstorageobjectslist = [];
var socket = io();
(function() {
  var confirmPopup = new Popup('upload-confirmation-popup');
  var confirmMsg2 = '';
	confirmMsg2 += '	<div class="field-group clearfix">';
	confirmMsg2 += '		<div class="field-container text clearfix">';
	confirmMsg2 += '		    <label for="signin-description">You need to confirm presence at Microlok System.</label>';
	confirmMsg2 += '		    <label for="signin-description">Press the ENTER button on the front panel to begin upload process:</label>';
	confirmMsg2 += '	    </div>';
	confirmMsg2 += '		<div class="field-container text clearfix" style="text-align: center;">';
	confirmMsg2 += '		    <label for="signin-description">CNFG</label>';
	confirmMsg2 += '		    <label for="signin-description">MODE</label>';
	confirmMsg2 += '	    </div>';
	confirmMsg2 += '	</div>';
	confirmMsg2 += '<div class="clearfix" id="login-loading-info" style="text-align:center">';
	confirmMsg2 += '	<img src="public/img/ajax-load.gif" width="16" height="16" />Waiting...';
	confirmMsg2 += '</div>';

  var confirmMsgLoad = '';
	confirmMsgLoad += '	<div class="clearfix" id="login-loading-info" style="text-align:center">';
	confirmMsgLoad += '		<img src="public/img/ajax-load.gif" width="16" height="16" /> Please wait...';
	confirmMsgLoad += '	</div>';

  var confirmMsgError = '';
	confirmMsgError += '<div class="field-group clearfix" id="login-panel">';
	confirmMsgError += '	<div class="field-container text clearfix" id="password_failure">';
	confirmMsgError += '		<label style="color: #AA3333;">FAILURE: Connection or server error. Refresh the page and try again.</label>';
	confirmMsgError += '	</div>';
	confirmMsgError += '</div>';

	var statusPopup = new Popup('upload-status');
  statusPopup.hide();

  function lookForConfigStatus() {
    var dataToSave = {};
    if(window.localStorage.length > 0) {
      var keyList = Object.keys(window.localStorage);
      keyList.forEach(function(key){
        dataToSave[key] = JSON.parse(window.localStorage[key]);
      });
      if(dataToSave.hasOwnProperty('objNewPassword')){
        var objNewPassword = dataToSave['objNewPassword'];
        delete dataToSave['objNewPassword'];
        socket.emit('change_password', objNewPassword['currentPass'], objNewPassword['newPass'], function(error) {
          if(error)
            console.log(error);
        });
      }
      socket.emit('save_local_storage_data', JSON.stringify(dataToSave) , function(error) {
        if(error){
          console.log("error");
        }
        else {
          alert("Data Uploaded Successfully");
          ClearLocalStorage();
          ExitConfigMode();
        }
      });
    }
    else {
      messagePopup.show({
                   title : 'Message',
                   message : ('<p style="text-align:center"> No local changes to upload. </p>'),
                   width : '370px',
                   buttons : [ {
                         title : 'OK',
                         action : function() {
                                    ExitConfigMode();
                                  }
                              } ]
                  });
    }
  }

  function lookForAcceptButton() {
    // TODO: AJAX for button state periodically
    new Ajax.Request('/look-accept', {
      method: 'post',
      onSuccess : function(transport) {
        /*resultErrorCode*/
        var status = transport.responseJSON.status_code;
        if (status == "0") {
          var result = transport.responseJSON.result;
          if (result == "1") {
            confirmPopup.show({
                  title : 'Presence confirmation',
                  message : confirmMsgLoad,
                  buttons : [ ]
                });
            lookForConfigStatus.delay(2);
          } else {
            lookForAcceptButton.delay(2);
          }
        } else {
          lookForAcceptButton.delay(2);
        }
      },
      onFailure : function() {
        lookForAcceptButton.delay(2);
      }
    });
  }

  function lookForWaitStatus() {
    confirmPopup.show({
          title : 'Presence confirmation',
          message : confirmMsg2,
          buttons : [ ]
        });
    lookForAcceptButton.delay(2);

    }

  function showWaitConfirmation() {
    confirmPopup.show({
          title : 'Presence confirmation',
          message : confirmMsgLoad,
          buttons : [ ]
        });
    var nocache = new Date().getTime();
    new Ajax.Request('/wait-confirm', {
      method: 'post',
      parameters: {
        'nocache': nocache
      },
      onSuccess : function(transport) {
        var status = transport.responseJSON.status_code;
        if (status == "0") {
          lookForWaitStatus.delay(2);
        } else {
          confirmPopup.show({
            title : 'FAILURE:',
            message : confirmMsgError,
            buttons : [ {
              title : 'Close'
            } ]
          });
        }
      },
      onFailure : function() {
        confirmPopup.show({
          title : 'FAILURE:',
          message : confirmMsgError,
          buttons : [ {
            title : 'Close'
          } ]
        });
      }
    });
  }

  function ExitConfigMode(){
    socket.emit('expire-logged-in-session', function(response){
      if(response.status == "1"){
        document.location = "/";
      }
    });
  }

function BindData(){
  socket.emit('get_all_config_list', function(error, generaldata,userdatalog,timerdata,AllBoardDataDict,cpucommdata) {

    if(error){
      ShowAlert("No data found .");
    }
    else{
    var objGeneralConfig = GetFromLocalStorage('objGeneralConfig');
    if (objGeneralConfig){
      localstorageobjectslist.push(objGeneralConfig);
    }
    var objTimerConfig = GetFromLocalStorage('objTimerConfig');
    if (objTimerConfig){
      localstorageobjectslist.push(objTimerConfig);
    }
    var objUserDataLogConfig = GetFromLocalStorage('objUserDataLogConfig');
    if (objUserDataLogConfig){
      localstorageobjectslist.push(objUserDataLogConfig);
    }
    var objBoardConfig = GetFromLocalStorage('objBoardConfig');
    if (objBoardConfig){
      localstorageobjectslist.push(objBoardConfig);
    }
    var objCPUComm = GetFromLocalStorage('objCPUComm');
    if (objCPUComm){
      localstorageobjectslist.push(objCPUComm);
    }
    if(localstorageobjectslist.length > 0){
      localstorageobjectslist.forEach(function(i){
        if(i.type == "vital"){
        var container = document.getElementById('vital_page_name');
        var node = document.createElement('div');
        node.id = "div" + (i.name);
        node.innerHTML = '<h2 style="font-size:15px;color: rgb(96, 120, 144);" id="heading'+i.name+'">'+ i.name +':<span style="padding-left:126px;">New</span><span style="margin-left:46px;">Old</span></h2>';

        container.appendChild(node);
        var container = document.getElementById('div' + (i.name));
        var nodelogic = document.createElement('div');
        var nodesynchronization = document.createElement('div');
        var nodedelay = document.createElement('div');
        nodelogic.id = "divlogic";
        nodesynchronization.id = "divsynchronization";
        nodedelay.id = "divdelay";
          if(i.name == "General Configuration"){
          var logictimeout = generaldata.configSettings!=""? generaldata.configSettings.logicTimeout:generaldata.configData['A.LOGIC_TIMEOUT'].replace(':MSEC','')
          var synchronizationtimeout = generaldata.configSettings!=""?generaldata.configSettings.synchronizationTimeout:generaldata.configData['F.SYNCHRONIZATION_TIMEOUT'].replace(':MSEC','')
          var delayreset = generaldata.configSettings!=""?generaldata.configSettings.delayReset:generaldata.configData['F.DELAY_RESET'].replace(':MSEC','')
          if(i.logicTimeout != logictimeout){
              nodelogic.innerHTML = '<h2 style="font-size:13px;" id="heading'+i.logicTimeout+'">Logic Timeout:<span style="margin-left:195px;">'+ i.logicTimeout +'</span><span style="margin-left:46px;">'+ logictimeout +'</span></h2>';
            }
          if(i.synchronizationTimeout != synchronizationtimeout){
              nodesynchronization.innerHTML ='<h2 style="font-size:13px;" id="heading'+i.synchronizationTimeout+'">Synchronization Timeout:<span style="margin-left:128px;">'+ i.synchronizationTimeout +'</span><span style="margin-left:46px;">'+ synchronizationtimeout +'</span></h2>';
            }
          if(i.delayReset != delayreset){
            nodedelay.innerHTML ='<h2 style="font-size:13px;" id="heading'+i.delayReset+'">Delay Reset:<span style="margin-left:128px;">'+ i.delayReset +'</span><span style="margin-left:46px;">'+ delayreset +'</span></h2>';
          }
        container.appendChild(nodelogic);
        container.appendChild(nodesynchronization);
        container.appendChild(nodedelay);
        for (var key in i.configList) {
          if (i.configList.hasOwnProperty(key)) {
            var events = generaldata.configSettings!=""?generaldata.configSettings.configList[key]:generaldata.configData["USER.NUMERIC"][key].replace('Initial.value=','')
            if(i.configList[key] != events){
            var container = document.getElementById('div' + (i.name));
            var node1 = document.createElement('div');
            node1.id = "divheadingconfig" + (i.name);
            node1.innerHTML = '<h2 style="font-size:13px;" id="heading'+i.configList[key]+'">Event '+ key.replace('User_Num_','') +' :<span style="margin-left:226px;">'+ i.configList[key] +'</span><span style="margin-left:68px;">'+ events +'</span></h2>';
            container.appendChild(node1);
            // console.log(key, i.configList[key]);
          }
        }
         }
        }
        else if(i.name == "Timers Configuration"){
          for (var key in i.configList) {
            timerdata.configData.forEach(function(item){
              if(item.Timer == key){
              if (i.configList.hasOwnProperty(key)) {
                setdelay = timerdata.configSettings.configList?timerdata.configSettings.configList[key].setDelaySV:item.SET.trim()
                cleardelay = timerdata.configSettings.configList?timerdata.configSettings.configList[key].clearDelaySV:item.CLEAR.trim()
                  if(i.configList[key].setDelaySV != setdelay || i.configList[key].clearDelaySV != cleardelay ){
                var container = document.getElementById('div' + (i.name));
                var node1 = document.createElement('div');
                node1.id = "divheadingconfig" + (i.name);
                node1.innerHTML = '<h2 style="font-size:13px;">'+ key +'</h2>'+
                                  '<div class="field-container text clearfix">'+
                                  '<h2 style="font-size:13px;" id="heading'+i.configList[key].setDelaySV+'"> Set Delay:<span style="margin-left:210px;">'+ i.configList[key].setDelaySV +'</span><span style="margin-left:50px;">'+ setdelay +'</span></h2>'+
                                  '</div>'+
                                  '<div class="field-container text clearfix">'+
                                  '<h2 style="font-size:13px;" id="heading'+i.configList[key].clearDelaySV+'"> Clear Delay:<span style="margin-left:200px;">'+ i.configList[key].clearDelaySV +'</span><span style="margin-left:50px;">'+ cleardelay+'</span></h2>'+
                                  '</div>';
                container.appendChild(node1);
              }
            }
        }
      });
           }
        }
        else{
          for (var key1 in i.configList) {
            if(AllBoardDataDict.hasOwnProperty('configList')){
              if(i.configList[key1]["A.ENABLE"] == true){
                i.configList[key1]["A.ENABLE"] = "1";
              }
              else{
                i.configList[key1]["A.ENABLE"] = "0";
              }
              if(AllBoardDataDict.configList[key1]!=undefined){
              if(AllBoardDataDict.configList[key1]["A.ENABLE"] == true){
                AllBoardDataDict.configList[key1]["A.ENABLE"] = "1";
              }
              else{
                AllBoardDataDict.configList[key1]["A.ENABLE"] = "0";
              }
              if(i.configList[key1]["A.ENABLE"] != AllBoardDataDict.configList[key1]["A.ENABLE"]){
              var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+key1+'"> '+key1+'("A.ENABLE")<span style="margin-left:110px;">'+ i.configList[key1]["A.ENABLE"] +'</span><span style="margin-left:60px;">'+ AllBoardDataDict.configList[key1]["A.ENABLE"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              }
              else{
                if(i.configList[key1]["A.ENABLE"]=="1"){
                  var new_value = "0";
                }
                else{
                  var new_value = "1";
                }
                var container = document.getElementById('div' + (i.name));
                var node1 = document.createElement('div');
                node1.id = "divheadingconfig" + (i.name);
                node1.innerHTML = '<div class="field-container text clearfix">'+
                                  '<h2 style="font-size:13px;" id="heading'+key1+'"> '+key1+'("A.ENABLE")<span style="margin-left:110px;">'+ i.configList[key1]["A.ENABLE"] +'</span><span style="margin-left:60px;">'+ new_value +'</span></h2>'+
                                  '</div>';
                container.appendChild(node1);
              }
            }
            else{
            for(var j=0;j<AllBoardDataDict.length;j++){
              // if(AllBoardDataDict[j].value.TYPE.indexOf(key1)>-1){
              if(AllBoardDataDict[j].value.TYPE == key1){
                if(i.configList[key1]["A.ENABLE"] == true){
                  i.configList[key1]["A.ENABLE"] = "1";
                }
                else{
                  i.configList[key1]["A.ENABLE"] = "0";
                }
                if(i.configList[key1]["A.ENABLE"] != AllBoardDataDict[j].value["A.ENABLE"]){
                var container = document.getElementById('div' + (i.name));
                var node1 = document.createElement('div');
                node1.id = "divheadingconfig" + (i.name);
                node1.innerHTML = '<div class="field-container text clearfix">'+
                                  '<h2 style="font-size:13px;" id="heading'+key1+'"> '+key1+'("A.ENABLE")<span style="margin-left:110px;">'+ i.configList[key1]["A.ENABLE"] +'</span><span style="margin-left:60px;">'+ AllBoardDataDict[j].value["A.ENABLE"] +'</span></h2>'+
                                  '</div>';
                container.appendChild(node1);
                }
              }
            }
            }
          }
        }
        }
        else{
        var container = document.getElementById('nonvital_page_name');
        var node = document.createElement('div');
        node.id = "div" + (i.name);
        if(i.name == "User Data Log Bit Selection"){
        node.innerHTML = '<h2 style="font-size:15px;color: rgb(96, 120, 144);" id="heading'+i.name+'">'+ i.name +'  (selected items):<span id="spannew'+i.name+'" style="margin-left:126px;">New</span><span id="spanold'+i.name+'" style="margin-left:116px;">Old</span></h2>';
        }
        else{
          node.innerHTML = '<h2 style="font-size:15px;color: rgb(96, 120, 144);" id="heading'+i.name+'">'+ i.name +':<span id="spannew'+i.name+'" style="margin-left:126px;">New</span><span id="spanold'+i.name+'" style="margin-left:116px;">Old</span></h2>';
        }
        container.appendChild(node);
        var container = document.getElementById('heading'+(i.name));
        var node = document.createElement('div');
        node.id = "divheading" + (i.name);
        if(i.name == "User Data Log Bit Selection"){
          document.getElementById('spannew' + (i.name)).style.display= "none";
          document.getElementById('spanold' + (i.name)).style.display= "none";
          for (var key in i.CheckedNamesDict) {
            if ((JSON.parse(i.objUserDataLog)).hasOwnProperty(key)) {
              var container = document.getElementById("div" + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.CheckedNamesDict[key]);
              node1.innerHTML = '<h2 style="font-size:13px;" id="heading'+i.CheckedNamesDict[key]+'">'+ i.CheckedNamesDict[key] +' </h2>';
              container.appendChild(node1);
            }
           }
        }
        else if(i.name == "CPU Communication"){
          if(i.configList.CPU_BASE.ETHA["A.ENABLE"] == true){
            i.configList.CPU_BASE.ETHA["A.ENABLE"] = "1";
          }
          else{
            i.configList.CPU_BASE.ETHA["A.ENABLE"] = "0";
          }
          if(i.configList.CPU_BASE.ETHB["A.ENABLE"] == true){
            i.configList.CPU_BASE.ETHB["A.ENABLE"] = "1";
          }
          else{
            i.configList.CPU_BASE.ETHB["A.ENABLE"] = "0";
          }
            if(cpucommdata.hasOwnProperty('configList')){
              if(cpucommdata.configList.CPU_BASE.ETHA["A.ENABLE"] == true){
                cpucommdata.configList.CPU_BASE.ETHA["A.ENABLE"] = "1";
              }
              else{
                cpucommdata.configList.CPU_BASE.ETHA["A.ENABLE"] = "0";
              }
              if(cpucommdata.configList.CPU_BASE.ETHB["A.ENABLE"] == true){
                cpucommdata.configList.CPU_BASE.ETHB["A.ENABLE"] = "1";
              }
              else{
                cpucommdata.configList.CPU_BASE.ETHB["A.ENABLE"] = "0";
              }
              if(i.configList.CPU_BASE.ETHA["A.ENABLE"] != cpucommdata.configList.CPU_BASE.ETHA["A.ENABLE"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHA[ENABLE]<span style="margin-left:182px;">'+ i.configList.CPU_BASE.ETHA["A.ENABLE"] +'</span><span style="margin-left:140px;">'+ cpucommdata.configList.CPU_BASE.ETHA["A.ENABLE"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHB["A.ENABLE"] != cpucommdata.configList.CPU_BASE.ETHB["A.ENABLE"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHB[ENABLE]<span style="margin-left:182px;">'+ i.configList.CPU_BASE.ETHB["A.ENABLE"] +'</span><span style="margin-left:140px;">'+ cpucommdata.configList.CPU_BASE.ETHB["A.ENABLE"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"] != cpucommdata.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHA[LOCAL ADDRESS]<span style="margin-left:90px;">'+ i.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"] +'</span><span style="margin-left:50px;">'+ cpucommdata.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHA["A.SUBNET.MASK"] != cpucommdata.configList.CPU_BASE.ETHA["A.SUBNET.MASK"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHA[SUBNET MASK]<span style="margin-left:108px;">'+ i.configList.CPU_BASE.ETHA["A.SUBNET.MASK"] +'</span><span style="margin-left:50px;">'+ cpucommdata.configList.CPU_BASE.ETHA["A.SUBNET.MASK"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHA["A.GATEWAY"] != cpucommdata.configList.CPU_BASE.ETHA["A.GATEWAY"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHA[GATEWAY]<span style="margin-left:140px;">'+ i.configList.CPU_BASE.ETHA["A.GATEWAY"] +'</span><span style="margin-left:64px;">'+ cpucommdata.configList.CPU_BASE.ETHA["A.GATEWAY"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"] != cpucommdata.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHB[LOCAL ADDRESS]<span style="margin-left:90px;">'+ i.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"] +'</span><span style="margin-left:50px;">'+ cpucommdata.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHB["A.SUBNET.MASK"] != cpucommdata.configList.CPU_BASE.ETHB["A.SUBNET.MASK"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHB[SUBNET MASK]<span style="margin-left:108px;">'+ i.configList.CPU_BASE.ETHB["A.SUBNET.MASK"] +'</span><span style="margin-left:50px;">'+ cpucommdata.configList.CPU_BASE.ETHB["A.SUBNET.MASK"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHB["A.GATEWAY"] != cpucommdata.configList.CPU_BASE.ETHB["A.GATEWAY"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHB[GATEWAY]<span style="margin-left:140px;">'+ i.configList.CPU_BASE.ETHB["A.GATEWAY"] +'</span><span style="margin-left:64px;">'+ cpucommdata.configList.CPU_BASE.ETHB["A.GATEWAY"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
            }
            else{
              if(i.configList.CPU_BASE.ETHA["A.ENABLE"] != cpucommdata[0].value.ETHA["A.ENABLE"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHA[ENABLE]<span style="margin-left:182px;">'+ i.configList.CPU_BASE.ETHA["A.ENABLE"] +'</span><span style="margin-left:140px;">'+ cpucommdata[0].value.ETHA["A.ENABLE"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHB["A.ENABLE"] != cpucommdata[0].value.ETHB["A.ENABLE"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHB[ENABLE]<span style="margin-left:182px;">'+ i.configList.CPU_BASE.ETHB["A.ENABLE"] +'</span><span style="margin-left:140px;">'+ cpucommdata[0].value.ETHB["A.ENABLE"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"] != cpucommdata[0].value.ETHA["A.LOCAL.ADDRESS"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHA[LOCAL ADDRESS]<span style="margin-left:90px;">'+ i.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"] +'</span><span style="margin-left:50px;">'+ cpucommdata[0].value.ETHA["A.LOCAL.ADDRESS"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHA["A.SUBNET.MASK"] != cpucommdata[0].value.ETHA["A.SUBNET.MASK"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHA[SUBNET MASK]<span style="margin-left:108px;">'+ i.configList.CPU_BASE.ETHA["A.SUBNET.MASK"] +'</span><span style="margin-left:50px;">'+ cpucommdata[0].value.ETHA["A.SUBNET.MASK"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHA["A.GATEWAY"] != cpucommdata[0].value.ETHA["A.GATEWAY"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHA[GATEWAY]<span style="margin-left:140px;">'+ i.configList.CPU_BASE.ETHA["A.GATEWAY"] +'</span><span style="margin-left:64px;">'+ cpucommdata[0].value.ETHA["A.GATEWAY"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"] != cpucommdata[0].value.ETHB["A.LOCAL.ADDRESS"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHB[LOCAL ADDRESS]<span style="margin-left:90px;">'+ i.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"] +'</span><span style="margin-left:50px;">'+ cpucommdata[0].value.ETHB["A.LOCAL.ADDRESS"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHB["A.SUBNET.MASK"] != cpucommdata[0].value.ETHB["A.SUBNET.MASK"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHB[SUBNET MASK]<span style="margin-left:108px;">'+ i.configList.CPU_BASE.ETHB["A.SUBNET.MASK"] +'</span><span style="margin-left:50px;">'+ cpucommdata[0].value.ETHB["A.SUBNET.MASK"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
              if(i.configList.CPU_BASE.ETHB["A.GATEWAY"] != cpucommdata[0].value.ETHB["A.GATEWAY"]){
                var container = document.getElementById('div' + (i.name));
              var node1 = document.createElement('div');
              node1.id = "divheadingconfig" + (i.name);
              node1.innerHTML = '<div class="field-container text clearfix">'+
                                '<h2 style="font-size:13px;" id="heading'+i.configList.CPU_BASE.TYPE+'"> ETHB[GATEWAY]<span style="margin-left:140px;">'+ i.configList.CPU_BASE.ETHB["A.GATEWAY"] +'</span><span style="margin-left:64px;">'+ cpucommdata[0].value.ETHB["A.GATEWAY"] +'</span></h2>'+
                                '</div>';
              container.appendChild(node1);
              }
            }
        }
        }
      });
    }
  }
});
}
document.observe("dom:loaded", function() {
  BindData();
  var applybutton = document.getElementById('apply_changes_btn') ? false : true;
  if(applybutton){
  }
  else{
    $('apply_changes_btn').observe('click',function(){
      showWaitConfirmation();
      });
  }
  var discardbutton = document.getElementById('discard_changes_btn_new') ? false : true;
  if(discardbutton){
  }
  else{
  $('discard_changes_btn_new').observe('click', function(){
    if(window.localStorage.length > 0){
    ClearLocalStorage();
    ExitConfigMode();
    }
    else{
      messagePopup.show({
                   title : 'Message',
                   message : ('<p style="text-align:center"> No local changes to discard. </p>'),
                   width : '370px',
                   buttons : [ {
                         title : 'OK',
                         action : function() {
                                    ExitConfigMode();
                                  }
                              } ]
                  });
    }
  });
}
});
})();
