(function() {
	var socket = io();
	var last_transport;
	var selectedLink ="";
	var selectedLinkData ="";
	var selectedStationData ="";
	document.observe("dom:loaded", function() {
		var element = document.getElementById("aclist");
		element.classList.add("active");
		// var implementationID;
		// var portTimeout;
		// var ipPort;
		socket.emit('get-communication-conf-data');
		socket.on('communication-conf-data', function(objLinkConf){
			if(objLinkConf.status == "ok"){
				last_transport = objLinkConf.data;
				BindData(objLinkConf.data);
			}
			else{
				ShowAlert(objLinkConf.data);
			}
		});

		function BindData(confData){
			console.log(confData)
			var select = $('linkselector');
			select.innerHTML = "";
			var option = document.createElement("option");
			option.innerHTML = "";
			select.appendChild(option);
			for (var i=0; i < confData.length; i++)
			{
				var option = document.createElement("option");
				//option.id = obj[i];
				option.value = confData[i].key.replace('Link.','');
				option.innerHTML = confData[i].key.replace('Link.','').trim();
				select.appendChild(option);
			}
			/*if(selectedLink == ""){
			select.selectedIndex = 0
			selectedLink = select.options[0].value;
			console.log(selectedLink);
		}
		else{
		select.value = selectedLink;
	}
	var index = confData.map(function(o) { return o.key; }).indexOf('Link.' +selectedLink);
	var selectedLinkData = confData[index].value;
	console.log(selectedLinkData);
	document.getElementById('protocol').value = selectedLinkData["PROTOCOL"];
	document.getElementById('port1').value = selectedLinkData.hasOwnProperty("A.PORT") ? selectedLinkData["A.PORT"] : (selectedLinkData.hasOwnProperty("F.PORT") ? selectedLinkData["F.PORT"] : "");
	document.getElementById('port2').value = selectedLinkData.hasOwnProperty("A.PORT2") ? selectedLinkData["A.PORT2"] : (selectedLinkData.hasOwnProperty("F.PORT2") ? selectedLinkData["F.PORT2"] : "");
	var link_info_table = document.getElementById('link-adjust-tbody');
	var rows;
	Object.keys(selectedLinkData).forEach(function(key) {
	if(key != "A.PORT" && key != "A.PORT2" && key != "PROTOCOL" && key.indexOf("STATION.") == -1){
	if(rows === undefined)
	rows = '<tr class="intable"><td style="width:250px">'+ key.replace("A.","").replace("F.","") +'</td><td style="text-align:right;font-weight:bold">'+selectedLinkData[key].replace(":MSEC","")+'</td></tr>'
	else
	rows += '<tr class="intable"><td style="width:250px">'+ key.replace("A.","").replace("F.","") +'</td><td style="text-align:right;font-weight:bold">'+selectedLinkData[key].replace(":MSEC","")+'</td></tr>'
}
});
link_info_table.innerHTML = rows;*/
}

function BindStationList(selectedLink){
	var select = $('stationselector');
	select.innerHTML = "";
	var confData = last_transport;
	var index = confData.map(function(o) { return o.key; }).indexOf('Link.' +selectedLink);
	selectedLinkData = confData[index].value;
	var selectedLinkAllStationData = [];
	Object.keys(selectedLinkData).forEach(function(key){
		if (key.indexOf("STATION.NAME") > -1) {
			selectedLinkAllStationData.push({key : key, value : selectedLinkData[key]})
		}
	});
	//console.log(selectedLinkAllStationData);
	var option = document.createElement("option");
	option.innerHTML = "";
	select.appendChild(option);
	for (var i=0; i < selectedLinkAllStationData.length; i++)
	{
		var option = document.createElement("option");
		//option.id = obj[i];
		option.value = selectedLinkAllStationData[i].key;
		option.innerHTML = selectedLinkAllStationData[i].key.replace('STATION.NAME.','').replace('STATION.NAME','').trim();
		select.appendChild(option);
	}
}

function hide_all_forms(){
	document.getElementById("frm_Safe_P_Link").style.display = "none";
	document.getElementById("frm_Safe_P_Station").style.display = "none";
	document.getElementById("frm_Peer_link").style.display = "none";
	document.getElementById("frm_Peer_Station").style.display = "none";
}


function bind_Safe_P_station_data(selectedStationData){
	clear_Safe_P_Station_form();
	document.getElementById("frm_Safe_P_Link").style.display = "none";
	document.getElementById("frm_Peer_link").style.display = "none";
	document.getElementById("frm_Peer_Station").style.display = "none";
	document.getElementById("frm_Safe_P_Station").style.display = "block";
	Object.keys(selectedStationData).forEach(function(key){
		switch(key) {
			case "A.ENABLE":
			document.getElementById("Safe_P_img_StationEnable").src = "public/img/accept.png"
			document.getElementById("Safe_P_chk_StationEnable").checked = selectedStationData["A.ENABLE"] == "1" ? true : false;
			break;
			case "F.ENABLE":
			document.getElementById("Safe_P_img_StationEnable").src = "public/img/error.png"
			document.getElementById("Safe_P_chk_StationEnable").checked = selectedStationData["F.ENABLE"] == "1" ? true : false;
			break;
			case "V.ENABLE":
			document.getElementById("Safe_P_img_StationEnable").src = "public/img/Warning.png"
			document.getElementById("Safe_P_chk_StationEnable").checked = selectedStationData["V.ENABLE"] == "1" ? true : false;
			break;

			case "A.IMPLEMENTATION.VERSION":
			document.getElementById("Safe_P_img_implementationID").src = "public/img/accept.png"
			document.getElementById("Safe_P_implementationID").value = selectedStationData["A.IMPLEMENTATION.VERSION"];
			break;
			case "F.IMPLEMENTATION.VERSION":
			document.getElementById("Safe_P_img_implementationID").src = "public/img/error.png"
			document.getElementById("Safe_P_implementationID").value = selectedStationData["F.IMPLEMENTATION.VERSION"];
			break;
			case "V.IMPLEMENTATION.VERSION":
			document.getElementById("Safe_P_img_implementationID").src = "public/img/Warning.png"
			document.getElementById("Safe_P_implementationID").value = selectedStationData["V.IMPLEMENTATION.VERSION"];
			break;

			case "A.ACTIVITY.TIMEOUT":
			document.getElementById("Safe_P_img_activity_timeout").src = "public/img/accept.png"
			document.getElementById("Safe_P_activity_timeout").value = selectedStationData["A.ACTIVITY.TIMEOUT"].replace(':MSEC','');
			break;
			case "F.ACTIVITY.TIMEOUT":
			document.getElementById("Safe_P_img_activity_timeout").src = "public/img/error.png"
			document.getElementById("Safe_P_activity_timeout").value = selectedStationData["F.ACTIVITY.TIMEOUT"].replace(':MSEC','');
			break;
			case "V.ACTIVITY.TIMEOUT":
			document.getElementById("Safe_P_img_activity_timeout").src = "public/img/Warning.png"
			document.getElementById("Safe_P_activity_timeout").value = selectedStationData["V.ACTIVITY.TIMEOUT"].replace(':MSEC','');
			break;

			case "A.SAFEP.ADDRESS":
			document.getElementById("Safe_P_img_station_address").src = "public/img/accept.png"
			document.getElementById("Safe_P_txt_station_address").value = selectedStationData["A.SAFEP.ADDRESS"];
			break;
			case "F.SAFEP.ADDRESS":
			document.getElementById("Safe_P_img_station_address").src = "public/img/error.png"
			document.getElementById("Safe_P_txt_station_address").value = selectedStationData["F.SAFEP.ADDRESS"];
			break;
			case "V.SAFEP.ADDRESS":
			document.getElementById("Safe_P_img_station_address").src = "public/img/Warning.png"
			document.getElementById("Safe_P_txt_station_address").value = selectedStationData["V.SAFEP.ADDRESS"];
			break;

			case "A.SAFEP.PEER.ADDRESS":
			document.getElementById("Safe_P_img_peer_address").src = "public/img/accept.png"
			document.getElementById("Safe_P_txt_peer_address").value = selectedStationData["A.SAFEP.PEER.ADDRESS"];
			break;
			case "F.SAFEP.PEER.ADDRESS":
			document.getElementById("Safe_P_img_peer_address").src = "public/img/error.png"
			document.getElementById("Safe_P_txt_peer_address").value = selectedStationData["F.SAFEP.PEER.ADDRESS"];
			break;
			case "V.SAFEP.PEER.ADDRESS":
			document.getElementById("Safe_P_img_peer_address").src = "public/img/Warning.png"
			document.getElementById("Safe_P_txt_peer_address").value = selectedStationData["V.SAFEP.PEER.ADDRESS"];
			break;

			case "A.BROADCAST.INTERVAL":
			document.getElementById("Safe_P_img_broadcast_interval").src = "public/img/accept.png"
			document.getElementById("Safe_P_txt_broadcast_interval").value = selectedStationData["A.BROADCAST.INTERVAL"].replace(':MSEC','');
			break;
			case "F.BROADCAST.INTERVAL":
			document.getElementById("Safe_P_img_broadcast_interval").src = "public/img/error.png"
			document.getElementById("Safe_P_txt_broadcast_interval").value = selectedStationData["F.BROADCAST.INTERVAL"].replace(':MSEC','');
			break;
			case "V.BROADCAST.INTERVAL":
			document.getElementById("Safe_P_img_broadcast_interval").src = "public/img/Warning.png"
			document.getElementById("Safe_P_txt_broadcast_interval").value = selectedStationData["V.BROADCAST.INTERVAL"].replace(':MSEC','');
			break;

			case "A.STALE.DATA.TIMEOUT":
			document.getElementById("Safe_P_img_stale_data_timeout").src = "public/img/accept.png"
			document.getElementById("Safe_P_txt_stale_data_timeout").value = selectedStationData["A.STALE.DATA.TIMEOUT"].replace(':MSEC','');
			break;
			case "F.STALE.DATA.TIMEOUT":
			document.getElementById("Safe_P_img_stale_data_timeout").src = "public/img/error.png"
			document.getElementById("Safe_P_txt_stale_data_timeout").value = selectedStationData["F.STALE.DATA.TIMEOUT"].replace(':MSEC','');
			break;
			case "V.STALE.DATA.TIMEOUT":
			document.getElementById("Safe_P_img_stale_data_timeout").src = "public/img/Warning.png"
			document.getElementById("Safe_P_txt_stale_data_timeout").value = selectedStationData["V.STALE.DATA.TIMEOUT"].replace(':MSEC','');
			break;

			case "A.PEER.IP.ADDRESS.1.A":
			document.getElementById("Safe_P_img_destination_address_1A").src = "public/img/accept.png"
			document.getElementById("Safe_P_txt_destination_address_1A1").value = selectedStationData["A.PEER.IP.ADDRESS.1.A"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_1A2").value = selectedStationData["A.PEER.IP.ADDRESS.1.A"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_1A3").value = selectedStationData["A.PEER.IP.ADDRESS.1.A"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_1A4").value = selectedStationData["A.PEER.IP.ADDRESS.1.A"].split('.')[3];
			break;
			case "F.PEER.IP.ADDRESS.1.A":
			document.getElementById("Safe_P_img_destination_address_1A").src = "public/img/error.png"
			document.getElementById("Safe_P_txt_destination_address_1A1").value = selectedStationData["F.PEER.IP.ADDRESS.1.A"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_1A2").value = selectedStationData["F.PEER.IP.ADDRESS.1.A"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_1A3").value = selectedStationData["F.PEER.IP.ADDRESS.1.A"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_1A4").value = selectedStationData["F.PEER.IP.ADDRESS.1.A"].split('.')[3];
			break;
			case "V.PEER.IP.ADDRESS.1.A":
			document.getElementById("Safe_P_img_destination_address_1A").src = "public/img/Warning.png"
			document.getElementById("Safe_P_txt_destination_address_1A1").value = selectedStationData["V.PEER.IP.ADDRESS.1.A"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_1A2").value = selectedStationData["V.PEER.IP.ADDRESS.1.A"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_1A3").value = selectedStationData["V.PEER.IP.ADDRESS.1.A"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_1A4").value = selectedStationData["V.PEER.IP.ADDRESS.1.A"].split('.')[3];
			break;

			case "A.PEER.IP.ADDRESS.1.B":
			document.getElementById("Safe_P_img_destination_address_1B").src = "public/img/accept.png"
			document.getElementById("Safe_P_txt_destination_address_1B1").value = selectedStationData["A.PEER.IP.ADDRESS.1.B"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_1B2").value = selectedStationData["A.PEER.IP.ADDRESS.1.B"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_1B3").value = selectedStationData["A.PEER.IP.ADDRESS.1.B"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_1B4").value = selectedStationData["A.PEER.IP.ADDRESS.1.B"].split('.')[3];
			break;
			case "F.PEER.IP.ADDRESS.1.B":
			document.getElementById("Safe_P_img_destination_address_1B").src = "public/img/error.png"
			document.getElementById("Safe_P_txt_destination_address_1B1").value = selectedStationData["F.PEER.IP.ADDRESS.1.B"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_1B2").value = selectedStationData["F.PEER.IP.ADDRESS.1.B"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_1B3").value = selectedStationData["F.PEER.IP.ADDRESS.1.B"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_1B4").value = selectedStationData["F.PEER.IP.ADDRESS.1.B"].split('.')[3];
			break;
			case "V.PEER.IP.ADDRESS.1.B":
			document.getElementById("Safe_P_img_destination_address_1B").src = "public/img/Warning.png"
			document.getElementById("Safe_P_txt_destination_address_1B1").value = selectedStationData["V.PEER.IP.ADDRESS.1.B"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_1B2").value = selectedStationData["V.PEER.IP.ADDRESS.1.B"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_1B3").value = selectedStationData["V.PEER.IP.ADDRESS.1.B"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_1B4").value = selectedStationData["V.PEER.IP.ADDRESS.1.B"].split('.')[3];
			break;

			case "A.PEER.IP.ADDRESS.2.A":
			document.getElementById("Safe_P_img_destination_address_2A").src = "public/img/accept.png"
			document.getElementById("Safe_P_txt_destination_address_2A1").value = selectedStationData["A.PEER.IP.ADDRESS.2.A"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_2A2").value = selectedStationData["A.PEER.IP.ADDRESS.2.A"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_2A3").value = selectedStationData["A.PEER.IP.ADDRESS.2.A"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_2A4").value = selectedStationData["A.PEER.IP.ADDRESS.2.A"].split('.')[3];
			break;
			case "F.PEER.IP.ADDRESS.2.A":
			document.getElementById("Safe_P_img_destination_address_2A").src = "public/img/error.png"
			document.getElementById("Safe_P_txt_destination_address_2A1").value = selectedStationData["F.PEER.IP.ADDRESS.2.A"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_2A2").value = selectedStationData["F.PEER.IP.ADDRESS.2.A"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_2A3").value = selectedStationData["F.PEER.IP.ADDRESS.2.A"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_2A4").value = selectedStationData["F.PEER.IP.ADDRESS.2.A"].split('.')[3];
			break;
			case "V.PEER.IP.ADDRESS.2.A":
			document.getElementById("Safe_P_img_destination_address_2A").src = "public/img/Warning.png"
			document.getElementById("Safe_P_txt_destination_address_2A1").value = selectedStationData["V.PEER.IP.ADDRESS.2.A"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_2A2").value = selectedStationData["V.PEER.IP.ADDRESS.2.A"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_2A3").value = selectedStationData["V.PEER.IP.ADDRESS.2.A"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_2A4").value = selectedStationData["V.PEER.IP.ADDRESS.2.A"].split('.')[3];
			break;

			case "A.PEER.IP.ADDRESS.2.B":
			document.getElementById("Safe_P_img_destination_address_2B").src = "public/img/accept.png"
			document.getElementById("Safe_P_txt_destination_address_2B1").value = selectedStationData["A.PEER.IP.ADDRESS.2.B"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_2B2").value = selectedStationData["A.PEER.IP.ADDRESS.2.B"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_2B3").value = selectedStationData["A.PEER.IP.ADDRESS.2.B"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_2B4").value = selectedStationData["A.PEER.IP.ADDRESS.2.B"].split('.')[3];
			break;
			case "F.PEER.IP.ADDRESS.2.B":
			document.getElementById("Safe_P_img_destination_address_2B").src = "public/img/error.png"
			document.getElementById("Safe_P_txt_destination_address_2B1").value = selectedStationData["F.PEER.IP.ADDRESS.2.B"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_2B2").value = selectedStationData["F.PEER.IP.ADDRESS.2.B"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_2B3").value = selectedStationData["F.PEER.IP.ADDRESS.2.B"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_2B4").value = selectedStationData["F.PEER.IP.ADDRESS.2.B"].split('.')[3];
			break;
			case "V.PEER.IP.ADDRESS.2.B":
			document.getElementById("Safe_P_img_destination_address_2B").src = "public/img/Warning.png"
			document.getElementById("Safe_P_txt_destination_address_2B1").value = selectedStationData["V.PEER.IP.ADDRESS.2.B"].split('.')[0];
			document.getElementById("Safe_P_txt_destination_address_2B2").value = selectedStationData["V.PEER.IP.ADDRESS.2.B"].split('.')[1];
			document.getElementById("Safe_P_txt_destination_address_2B3").value = selectedStationData["V.PEER.IP.ADDRESS.2.B"].split('.')[2];
			document.getElementById("Safe_P_txt_destination_address_2B4").value = selectedStationData["V.PEER.IP.ADDRESS.2.B"].split('.')[3];
			break;
		}
	});
}

function clear_Safe_P_Station_form(){
	document.getElementById("Safe_P_img_StationEnable").src = "public/img/accept.png"
	document.getElementById("Safe_P_chk_StationEnable").checked = false;
	document.getElementById("Safe_P_img_implementationID").src = "public/img/accept.png"
	document.getElementById("Safe_P_implementationID").value = "";
	document.getElementById("Safe_P_img_activity_timeout").src = ""
	document.getElementById("Safe_P_activity_timeout").value = "";
	document.getElementById("Safe_P_img_station_address").src = "public/img/accept.png"
	document.getElementById("Safe_P_txt_station_address").value = "";
	document.getElementById("Safe_P_img_peer_address").src = "public/img/accept.png"
	document.getElementById("Safe_P_txt_peer_address").value = "";
	document.getElementById("Safe_P_img_broadcast_interval").src = "public/img/accept.png"
	document.getElementById("Safe_P_txt_broadcast_interval").value = "";
	document.getElementById("Safe_P_img_stale_data_timeout").src = "public/img/accept.png"
	document.getElementById("Safe_P_txt_stale_data_timeout").value = "";
	document.getElementById("Safe_P_img_destination_address_1A").src = "public/img/accept.png"
	document.getElementById("Safe_P_txt_destination_address_1A1").value = "";
	document.getElementById("Safe_P_txt_destination_address_1A2").value = "";
	document.getElementById("Safe_P_txt_destination_address_1A3").value = "";
	document.getElementById("Safe_P_txt_destination_address_1A4").value = "";
	document.getElementById("Safe_P_img_destination_address_1B").src = "public/img/accept.png"
	document.getElementById("Safe_P_txt_destination_address_1B1").value = "";
	document.getElementById("Safe_P_txt_destination_address_1B2").value = "";
	document.getElementById("Safe_P_txt_destination_address_1B3").value = "";
	document.getElementById("Safe_P_txt_destination_address_1B4").value = "";
	document.getElementById("Safe_P_img_destination_address_2A").src = "public/img/accept.png"
	document.getElementById("Safe_P_txt_destination_address_2A1").value = "";
	document.getElementById("Safe_P_txt_destination_address_2A2").value = "";
	document.getElementById("Safe_P_txt_destination_address_2A3").value = "";
	document.getElementById("Safe_P_txt_destination_address_2A4").value = "";
	document.getElementById("Safe_P_img_destination_address_2B").src = "public/img/accept.png"
	document.getElementById("Safe_P_txt_destination_address_2B1").value = "";
	document.getElementById("Safe_P_txt_destination_address_2B2").value = "";
	document.getElementById("Safe_P_txt_destination_address_2B3").value = "";
	document.getElementById("Safe_P_txt_destination_address_2B4").value = "";
}

function clear_peer_link_form(){
	document.getElementById("Peer_img_StationEnable").src = "public/img/accept.png"
	document.getElementById("Peer_chk_StationEnable").checked = false;
	document.getElementById("Peer_img_implementationID").src = "public/img/accept.png"
	document.getElementById("Peer_implementationID").value = "";
	document.getElementById("Peer_img_port_timeout").src = "public/img/accept.png"
	document.getElementById("Peer_port_timeout").value = ""
	document.getElementById("Peer_Link_img_physical_port_number").src = "public/img/accept.png"
	document.getElementById("Peer_Link_select_physical_port_number").innerHTML = "";
	document.getElementById("Peer_Link_select_physical_port_number").value = "";
	document.getElementById("Peer_img_baud_rate").src = "public/img/accept.png"
	document.getElementById("Peer_txt_baud_rate").value = "";
	document.getElementById("Peer_img_stop_bits").src = "public/img/accept.png"
	document.getElementById("Peer_txt_stop_bits").value = "";
	document.getElementById("Peer_img_parity").src = "public/img/accept.png"
	document.getElementById("Peer_txt_parity").value = "";
	document.getElementById("Peer_img_key_on_delay").src = "public/img/accept.png"
	document.getElementById("Peer_txt_key_on_delay").value = "";
	document.getElementById("Peer_img_key_off_delay").src = "public/img/accept.png"
	document.getElementById("Peer_txt_key_off_delay").value = "";
	document.getElementById("Peer_img_grant_delay").src = "public/img/accept.png"
	document.getElementById("Peer_txt_grant_delay").value = "";
	document.getElementById("Peer_img_point_to_point").src = "public/img/accept.png"
	document.getElementById("Peer_chk_point_to_point").checked = "";
	document.getElementById("Peer_img_ip_port").src = "public/img/accept.png"
	document.getElementById("Peer_ip_port").value = "";
	document.getElementById("Peer_Link_img_physical_port_number_2").src = "public/img/accept.png"
	document.getElementById("Peer_Link_select_physical_port_number_2").innerHTML = "";
	document.getElementById("Peer_Link_select_physical_port_number_2").value ="";
}

function bind_Safe_P_link_data(selectedLinkData){
	document.getElementById("frm_Peer_link").style.display = "none";
	document.getElementById("frm_Safe_P_Link").style.display = "block";
	document.getElementById("frm_Safe_P_Station").style.display = "none";
  // var objLinkConfig = GetFromLocalStorage('objLinkConfig');
	Object.keys(selectedLinkData).forEach(function(key){
		switch(key) {
			case "A.ENABLE":
			document.getElementById("Safe_P_Link_img_Enable").src = "public/img/accept.png"
			document.getElementById("Safe_P_Link_chk_Enable").checked = selectedLinkData["A.ENABLE"] == "1" ? true : false;
			break;
			case "F.ENABLE":
			document.getElementById("Safe_P_Link_img_Enable").src = "public/img/error.png"
			document.getElementById("Safe_P_Link_chk_Enable").checked = selectedLinkData["F.ENABLE"] == "1" ? true : false;
			break;
			case "V.ENABLE":
			document.getElementById("Safe_P_Link_img_Enable").src = "public/img/Warning.png"
			document.getElementById("Safe_P_Link_chk_Enable").checked = selectedLinkData["V.ENABLE"] == "1" ? true : false;
			break;

			case "A.IMPLEMENTATION.VERSION":
			document.getElementById("Safe_P_Link_img_implementationID").src = "public/img/accept.png"
			document.getElementById("Safe_P_Link_implementationID").value = selectedLinkData["A.IMPLEMENTATION.VERSION"];
			break;
			case "F.IMPLEMENTATION.VERSION":
			document.getElementById("Safe_P_Link_img_implementationID").src = "public/img/error.png"
			document.getElementById("Safe_P_Link_implementationID").value = selectedLinkData["F.IMPLEMENTATION.VERSION"];
			break;
			case "V.IMPLEMENTATION.VERSION":
			document.getElementById("Safe_P_Link_img_implementationID").src = "public/img/Warning.png"
			document.getElementById("Safe_P_Link_implementationID").value = selectedLinkData["V.IMPLEMENTATION.VERSION"];
			break;

			case "A.PORT.TIMEOUT":
			document.getElementById("Safe_P_Link_img_port_timeout").src = "public/img/accept.png"
			document.getElementById("Safe_P_Link_port_timeout").value = selectedLinkData["A.PORT.TIMEOUT"].replace(':MSEC','');
			break;
			case "F.PORT.TIMEOUT":
			document.getElementById("Safe_P_Link_img_port_timeout").src = "public/img/error.png"
			document.getElementById("Safe_P_Link_port_timeout").value = selectedLinkData["F.PORT.TIMEOUT"].replace(':MSEC','');
			break;
			case "V.PORT.TIMEOUT":
			document.getElementById("Safe_P_Link_img_port_timeout").src = "public/img/Warning.png"
			document.getElementById("Safe_P_Link_port_timeout").value = selectedLinkData["V.PORT.TIMEOUT"].replace(':MSEC','');
			break;

			case "A.PORT":
			document.getElementById("Safe_P_Link_img_station_address").src = "public/img/accept.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["A.PORT"];
			option.innerHTML = selectedLinkData["A.PORT"];
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_1").appendChild(option);
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_1").value = selectedLinkData["A.PORT"];
			break;
			case "F.PORT":
			document.getElementById("Safe_P_Link_img_station_address").src = "public/img/error.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["F.PORT"];
			option.innerHTML = selectedLinkData["F.PORT"];
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_1").appendChild(option);
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_1").value = selectedLinkData["F.PORT"];
			break;
			case "V.PORT":
			document.getElementById("Safe_P_Link_img_station_address").src = "public/img/Warning.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["V.PORT"];
			option.innerHTML = selectedLinkData["V.PORT"];
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_1").appendChild(option);
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_1").value = selectedLinkData["V.PORT"];
			break;

			case "A.PORT2":
			document.getElementById("Safe_P_Link_img_Physical_Port_Number_2").src = "public/img/accept.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["A.PORT2"];
			option.innerHTML = selectedLinkData["A.PORT2"];
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_2").appendChild(option);
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_2").value = selectedLinkData["A.PORT2"];
			break;
			case "F.PORT2":
			document.getElementById("Safe_P_Link_img_Physical_Port_Number_2").src = "public/img/error.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["F.PORT2"];
			option.innerHTML = selectedLinkData["F.PORT2"];
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_2").appendChild(option);
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_2").value = selectedLinkData["F.PORT2"];
			break;
			case "V.PORT2":
			document.getElementById("Safe_P_Link_img_Physical_Port_Number_2").src = "public/img/Warning.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["V.PORT2"];
			option.innerHTML = selectedLinkData["V.PORT2"];
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_2").appendChild(option);
			document.getElementById("Safe_P_Link_select_Physical_Port_Number_2").value = selectedLinkData["V.PORT2"];
			break;

			case "A.IP.PORT":
			document.getElementById("Safe_P_Link_img_IP_port").src = "public/img/accept.png"
			document.getElementById("Safe_P_Link_txt_IP_port").value = selectedLinkData["A.IP.PORT"];
			break;
			case "F.IP.PORT":
			document.getElementById("Safe_P_Link_img_IP_port").src = "public/img/error.png"
			document.getElementById("Safe_P_Link_txt_IP_port").value = selectedLinkData["F.IP.PORT"];
			break;
			case "V.IP.PORT":
			document.getElementById("Safe_P_Link_img_IP_port").src = "public/img/Warning.png"
			document.getElementById("Safe_P_Link_txt_IP_port").value = selectedLinkData["V.IP.PORT"];
			break;

		}
	});
}

function bind_Peer_link_data(selectedLinkData){
	document.getElementById("frm_Peer_link").style.display = "block";
	document.getElementById("frm_Safe_P_Link").style.display = "none";
	document.getElementById("frm_Safe_P_Station").style.display = "none";
	clear_peer_link_form();
	Object.keys(selectedLinkData).forEach(function(key){
		switch(key) {
			case "A.ENABLE":
			document.getElementById("Peer_img_StationEnable").src = "public/img/accept.png"
			document.getElementById("Peer_chk_StationEnable").checked = selectedLinkData["A.ENABLE"] == "1" ? true : false;
			break;
			case "F.ENABLE":
			document.getElementById("Peer_img_StationEnable").src = "public/img/error.png"
			document.getElementById("Peer_chk_StationEnable").checked = selectedLinkData["F.ENABLE"] == "1" ? true : false;
			break;
			case "V.ENABLE":
			document.getElementById("Peer_img_StationEnable").src = "public/img/Warning.png"
			document.getElementById("Peer_chk_StationEnable").checked = selectedLinkData["V.ENABLE"] == "1" ? true : false;
			break;

			case "A.IMPLEMENTATION.VERSION":
			document.getElementById("Peer_img_implementationID").src = "public/img/accept.png"
			document.getElementById("Peer_implementationID").value = selectedLinkData["A.IMPLEMENTATION.VERSION"];
			break;
			case "F.IMPLEMENTATION.VERSION":
			document.getElementById("Peer_img_implementationID").src = "public/img/error.png"
			document.getElementById("Peer_implementationID").value = selectedLinkData["F.IMPLEMENTATION.VERSION"];
			break;
			case "V.IMPLEMENTATION.VERSION":
			document.getElementById("Peer_img_implementationID").src = "public/img/Warning.png"
			document.getElementById("Peer_implementationID").value = selectedLinkData["V.IMPLEMENTATION.VERSION"];
			break;

			case "A.PORT.TIMEOUT":
			document.getElementById("Peer_img_port_timeout").src = "public/img/accept.png"
			document.getElementById("Peer_port_timeout").value = selectedLinkData["A.PORT.TIMEOUT"].replace(':MSEC','');
			break;
			case "F.PORT.TIMEOUT":
			document.getElementById("Peer_img_port_timeout").src = "public/img/error.png"
			document.getElementById("Peer_port_timeout").value = selectedLinkData["F.PORT.TIMEOUT"].replace(':MSEC','');
			break;
			case "V.PORT.TIMEOUT":
			document.getElementById("Peer_img_port_timeout").src = "public/img/Warning.png"
			document.getElementById("Peer_port_timeout").value = selectedLinkData["V.PORT.TIMEOUT"].replace(':MSEC','');
			break;

			case "A.PORT":
			document.getElementById("Peer_Link_img_physical_port_number").src = "public/img/accept.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["A.PORT"];
			option.innerHTML = selectedLinkData["A.PORT"];
			document.getElementById("Peer_Link_select_physical_port_number").appendChild(option);
			document.getElementById("Peer_Link_select_physical_port_number").value = selectedLinkData["A.PORT"];
			break;
			case "F.PORT":
			document.getElementById("Peer_Link_img_physical_port_number").src = "public/img/error.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["F.PORT"];
			option.innerHTML = selectedLinkData["F.PORT"];
			document.getElementById("Peer_Link_select_physical_port_number").appendChild(option);
			document.getElementById("Peer_Link_select_physical_port_number").value = selectedLinkData["F.PORT"];
			break;
			case "V.PORT":
			document.getElementById("Peer_Link_img_physical_port_number").src = "public/img/Warning.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["V.PORT"];
			option.innerHTML = selectedLinkData["V.PORT"];
			document.getElementById("Peer_Link_select_physical_port_number").appendChild(option);
			document.getElementById("Peer_Link_select_physical_port_number").value = selectedLinkData["V.PORT"];
			break;

			case "A.BAUD":
			document.getElementById("Peer_img_baud_rate").src = "public/img/accept.png"
			document.getElementById("Peer_txt_baud_rate").value = selectedLinkData["A.BAUD"];
			break;
			case "F.BAUD":
			document.getElementById("Peer_img_baud_rate").src = "public/img/error.png"
			document.getElementById("Peer_txt_baud_rate").value = selectedLinkData["F.BAUD"];
			break;
			case "V.BAUD":
			document.getElementById("Peer_img_baud_rate").src = "public/img/Warning.png"
			document.getElementById("Peer_txt_baud_rate").value = selectedLinkData["V.BAUD"];
			break;

			case "A.STOPBITS":
			document.getElementById("Peer_img_stop_bits").src = "public/img/accept.png"
			document.getElementById("Peer_txt_stop_bits").value = selectedLinkData["A.STOPBITS"];
			break;
			case "F.STOPBITS":
			document.getElementById("Peer_img_stop_bits").src = "public/img/error.png"
			document.getElementById("Peer_txt_stop_bits").value = selectedLinkData["F.STOPBITS"];
			break;
			case "V.STOPBITS":
			document.getElementById("Peer_img_stop_bits").src = "public/img/Warning.png"
			document.getElementById("Peer_txt_stop_bits").value = selectedLinkData["V.STOPBITS"];
			break;

			case "A.PARITY":
			document.getElementById("Peer_img_parity").src = "public/img/accept.png"
			document.getElementById("Peer_txt_parity").value = selectedLinkData["A.PARITY"];
			break;
			case "F.PARITY":
			document.getElementById("Peer_img_parity").src = "public/img/error.png"
			document.getElementById("Peer_txt_parity").value = selectedLinkData["F.PARITY"];
			break;
			case "V.PARITY":
			document.getElementById("Peer_img_parity").src = "public/img/Warning.png"
			document.getElementById("Peer_txt_parity").value = selectedLinkData["V.PARITY"];
			break;

			case "A.KEY.ON.DELAY":
			document.getElementById("Peer_img_key_on_delay").src = "public/img/accept.png"
			document.getElementById("Peer_txt_key_on_delay").value = selectedLinkData["A.KEY.ON.DELAY"];
			break;
			case "F.KEY.ON.DELAY":
			document.getElementById("Peer_img_key_on_delay").src = "public/img/error.png"
			document.getElementById("Peer_txt_key_on_delay").value = selectedLinkData["F.KEY.ON.DELAY"];
			break;
			case "V.KEY.ON.DELAY":
			document.getElementById("Peer_img_key_on_delay").src = "public/img/Warning.png"
			document.getElementById("Peer_txt_key_on_delay").value = selectedLinkData["V.KEY.ON.DELAY"];
			break;

			case "A.KEY.OFF.DELAY":
			document.getElementById("Peer_img_key_off_delay").src = "public/img/accept.png"
			document.getElementById("Peer_txt_key_off_delay").value = selectedLinkData["A.KEY.OFF.DELAY"];
			break;
			case "F.KEY.OFF.DELAY":
			document.getElementById("Peer_img_key_off_delay").src = "public/img/error.png"
			document.getElementById("Peer_txt_key_off_delay").value = selectedLinkData["F.KEY.OFF.DELAY"];
			break;
			case "V.KEY.OFF.DELAY":
			document.getElementById("Peer_img_key_off_delay").src = "public/img/Warning.png"
			document.getElementById("Peer_txt_key_off_delay").value = selectedLinkData["V.KEY.OFF.DELAY"];
			break;

			case "A.GRANT.DELAY":
			document.getElementById("Peer_img_grant_delay").src = "public/img/accept.png"
			document.getElementById("Peer_txt_grant_delay").value = selectedLinkData["A.GRANT.DELAY"].replace(':MSEC','');
			break;
			case "F.GRANT.DELAY":
			document.getElementById("Peer_img_grant_delay").src = "public/img/error.png"
			document.getElementById("Peer_txt_grant_delay").value = selectedLinkData["F.GRANT.DELAY"].replace(':MSEC','');
			break;
			case "V.GRANT.DELAY":
			document.getElementById("Peer_img_grant_delay").src = "public/img/Warning.png"
			document.getElementById("Peer_txt_grant_delay").value = selectedLinkData["V.GRANT.DELAY"].replace(':MSEC','');
			break;

			case "A.POINT.POINT":
			document.getElementById("Peer_img_point_to_point").src = "public/img/accept.png"
			document.getElementById("Peer_chk_point_to_point").checked = selectedLinkData["A.POINT.POINT"] == "1" ? true : false;
			break;
			case "F.POINT.POINT":
			document.getElementById("Peer_img_point_to_point").src = "public/img/error.png"
			document.getElementById("Peer_chk_point_to_point").checked = selectedLinkData["F.POINT.POINT"] == "1" ? true : false;
			break;
			case "V.POINT.POINT":
			document.getElementById("Peer_img_point_to_point").src = "public/img/Warning.png"
			document.getElementById("Peer_chk_point_to_point").checked = selectedLinkData["V.POINT.POINT"] == "1" ? true : false;
			break;

			case "A.IP.PORT":
			document.getElementById("Peer_img_ip_port").src = "public/img/accept.png"
			document.getElementById("Peer_ip_port").value = selectedLinkData["A.IP.PORT"];
			break;
			case "F.IP.PORT":
			document.getElementById("Peer_img_ip_port").src = "public/img/error.png"
			document.getElementById("Peer_ip_port").value = selectedLinkData["F.IP.PORT"];
			break;
			case "V.IP.PORT":
			document.getElementById("Peer_img_ip_port").src = "public/img/Warning.png"
			document.getElementById("Peer_ip_port").value = selectedLinkData["V.IP.PORT"];
			break;

			case "A.PORT2":
			document.getElementById("Peer_Link_img_physical_port_number_2").src = "public/img/accept.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["A.PORT2"];
			option.innerHTML = selectedLinkData["A.PORT2"];
			document.getElementById("Peer_Link_select_physical_port_number_2").appendChild(option);
			document.getElementById("Peer_Link_select_physical_port_number_2").value = selectedLinkData["A.PORT2"];
			break;
			case "F.PORT2":
			document.getElementById("Peer_Link_img_physical_port_number_2").src = "public/img/error.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["F.PORT2"];
			option.innerHTML = selectedLinkData["F.PORT2"];
			document.getElementById("Peer_Link_select_physical_port_number_2").appendChild(option);
			document.getElementById("Peer_Link_select_physical_port_number_2").value = selectedLinkData["F.PORT2"];
			break;
			case "V.PORT2":
			document.getElementById("Peer_Link_img_physical_port_number_2").src = "public/img/Warning.png"
			var option = document.createElement("option");
			option.value = selectedLinkData["V.PORT2"];
			option.innerHTML = selectedLinkData["V.PORT2"];
			document.getElementById("Peer_Link_select_physical_port_number_2").appendChild(option);
			document.getElementById("Peer_Link_select_physical_port_number_2").value = selectedLinkData["V.PORT2"];
			break;

		}
	});
}

function bind_Peer_station_data(selectedLinkData){
	document.getElementById("frm_Safe_P_Link").style.display = "none";
	document.getElementById("frm_Peer_link").style.display = "none";
	document.getElementById("frm_Peer_Station").style.display = "block";
	document.getElementById("frm_Safe_P_Station").style.display = "none";
	Object.keys(selectedStationData).forEach(function(key){
		switch(key) {
			case "A.ENABLE":
			document.getElementById("Peer_Station_img_StationEnable").src = "public/img/accept.png"
			document.getElementById("Peer_Station_chk_StationEnable").checked = selectedStationData["A.ENABLE"] == "1" ? true : false;
			break;
			case "F.ENABLE":
			document.getElementById("Peer_Station_img_StationEnable").src = "public/img/error.png"
			document.getElementById("Peer_Station_chk_StationEnable").checked = selectedStationData["F.ENABLE"] == "1" ? true : false;
			break;
			case "V.ENABLE":
			document.getElementById("Peer_Station_img_StationEnable").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_chk_StationEnable").checked = selectedStationData["V.ENABLE"] == "1" ? true : false;
			break;

			case "A.MII.NV.ADDRESS":
			document.getElementById("Peer_Station_img_Microlok_Address").src = "public/img/accept.png"
			document.getElementById("Peer_Station_Microlok_Address").value = selectedStationData["A.IMPLEMENTATION.VERSION"];
			break;
			case "F.MII.NV.ADDRESS":
			document.getElementById("Peer_Station_img_Microlok_Address").src = "public/img/error.png"
			document.getElementById("Peer_Station_Microlok_Address").value = selectedStationData["F.IMPLEMENTATION.VERSION"];
			break;
			case "V.MII.NV.ADDRESS":
			document.getElementById("Peer_Station_img_Microlok_Address").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_Microlok_Address").value = selectedStationData["V.IMPLEMENTATION.VERSION"];
			break;

			case "A.TIME.STAMP":
			document.getElementById("Peer_Station_img_Time_Stamp").src = "public/img/accept.png"
			document.getElementById("Peer_Station_chk_Time_Stamp").checked = selectedStationData["A.TIME.STAMP"] == "1" ? true : false;
			break;
			case "F.TIME.STAMP":
			document.getElementById("Peer_Station_img_Time_Stamp").src = "public/img/error.png"
			document.getElementById("Peer_Station_chk_Time_Stamp").checked = selectedStationData["F.TIME.STAMP"] == "1" ? true : false;
			break;
			case "V.TIME.STAMP":
			document.getElementById("Peer_Station_img_Time_Stamp").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_chk_Time_Stamp").checked = selectedStationData["V.TIME.STAMP"] == "1" ? true : false;
			break;

			case "A.PEER.ADDRESS":
			document.getElementById("Peer_Station_img_Peer_Address").src = "public/img/accept.png"
			document.getElementById("Peer_Station_Peer_Address").value = selectedStationData["A.PEER.ADDRESS"];
			break;
			case "F.PEER.ADDRESS":
			document.getElementById("Peer_Station_img_Peer_Address").src = "public/img/error.png"
			document.getElementById("Peer_Station_Peer_Address").value = selectedStationData["F.PEER.ADDRESS"];
			break;
			case "V.PEER.ADDRESS":
			document.getElementById("Peer_Station_img_Peer_Address").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_Peer_Address").value = selectedStationData["V.PEER.ADDRESS"];
			break;

			case "A.IMPLEMENTATION.VERSION":
			document.getElementById("Peer_Station_img_ImplementationID").src = "public/img/accept.png"
			document.getElementById("Peer_Station_ImplementationID").value = selectedStationData["A.IMPLEMENTATION.VERSION"];
			break;
			case "F.IMPLEMENTATION.VERSION":
			document.getElementById("Peer_Station_img_ImplementationID").src = "public/img/error.png"
			document.getElementById("Peer_Station_ImplementationID").value = selectedStationData["F.IMPLEMENTATION.VERSION"];
			break;
			case "V.IMPLEMENTATION.VERSION":
			document.getElementById("Peer_Station_img_ImplementationID").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_ImplementationID").value = selectedStationData["V.IMPLEMENTATION.VERSION"];
			break;

			case "A.ACTIVITY.TIMEOUT":
			document.getElementById("Peer_Station_img_activity_timeout").src = "public/img/accept.png"
			document.getElementById("Peer_Station_activity_timeout").value = selectedStationData["A.ACTIVITY.TIMEOUT"];
			break;
			case "F.ACTIVITY.TIMEOUT":
			document.getElementById("Peer_Station_img_activity_timeout").src = "public/img/error.png"
			document.getElementById("Peer_Station_activity_timeout").value = selectedStationData["F.ACTIVITY.TIMEOUT"];
			break;
			case "V.ACTIVITY.TIMEOUT":
			document.getElementById("Peer_Station_img_activity_timeout").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_activity_timeout").value = selectedStationData["V.ACTIVITY.TIMEOUT"];
			break;

			case "A.ACK.TIMEOUT":
			document.getElementById("Peer_Station_img_ACK_Timeout").src = "public/img/accept.png"
			document.getElementById("Peer_Station_txt_ACK_Timeout").value = selectedStationData["A.ACK.TIMEOUT"];
			break;
			case "F.ACK.TIMEOUT":
			document.getElementById("Peer_Station_img_ACK_Timeout").src = "public/img/error.png"
			document.getElementById("Peer_Station_txt_ACK_Timeout").value = selectedStationData["F.ACK.TIMEOUT"];
			break;
			case "V.ACK.TIMEOUT":
			document.getElementById("Peer_Station_img_ACK_Timeout").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_txt_ACK_Timeout").value = selectedStationData["V.ACK.TIMEOUT"];
			break;

			case "A.HEARTBEAT.INTERVAL":
			document.getElementById("Peer_Station_img_Heartbeat_Interval").src = "public/img/accept.png"
			document.getElementById("Peer_Station_txt_Heartbeat_Interval").value = selectedStationData["A.HEARTBEAT.INTERVAL"];
			break;
			case "F.HEARTBEAT.INTERVAL":
			document.getElementById("Peer_Station_img_Heartbeat_Interval").src = "public/img/error.png"
			document.getElementById("Peer_Station_txt_Heartbeat_Interval").value = selectedStationData["F.HEARTBEAT.INTERVAL"];
			break;
			case "V.HEARTBEAT.INTERVAL":
			document.getElementById("Peer_Station_img_Heartbeat_Interval").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_txt_Heartbeat_Interval").value = selectedStationData["V.HEARTBEAT.INTERVAL"];
			break;

			case "A.INDICATION.UPDATE.CYCLE":
			document.getElementById("Peer_Station_img_Indication_Update_Cycle").src = "public/img/accept.png"
			document.getElementById("Peer_Station_txt_Indication_Update_Cycle").value = selectedStationData["A.INDICATION.UPDATE.CYCLE"];
			break;
			case "F.INDICATION.UPDATE.CYCLE":
			document.getElementById("Peer_Station_img_Indication_Update_Cycle").src = "public/img/error.png"
			document.getElementById("Peer_Station_txt_Indication_Update_Cycle").value = selectedStationData["F.INDICATION.UPDATE.CYCLE"];
			break;
			case "V.INDICATION.UPDATE.CYCLE":
			document.getElementById("Peer_Station_img_Indication_Update_Cycle").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_txt_Indication_Update_Cycle").value = selectedStationData["V.INDICATION.UPDATE.CYCLE"];
			break;

			case "A.STALE.DATA.TIMEOUT":
			document.getElementById("Peer_Station_img_stale_data_timeout").src = "public/img/accept.png"
			document.getElementById("Peer_Station_txt_stale_data_timeout").value = selectedStationData["A.STALE.DATA.TIMEOUT"];
			break;
			case "F.STALE.DATA.TIMEOUT":
			document.getElementById("Peer_Station_img_stale_data_timeout").src = "public/img/error.png"
			document.getElementById("Peer_Station_txt_stale_data_timeout").value = selectedStationData["F.STALE.DATA.TIMEOUT"];
			break;
			case "V.STALE.DATA.TIMEOUT":
			document.getElementById("Peer_Station_img_stale_data_timeout").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_txt_stale_data_timeout").value = selectedStationData["V.STALE.DATA.TIMEOUT"];
			break;

			case "A.CLOCK.MASTER":
			document.getElementById("Peer_Station_img_Clock_Master").src = "public/img/accept.png"
			document.getElementById("Peer_Station_chk_Clock_Master").value = selectedStationData["A.CLOCK.MASTER"];
			break;
			case "F.CLOCK.MASTER":
			document.getElementById("Peer_Station_img_Clock_Master").src = "public/img/error.png"
			document.getElementById("Peer_Station_chk_Clock_Master").value = selectedStationData["F.CLOCK.MASTER"];
			break;
			case "V.CLOCK.MASTER":
			document.getElementById("Peer_Station_img_Clock_Master").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_chk_Clock_Master").value = selectedStationData["V.CLOCK.MASTER"];
			break;

			case "A.PEER.IP.ADDRESS.1.A":
			document.getElementById("Peer_Station_img_destination_address_1A").src = "public/img/accept.png"
			document.getElementById("Peer_Station_txt_destination_address_1A1").value = selectedStationData["A.PEER.IP.ADDRESS.1.A"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_1A2").value = selectedStationData["A.PEER.IP.ADDRESS.1.A"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_1A3").value = selectedStationData["A.PEER.IP.ADDRESS.1.A"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_1A4").value = selectedStationData["A.PEER.IP.ADDRESS.1.A"].split('.')[3];
			break;
			case "F.PEER.IP.ADDRESS.1.A":
			document.getElementById("Peer_Station_img_destination_address_1A").src = "public/img/error.png"
			document.getElementById("Peer_Station_txt_destination_address_1A1").value = selectedStationData["F.PEER.IP.ADDRESS.1.A"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_1A2").value = selectedStationData["F.PEER.IP.ADDRESS.1.A"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_1A3").value = selectedStationData["F.PEER.IP.ADDRESS.1.A"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_1A4").value = selectedStationData["F.PEER.IP.ADDRESS.1.A"].split('.')[3];
			break;
			case "V.PEER.IP.ADDRESS.1.A":
			document.getElementById("Peer_Station_img_destination_address_1A").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_txt_destination_address_1A1").value = selectedStationData["V.PEER.IP.ADDRESS.1.A"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_1A2").value = selectedStationData["V.PEER.IP.ADDRESS.1.A"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_1A3").value = selectedStationData["V.PEER.IP.ADDRESS.1.A"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_1A4").value = selectedStationData["V.PEER.IP.ADDRESS.1.A"].split('.')[3];
			break;

			case "A.PEER.IP.ADDRESS.1.B":
			document.getElementById("Peer_Station_img_destination_address_1B").src = "public/img/accept.png"
			document.getElementById("Peer_Station_txt_destination_address_1B1").value = selectedStationData["A.PEER.IP.ADDRESS.1.B"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_1B2").value = selectedStationData["A.PEER.IP.ADDRESS.1.B"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_1B3").value = selectedStationData["A.PEER.IP.ADDRESS.1.B"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_1B4").value = selectedStationData["A.PEER.IP.ADDRESS.1.B"].split('.')[3];
			break;
			case "F.PEER.IP.ADDRESS.1.B":
			document.getElementById("Peer_Station_img_destination_address_1B").src = "public/img/error.png"
			document.getElementById("Peer_Station_txt_destination_address_1B1").value = selectedStationData["F.PEER.IP.ADDRESS.1.B"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_1B2").value = selectedStationData["F.PEER.IP.ADDRESS.1.B"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_1B3").value = selectedStationData["F.PEER.IP.ADDRESS.1.B"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_1B4").value = selectedStationData["F.PEER.IP.ADDRESS.1.B"].split('.')[3];
			break;
			case "V.PEER.IP.ADDRESS.1.B":
			document.getElementById("Peer_Station_img_destination_address_1B").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_txt_destination_address_1B1").value = selectedStationData["V.PEER.IP.ADDRESS.1.B"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_1B2").value = selectedStationData["V.PEER.IP.ADDRESS.1.B"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_1B3").value = selectedStationData["V.PEER.IP.ADDRESS.1.B"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_1B4").value = selectedStationData["V.PEER.IP.ADDRESS.1.B"].split('.')[3];
			break;

			case "A.PEER.IP.ADDRESS.2.A":
			document.getElementById("Peer_Station_img_destination_address_2A").src = "public/img/accept.png"
			document.getElementById("Peer_Station_txt_destination_address_2A1").value = selectedStationData["A.PEER.IP.ADDRESS.2.A"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_2A2").value = selectedStationData["A.PEER.IP.ADDRESS.2.A"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_2A3").value = selectedStationData["A.PEER.IP.ADDRESS.2.A"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_2A4").value = selectedStationData["A.PEER.IP.ADDRESS.2.A"].split('.')[3];
			break;
			case "F.PEER.IP.ADDRESS.2.A":
			document.getElementById("Peer_Station_img_destination_address_2A").src = "public/img/error.png"
			document.getElementById("Peer_Station_txt_destination_address_2A1").value = selectedStationData["F.PEER.IP.ADDRESS.2.A"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_2A2").value = selectedStationData["F.PEER.IP.ADDRESS.2.A"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_2A3").value = selectedStationData["F.PEER.IP.ADDRESS.2.A"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_2A4").value = selectedStationData["F.PEER.IP.ADDRESS.2.A"].split('.')[3];
			break;
			case "V.PEER.IP.ADDRESS.2.A":
			document.getElementById("Peer_Station_img_destination_address_2A").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_txt_destination_address_2A1").value = selectedStationData["V.PEER.IP.ADDRESS.2.A"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_2A2").value = selectedStationData["V.PEER.IP.ADDRESS.2.A"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_2A3").value = selectedStationData["V.PEER.IP.ADDRESS.2.A"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_2A4").value = selectedStationData["V.PEER.IP.ADDRESS.2.A"].split('.')[3];
			break;

			case "A.PEER.IP.ADDRESS.2.B":
			document.getElementById("Peer_Station_img_destination_address_2B").src = "public/img/accept.png"
			document.getElementById("Peer_Station_txt_destination_address_2B1").value = selectedStationData["A.PEER.IP.ADDRESS.2.B"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_2B2").value = selectedStationData["A.PEER.IP.ADDRESS.2.B"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_2B3").value = selectedStationData["A.PEER.IP.ADDRESS.2.B"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_2B4").value = selectedStationData["A.PEER.IP.ADDRESS.2.B"].split('.')[3];
			break;
			case "F.PEER.IP.ADDRESS.2.B":
			document.getElementById("Peer_Station_img_destination_address_2B").src = "public/img/error.png"
			document.getElementById("Peer_Station_txt_destination_address_2B1").value = selectedStationData["F.PEER.IP.ADDRESS.2.B"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_2B2").value = selectedStationData["F.PEER.IP.ADDRESS.2.B"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_2B3").value = selectedStationData["F.PEER.IP.ADDRESS.2.B"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_2B4").value = selectedStationData["F.PEER.IP.ADDRESS.2.B"].split('.')[3];
			break;
			case "V.PEER.IP.ADDRESS.2.B":
			document.getElementById("Peer_Station_img_destination_address_2B").src = "public/img/Warning.png"
			document.getElementById("Peer_Station_txt_destination_address_2B1").value = selectedStationData["V.PEER.IP.ADDRESS.2.B"].split('.')[0];
			document.getElementById("Peer_Station_txt_destination_address_2B2").value = selectedStationData["V.PEER.IP.ADDRESS.2.B"].split('.')[1];
			document.getElementById("Peer_Station_txt_destination_address_2B3").value = selectedStationData["V.PEER.IP.ADDRESS.2.B"].split('.')[2];
			document.getElementById("Peer_Station_txt_destination_address_2B4").value = selectedStationData["V.PEER.IP.ADDRESS.2.B"].split('.')[3];
			break;
		}
	});
}

// $('Safe_P_Link_implementationID').observe('keyup', function(){
// var implementationID = $(this).value;
// formmodified="1";
// var objLinkConfig = GetLinkConfigObject(selectedLink,implementationID,portTimeout,ipPort);
// SaveToLocalStorage("objLinkConfig" , objLinkConfig);
// });
// $('Safe_P_Link_port_timeout').observe('keyup', function(){
// var portTimeout = $(this).value;
// formmodified="1";
// var objLinkConfig = GetLinkConfigObject(selectedLink,implementationID,portTimeout,ipPort);
// SaveToLocalStorage("objLinkConfig" , objLinkConfig);
// });
// $('Safe_P_Link_port_timeout').observe('mouseup', function(){
// var portTimeout = $(this).value;
// formmodified="1";
// var objLinkConfig = GetLinkConfigObject(selectedLink,implementationID,portTimeout,ipPort);
// SaveToLocalStorage("objLinkConfig" , objLinkConfig);
// });
// $('Safe_P_Link_txt_IP_port').observe('keyup', function(){
// var ipPort = $(this).value;
// formmodified="1";
// var objLinkConfig = GetLinkConfigObject(selectedLink,implementationID,portTimeout,ipPort);
// SaveToLocalStorage("objLinkConfig" , objLinkConfig);
// });
//
// function GetLinkConfigObject(selectedLink,implementationID,portTimeout,ipPort){	var LinkConfigObject = {
// 		"selectedLink" : "",
// 		"implementationID" : "",
// 		"portTimeout" :"",
// 		"ipPort" : ""
// 	};
//
// 		LinkConfigObject.selectedLink = selectedLink;
// 		LinkConfigObject.implementationID = implementationID==undefined?document.getElementById('Safe_P_Link_implementationID').value:implementationID;
// 		LinkConfigObject.portTimeout = portTimeout==undefined?document.getElementById('Safe_P_Link_port_timeout').value:portTimeout;
// 		LinkConfigObject.ipPort = ipPort==undefined?document.getElementById('Safe_P_Link_txt_IP_port').value:ipPort;
// 		return LinkConfigObject;
// }

function BindLinkScreenData(selectedLink){
	if(selectedLink != ""){		
		var confData = last_transport;
		var index = confData.map(function(o) { return o.key; }).indexOf('Link.' +selectedLink);
		selectedLinkData = confData[index].value;
		if(selectedLink.indexOf("SAFE_P") > -1){
			bind_Safe_P_link_data(selectedLinkData);
		}
		else if(selectedLink.indexOf("_PEER_") > -1){
			bind_Peer_link_data(selectedLinkData);
		}
	}
}

function BindStationScreenData(selectedStation){
	if(selectedStation != ""){
		if(selectedLinkData !=="" ){
			selectedStationData = selectedLinkData[selectedStation];
			console.log(selectedStationData);
		}
		if(selectedLink.indexOf("SAFE_P") > -1){
			document.getElementById("frm_Safe_P_Link").style.display = "block";
			bind_Safe_P_station_data(selectedStationData)
		}
		else if (selectedLink.indexOf("_PEER_") > -1){
			document.getElementById("frm_Peer_Station").style.display = "block";
			bind_Peer_station_data(selectedStationData)
		}
	}
}

$('linkselector').observe('change',function(){
	hide_all_forms();
	selectedLink = this.value;
	// var objLinkConfig = GetLinkConfigObject(selectedLink,implementationID,portTimeout,ipPort);
	// SaveToLocalStorage("objLinkConfig" , objLinkConfig);
	BindLinkScreenData(selectedLink);
	BindStationList(this.value)
});

$('stationselector').observe('change',function(){
	hide_all_forms();
	BindStationScreenData(this.value)
});

});
})();
