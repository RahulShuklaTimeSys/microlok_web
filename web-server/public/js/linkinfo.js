(function() {
	var socket = io();
  var last_transport;
	var selectedLink = "";
	document.observe("dom:loaded", function() {
		var element = document.getElementById("rmlist");
		element.classList.add("active");
		socket.emit('get-link-conf-data');
		socket.on('link-conf-data', function(objLinkConf){
      if(objLinkConf.status == "ok"){
        last_transport = objLinkConf.data;
        BindData(objLinkConf.data);
      }
      else{
        ShowAlert(objLinkConf.data);
      }
		});

    function BindData(confData){
      console.log(confData)
			var select = $('linkselector');
			select.innerHTML = "";
      //var option = document.createElement("option");
      for (var i=0; i < confData.length; i++)
      {
        var option = document.createElement("option");
        //option.id = obj[i];
        option.value = confData[i].key.replace('Link.','');
        option.innerHTML = confData[i].key.replace('Link.','').trim();
        select.appendChild(option);
      }
			if(selectedLink == ""){
				select.selectedIndex = 0
				selectedLink = select.options[0].value;
				console.log(selectedLink);
			}
			else{
				select.value = selectedLink;
			}
			var index = confData.map(function(o) { return o.key; }).indexOf('Link.' +selectedLink);
			var selectedLinkData = confData[index].value;
			console.log(selectedLinkData);
			document.getElementById('protocol').value = selectedLinkData["PROTOCOL"];
			document.getElementById('port1').value = selectedLinkData.hasOwnProperty("A.PORT") ? selectedLinkData["A.PORT"] : (selectedLinkData.hasOwnProperty("F.PORT") ? selectedLinkData["F.PORT"] : "");
			document.getElementById('port2').value = selectedLinkData.hasOwnProperty("A.PORT2") ? selectedLinkData["A.PORT2"] : (selectedLinkData.hasOwnProperty("F.PORT2") ? selectedLinkData["F.PORT2"] : "");
			var link_info_table = document.getElementById('link-adjust-tbody');
			var rows;
			Object.keys(selectedLinkData).forEach(function(key) {
				if(key != "A.PORT" && key != "A.PORT2" && key != "PROTOCOL" && key.indexOf("STATION.") == -1){
					if(rows === undefined)
						rows = '<tr class="intable"><td style="width:250px;font-size:13px;">'+ key.replace("A.","").replace("F.","") +'</td><td style="text-align:right;font-weight:bold;font-size:13px;">'+selectedLinkData[key].replace(":MSEC","")+'</td></tr>'
					else
						rows += '<tr class="intable"><td style="width:250px;font-size:13px;">'+ key.replace("A.","").replace("F.","") +'</td><td style="text-align:right;font-weight:bold;font-size:13px;">'+selectedLinkData[key].replace(":MSEC","")+'</td></tr>'
					}
				});
			link_info_table.innerHTML = rows;
		}

    $('linkselector').observe('change',function(){
			selectedLink = this.value;
			socket.emit('get-link-conf-data');
    });

	});
})();
