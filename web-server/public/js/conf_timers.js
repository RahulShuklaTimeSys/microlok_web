var timerIDList = [];
var last_transport;
var socket = io();

(function() {
  // Wait until dom loded


  function BindData(){

    socket.emit('get_timer_config_list', function(error, data) {
      if(error){
        ShowAlert("No data found for General Configuration.");
      }
      else{
          var configList = {};
          last_transport = data.ConfigList;
          var objTimerConfig = GetFromLocalStorage('objTimerConfig') ? GetFromLocalStorage('objTimerConfig') : (data.configSettings != "" ? data.configSettings : false);          
          data.configData.forEach(function(item){
          timerIDList.push(item.Timer);
          var container = document.getElementById('TimerList');
          var node = document.createElement('div');
          node.id = "div" + item.Timer;
          node.classList.add("field-group");
          node.classList.add("clearfix");
          node.setAttribute("style", "margin: 0 0 10px 0");
          node.innerHTML = '<h2 style="font-size:13px;">'+item.Timer+'</h2>'+
                            '<div class="field-container text clearfix">'+
                            ' <label for="SetDelay0" style="width: 200px; float: left; margin-top: 5px;font-size:13px;font-weight:100;">Set Delay:</label>'+
                            ' <img id="img_SetDelay0" style="float: left; display: block;" src="public/img/Warning.png">'+
                            ' <div id="DivSetDelay' + item.Timer + '" class="iw suffix" style="width: 175px; float: left; margin: 0 5px;">'+
                            '   <input id="SetDelay' + item.Timer + '" style="width:100px;height:21px;background-color:transprent;font-size:12px;" class="cfgproc" step="1" min="0" max="6553500" maxlength="10" value="' + (objTimerConfig.configList ? objTimerConfig.configList[item.Timer].setDelaySV :objTimerConfig ? objTimerConfig[item.Timer].setDelaySV : item.SET.trim()) + '" name="SetDelay0" type="number">'+
                            '   <span style="margin-top:-18px;">milliseconds</span>'+
                            ' </div>'+
                            ' <div class="validation-error" style="width: 100px; float: left; margin-top: 5px;">[default: <span id="def_SetDelay0">' + item.SET + '</span>]'+
                            ' </div>'+
                            '</div>'+
                            '<div class="field-container text clearfix">'+
                            '  <label for="ClearDelay0" style="width: 200px; float: left; margin-top: 5px;font-size:13px;font-weight:100;">Clear Delay:</label>'+
                            '  <img id="img_ClearDelay0" style="float: left; display: block;" src="public/img/Warning.png">'+
                            '  <div id="DivClearDelay' + item.Timer + '" class="iw suffix" style="width: 175px; float: left; margin: 0 5px;">'+
                            '    <input id="ClearDelay' + item.Timer + '" style="width:100px;height:21px;background-color:transprent;font-size:12px;" class="cfgproc" step="1" min="0" max="6553500" maxlength="10" value="' + (objTimerConfig.configList ? objTimerConfig.configList[item.Timer].clearDelaySV : objTimerConfig ? objTimerConfig[item.Timer].clearDelaySV : item.CLEAR.trim()) + '" name="ClearDelay0" type="number">'+
                            '    <span style="margin-top:-18px;">milliseconds</span>'+
                            '  </div>'+
                            ' <div class="validation-error" style="width: 100px; float: left; margin-top: 5px;">[default: <span id="def_ClearDelay0">' + item.CLEAR + '</span>]'+
                            ' </div>'+
                            '</div>';
          container.appendChild(node);
          var setdelay = document.getElementById('SetDelay' + item.Timer);
          var cleardelay = document.getElementById('ClearDelay' + item.Timer);
          setdelay.addEventListener("keyup", function(e){
            if(timerIDList.length > 0){
              timerIDList.forEach(function(ID){
                configList[ID] = { "setDelaySV" : document.getElementById('SetDelay' + ID).value, "clearDelaySV" : document.getElementById('ClearDelay' + ID).value};
              });
            }
            var formmodified="1";
            var objTimerConfig = {"configList" : configList, "type":"vital", "name":"Timers Configuration","formmodified":formmodified};
            SaveToLocalStorage("objTimerConfig" , objTimerConfig);
          });
          setdelay.addEventListener("mouseup", function(e){
            if(timerIDList.length > 0){
              timerIDList.forEach(function(ID){
                configList[ID] = { "setDelaySV" : document.getElementById('SetDelay' + ID).value, "clearDelaySV" : document.getElementById('ClearDelay' + ID).value};
              });
            }
            var formmodified="1";
            var objTimerConfig = {"configList" : configList, "type":"vital", "name":"Timers Configuration","formmodified":formmodified};
            SaveToLocalStorage("objTimerConfig" , objTimerConfig);
          });
          cleardelay.addEventListener("keyup", function(e){
            if(timerIDList.length > 0){
              timerIDList.forEach(function(ID){
                configList[ID] = { "setDelaySV" : document.getElementById('SetDelay' + ID).value, "clearDelaySV" : document.getElementById('ClearDelay' + ID).value};
              });
            }
            var formmodified="1";
            var objTimerConfig = {"configList" : configList, "type":"vital", "name":"Timers Configuration","formmodified":formmodified};
            SaveToLocalStorage("objTimerConfig" , objTimerConfig);
          });
          cleardelay.addEventListener("mouseup", function(e){
            if(timerIDList.length > 0){
              timerIDList.forEach(function(ID){
                configList[ID] = { "setDelaySV" : document.getElementById('SetDelay' + ID).value, "clearDelaySV" : document.getElementById('ClearDelay' + ID).value};
              });
            }
            var formmodified="1";
            var objTimerConfig = {"configList" : configList, "type":"vital", "name":"Timers Configuration","formmodified":formmodified};
            SaveToLocalStorage("objTimerConfig" , objTimerConfig);
          });
          document.getElementById('SetDelay' + item.Timer).disabled = document.getElementById('conf_save_changes_btn') ? false : true;
          if(document.getElementById('SetDelay' + item.Timer).disabled){
            document.getElementById('DivSetDelay' + item.Timer).classList.add('is-disabled');
          }
          document.getElementById('ClearDelay' + item.Timer).disabled = document.getElementById('conf_save_changes_btn') ? false : true;
          if(document.getElementById('ClearDelay' + item.Timer).disabled){
            document.getElementById('DivClearDelay' + item.Timer).classList.add('is-disabled');
          }
        });
      }
    });
  }

  function SetCurrentLabel(e, textboxId, currentLabelId, defaultLabelId){
    if(document.getElementById(defaultLabelId).innerHTML.indexOf(document.getElementById(textboxId).value) > 0){
      document.getElementById(currentLabelId).style.display = 'none';
    }
    else {
      document.getElementById(currentLabelId).style.display = '';
    }
  }

  document.observe("dom:loaded", function() {
    var element = document.getElementById("aclist");
  	element.classList.add("active");
    BindData();
    var checkbutton = document.getElementById('conf_save_changes_btn') ? false : true;
    if(checkbutton){
    }
    else{
      $('conf_save_changes_btn').observe('click',function(){
        document.location = "submit_data";
      });
    }

  });
})();
