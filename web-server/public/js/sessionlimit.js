function showSessionsTimeout() {
	var sessionsExpPopup = new Popup('sessions-exp-popup');
	sessionsExpPopup.show({
		title : 'Warning',
		message : 'All active sessions expired. Try again after <strong id="sessexp">30</strong> seconds.',
		buttons : [ {
			title : 'try again',
			action : function() {
				location.reload();
			}
		} ]
	});
}
sessionTimeout = showSessionsTimeout.delay();