(function() {
	var socket = io();
	socket.emit('get_system_info');

	function getDateString(date) {
		return (date.getMonth()+1) + "/" +
				date.getDate() + "/" +
				date.getFullYear() + " " +
				(date.getHours()<10?"0":"") + date.getHours() + ":" +
				(date.getMinutes()<10?"0":"") + date.getMinutes() + ":" +
				(date.getSeconds()<10?"0":"") + date.getSeconds();
	}

	save_page = "System Information\r\n";
	save_filename = "SysInfo";

	function updatePage(system_info) {
		// alert("");
		try {
			save_text = "";
			if (system_info) {
				var appname = system_info.sysinfo.sysInfo.Program;
				if (appname) {
					$('sysinfo-program').innerHTML=appname;
				}
				var execver = system_info.sysinfo.sysInfo.ExecVer;
				if (execver) {
					$('sysinfo-exec-ver').innerHTML = execver;
				}
				var execcrc = system_info.sysinfo.sysInfo.ExecCRC;
				if (execcrc) {
					$('sysinfo-exec-crc').innerHTML = execcrc;
				}
				var compver = system_info.sysinfo.sysInfo.CompVer;
				if (compver) {
					$('sysinfo-comp-ver').innerHTML = compver;
				}
				var appver = system_info.sysinfo.sysInfo.AppVersion;
				if (appver) {
					$('sysinfo-app-ver').innerHTML = appver;
				}
				var appcrc = system_info.sysinfo.sysInfo.AppCRC;
				if (appcrc) {
					$('sysinfo-app-crc').innerHTML = appcrc;
				}
				var siteid = system_info.sysinfo.sysInfo.SiteId;
				if (siteid) {
					$('sysinfo-site-id').innerHTML = siteid;
				}
				var datestamp = system_info.sysinfo.sysInfo.DateStamp;
				if (datestamp) {
					$('sysinfo-date-stamp').innerHTML = datestamp;
				}
			}
		} catch (h) {
			alert ("Incorrect board data");
			$('sysinfo-program').innerHTML = "";
			$('sysinfo-exec-ver').innerHTML = "";
			$('sysinfo-exec-crc').innerHTML = "";
		  $('sysinfo-comp-ver').innerHTML = "";
		  $('sysinfo-app-ver').innerHTML = "";
		 	$('sysinfo-app-crc').innerHTML = "";
		 	$('sysinfo-site-id').innerHTML = "";
			$('sysinfo-date-stamp').innerHTML = "";
			var errortable = $('sys-adjust-tbody-1');
			for(var i = errortable.rows.length - 1; i >= 0; i--){
				errortable.deleteRow(i);
			}
		}
	}

	document.observe("dom:loaded", function() {
		var boardtime = $('board-time');
		socket.on('system_info', function(system_info) {
				updatePage(system_info);
		});	

		if ($('info_clear_btn')) {
			$('info_clear_btn').observe( "click", function() {
				socket.emit('get_system_info');
				socket.on('system_info', function(system_info) {
						updatePage(system_info);
				});
		});
		}

	});
})();
