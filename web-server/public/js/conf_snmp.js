(function() {
	var socket = io();
  var last_transport;
	document.observe("dom:loaded", function() {
		socket.emit('get-snmp-conf-data', function(objSNMPConf){
      if(objSNMPConf.status == "ok"){
        last_transport = objSNMPConf.data;
        BindData(objSNMPConf.data);
      }
      else{
        ShowAlert(objSNMPConf.data);
      }
		});

    function BindData(confData){
      document.getElementById('lbl-snmp-commname').innerHTML = "&lt;" + confData.CommunityName + "&gt;";
      document.getElementById('snmp-commname').value = confData.CommunityName;
      document.getElementById('lbl-snmp-syscontact').innerHTML = "&lt;" + confData.SysContact + "&gt;";
      document.getElementById('snmp-syscontact').value = confData.SysContact;
      document.getElementById('snmp-traps-target1.1').value = confData.DestinationIP1.split('.')[0];
      document.getElementById('snmp-traps-target1.2').value = confData.DestinationIP1.split('.')[1];
      document.getElementById('snmp-traps-target1.3').value = confData.DestinationIP1.split('.')[2];
      document.getElementById('snmp-traps-target1.4').value = confData.DestinationIP1.split('.')[3];
      document.getElementById('snmp-traps-target2.1').value = confData.DestinationIP2.split('.')[0];
      document.getElementById('snmp-traps-target2.2').value = confData.DestinationIP2.split('.')[1];
      document.getElementById('snmp-traps-target2.3').value = confData.DestinationIP2.split('.')[2];
      document.getElementById('snmp-traps-target2.4').value = confData.DestinationIP2.split('.')[3];
      document.getElementById('lbl-snmp-inform-delay').innerHTML = "&lt;" + confData.RenableTrapsDelay + "&gt;";
      document.getElementById('snmp-inform-delay').value = confData.RenableTrapsDelay;
    }

    $('info_cancel_changes_btn').observe('click',function(){
      if(last_transport){
        BindData(last_transport);
      }
      else{
        socket.emit('get-snmp-conf-data', function(objSNMPConf){
          if(objSNMPConf.status == "ok"){
            last_transport = objSNMPConf.data;
            BindData(objSNMPConf.data);
          }
          else{
            ShowAlert(objSNMPConf.data);
          }
        });
      }
    });
	});
})();
