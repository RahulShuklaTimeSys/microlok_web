var dropdownIDList = [];
var last_transport;
var socket = io();
var changePasswordPopup = new Popup('changePasswordPopup');

(function() {
  // Wait until dom loded

  socket.on('time', function(timeString) {
      if(document.getElementById('PCVCFG').innerHTML == "" && document.getElementById('PCNVCFG').innerHTML == ""){
  			var board_time = new Date(timeString);
        var time =  (board_time.getMonth()+1) + "/"
                  + board_time.getDate() + "/"
                  + board_time.getFullYear() + " "
                  + (board_time.getHours()<10?"0": (board_time.getHours() >= 12 ? (board_time.getHours() - 12 < 10 ? "0" + board_time.getHours() - 12 : board_time.getHours() - 12) : board_time.getHours())) + ":"
                  + (board_time.getMinutes()<10?"0":"") + board_time.getMinutes() + ":"
                  + (board_time.getSeconds()<10?"0":"") + board_time.getSeconds() + " "
                  + (board_time.getHours() >= 12 ? "PM" : "AM");
        document.getElementById('PCVCFG').innerHTML = time;
        document.getElementById('PCNVCFG').innerHTML = time;
      }
	});


  function BindData(){
    document.getElementById('lbl_current_logic_timeout').style.display = 'none';
    document.getElementById('lbl_current_synchronization_timeout').style.display = 'none';
    socket.emit('get_general_config_list', function(error, data) {
      if(error){
        ShowAlert("No data found for General Configuration.");
      }
      else{
        var configList = {};
        var logicTimeout;
        var delayReset;
        var synchronizationTimeout;
        var formmodified;
        last_transport = data.ConfigList;
        var objGeneralConfig = GetFromLocalStorage('objGeneralConfig') ? GetFromLocalStorage('objGeneralConfig') : (data.configSettings != "" ? data.configSettings : false);
        //document.getElementById('PCVCFG').innerHTML = data.last_vital_configuration;
        //document.getElementById('PCNVCFG').innerHTML = data.last_non_vital_configuration;
        document.getElementById('txt_logic_timeout').disabled = document.getElementById('conf_save_changes_btn') ? false : true;
        if(document.getElementById('txt_logic_timeout').disabled)
          document.getElementById('div_LogicTimeout').classList.add('is-disabled');
        document.getElementById('txt_logic_timeout').value = objGeneralConfig ? objGeneralConfig.logicTimeout : data.configData['A.LOGIC_TIMEOUT'].split(':')[0];
        document.getElementById('lbl_current_logic_timeout').innerHTML = '[current: '+ (data.configSettings != "" ? data.configSettings.logicTimeout : data.configData['A.LOGIC_TIMEOUT'].replace(':MSEC','')) +']';
        document.getElementById('lbl_default_logic_timeout').innerHTML = "[default: "+ data.configData['A.LOGIC_TIMEOUT'].replace(':MSEC','') +"]";
        document.getElementById('txt_delay_reset').value = objGeneralConfig ? objGeneralConfig.delayReset : data.configData['F.DELAY_RESET'].split(':')[0];
        document.getElementById('lbl_default_delay_reset').innerHTML = "[default: "+ data.configData['F.DELAY_RESET'].replace(':MSEC','') +"]";
        document.getElementById('txt_synchronization_timeout').disabled = document.getElementById('conf_save_changes_btn') ? false : true;
        if(document.getElementById('txt_synchronization_timeout').disabled)
          document.getElementById('div_SynchronizationTimeout').classList.add('is-disabled');
        document.getElementById('txt_synchronization_timeout').value = objGeneralConfig ? objGeneralConfig.synchronizationTimeout : data.configData['F.SYNCHRONIZATION_TIMEOUT'].split(':')[0];
        document.getElementById('lbl_current_synchronization_timeout').innerHTML = '[current: '+ (data.configSettings != "" ? data.configSettings.synchronizationTimeout : data.configData['F.SYNCHRONIZATION_TIMEOUT'].replace(':MSEC','')) +']';
        document.getElementById('lbl_default_synchronization_timeout').innerHTML = "[default: "+ data.configData['F.SYNCHRONIZATION_TIMEOUT'].replace(':MSEC','') +"]";
        Object.keys(data.configData['USER.NUMERIC']).forEach(function(key) {
          dropdownIDList.push('select'+ key);
          var container = document.getElementById('options_list');
          var node = document.createElement('div');
          node.id = "div" + key;
          node.classList.add("general-configuration-dynamic-list-wrapper");
          node.innerHTML = '<label style="padding-top:5px;width:60px;vertical-align:middle;display:inline-block;font-size:12px;font-weight:100;">Event '+ key.replace('User_Num_','') +' : </label>'+
                           '<img style="padding: 5px 5px 0px 5px; margin-bottom: -2px; vertical-align:bottom" src="public/img/accept.png"></img>'+
                           '<select id="select'+ key +'"style="width:40px;height:20px;margin-right:5px">'+
                           '<option value="0">0</option>'+
                           '<option value="1">1</option>'+
                           '<option value="2">2</option>'+
                           '<option value="3">3</option>'+
                           '</select>'+
                           '<label id="current_select'+ key +'" style="font-size:85%;display:none;font-weight:100;">[current: '+ (data.configSettings != "" ? data.configSettings.configList[key] : data.configData['USER.NUMERIC'][key].replace('Initial.value=','')) +']</label>'+
                           '<label id="default_select'+ key +'" style="font-size:85%;color:#580f0f;font-weight:100;">[default: '+ data.configData['USER.NUMERIC'][key].replace('Initial.value=','') +']</label>';
          container.appendChild(node);
          var ddl = document.getElementById('select'+ key);
          ddl.value = objGeneralConfig ? objGeneralConfig.configList[key] : data.configData['USER.NUMERIC'][key].replace('Initial.value=','');
          ddl.disabled = document.getElementById('conf_save_changes_btn') ? false : true;
          ddl.addEventListener("change", function(e){
            var ddl = document.getElementById(e.target.id);
            if(dropdownIDList.length > 0){
              dropdownIDList.forEach(function(ddlID){
                configList[ddlID.replace('select','')] = document.getElementById(ddlID).value;
              });
            }
              formmodified = "1";
              var objGeneralConfig = GetGeneralConfigObject(logicTimeout,delayReset,synchronizationTimeout,formmodified,configList);
              SaveToLocalStorage("objGeneralConfig" , objGeneralConfig);
              if(document.getElementById('current_' + e.target.id).innerHTML.indexOf(ddl.options[ddl.selectedIndex].text) > 0){
                document.getElementById('current_' + e.target.id).style.display= 'none';
              }
              else {
                document.getElementById('current_' + e.target.id).style.display= 'inline-block';
              }
          });
        });
      }
    });
  }

  function GetGeneralConfigObject(logicTimeout,delayReset,synchronizationTimeout,formmodified,configList){
    var GeneralConfigObject = {
      "configList" : "",
      "logicTimeout" : "",
      "delayReset" : "",
      "synchronizationTimeout" : "",
      "type" : "vital",
      "name" : "General Configuration",
      "formmodified" : ""
    };
      if(configList==undefined){
        var configList = {};
        if(dropdownIDList.length > 0){
          dropdownIDList.forEach(function(ddlID){
            configList[ddlID.replace('select','')] = document.getElementById(ddlID).value;
            //configList.push({"id": ddlID.replace('select',""), "value" : document.getElementById(ddlID).value});
          });
          GeneralConfigObject.configList = configList;
      }
    }
      else{
        GeneralConfigObject.configList = configList;
      }
      GeneralConfigObject.logicTimeout = logicTimeout==undefined?document.getElementById('txt_logic_timeout').value:logicTimeout;
      GeneralConfigObject.delayReset = delayReset==undefined?document.getElementById('txt_delay_reset').value:delayReset;
      GeneralConfigObject.synchronizationTimeout = synchronizationTimeout==undefined?document.getElementById('txt_synchronization_timeout').value:synchronizationTimeout;
      GeneralConfigObject.formmodified = formmodified;
      console.log(JSON.stringify(GeneralConfigObject));
      return GeneralConfigObject;
  }

  function SetCurrentLabel(e, textboxId, currentLabelId, defaultLabelId){
    if(document.getElementById(currentLabelId).innerHTML.indexOf(document.getElementById(textboxId).value) > 0){
      document.getElementById(currentLabelId).style.display = 'none';
    }
    else {
      document.getElementById(currentLabelId).style.display = '';
    }
  }

  function IsValid(oldPassword, newPassword, confirmNewPassword, callback){
    if(oldPassword === newPassword){
      document.getElementById('password_failure').style.display = '';
      document.getElementById('password_failure').querySelectorAll('label')[0].innerHTML = 'FAILURE: New password should not match old one.';
      callback(false);
    }
    else if (newPassword !== confirmNewPassword){
      document.getElementById('password_failure').style.display = '';
      document.getElementById('password_failure').querySelectorAll('label')[0].innerHTML = 'FAILURE: New password and confirm password doesn`t match.';
      callback(false);
    }
    else if(newPassword.length > 12){
      document.getElementById('password_failure').style.display = '';
      document.getElementById('password_failure').querySelectorAll('label')[0].innerHTML = 'FAILURE: New password should contain not more than 12 characters.';
      callback(false);
    }
    else{
      isAccepted(oldPassword, newPassword, callback);
    }
  }

  function isAccepted(currentPass, newPassword, callback){
    socket.emit('validate_current_password', currentPass, newPassword, function(result) {
      if(!result){
        document.getElementById('password_failure').style.display = '';
        document.getElementById('password_failure').querySelectorAll('label')[0].innerHTML = 'FAILURE: Current password is not accepted. Please try again.';
        callback(false);
      }
      else
        callback(true);
    });
  }

  document.observe("dom:loaded", function() {
    var element = document.getElementById("aclist");
  	element.classList.add("active");
    var logicTimeout;
    var delayReset;
    var synchronizationTimeout;
    var configList;
    var formmodified;
    // var checkbutton;
    var checkbutton = document.getElementById('conf_save_changes_btn') ? false : true;

    // RemoveFromLocalStorage('objGeneralConfig');
    $('txt_logic_timeout').observe('change', function(e){
    var logicTimeout = $(this).value;
    formmodified="1";
    var objGeneralConfig = GetGeneralConfigObject(logicTimeout,delayReset,synchronizationTimeout,formmodified,configList);
    SaveToLocalStorage("objGeneralConfig" , objGeneralConfig);
    SetCurrentLabel(e, 'txt_logic_timeout', 'lbl_current_logic_timeout', 'lbl_default_logic_timeout');
    });
    $('txt_delay_reset').observe('change', function(){
    var delayReset = $(this).value;
    formmodified="1";
    var objGeneralConfig = GetGeneralConfigObject(logicTimeout,delayReset,synchronizationTimeout,formmodified,configList);
    SaveToLocalStorage("objGeneralConfig" , objGeneralConfig);
    });
    $('txt_synchronization_timeout').observe('change', function(e){
    var synchronizationTimeout = $(this).value;
    formmodified="1";
    var objGeneralConfig = GetGeneralConfigObject(logicTimeout,delayReset,synchronizationTimeout,formmodified,configList);
    SaveToLocalStorage("objGeneralConfig" , objGeneralConfig);
    SetCurrentLabel(e, 'txt_synchronization_timeout', 'lbl_current_synchronization_timeout', 'lbl_default_synchronization_timeout');
    });
    BindData();
    if(checkbutton){
    }
    else{
    $('conf_save_changes_btn').observe('click',function(){
      document.location = "submit_data";
    });
    $('change-pwd').observe('click',function(){
      changePasswordPopup.show({
                   title : 'Change Password',
                   message : ('<div class="field-group">'+
                                '<div class="field-container text clearfix" style="display: none;" id="password_failure">'+
                                ' <label style="color: #AA3333;">FAILURE: Cannot connect to server. Check your network connection and try again.</label>'+
                                '</div>'+
                                '<div class="field-container text clearfix">'+
                                ' <label for="cur_password">Current password:</label>'+
                                ' <div id="cur_password_container" class="iw">'+
                                '   <input id="cur_password" name="cur_password" maxlength="12" type="password">'+
                                ' </div>'+
                                '</div>'+
                                '<div class="field-container text clearfix">'+
                                ' <label for="new_password">New password:</label>'+
                                ' <div id="new_password_container" class="iw">'+
                                '   <input id="new_password" name="new_password" maxlength="12" type="password">'+
                                ' </div>'+
                                '</div>'+
                                '<div class="field-container text clearfix">'+
                                ' <label for="ver_password">Verify new password:</label>'+
                                ' <div id="ver_password_container" class="iw">'+
                                '   <input id="ver_password" name="ver_password" maxlength="12" type="password">'+
                                ' </div>'+
                                '</div>'+
                              '</div>'),
                   width : '370px',
                   buttons : [ {
                               title : 'Cancel',
                               action : function() {
                                          changePasswordPopup.hide();
                                        }
                               },
                               {
                                title : 'Change',
                                action : function(){
                                          submitFunction();
                                         }
                                } ]
                 });

                 $('ver_password').observe('keypress', function(evt) {
                   if (evt.keyCode == 13) {
                     submitFunction();
                   }
                 });
                 $('new_password').observe('keypress', function(evt) {
                   if (evt.keyCode == 13) {
                     submitFunction();
                   }
                 });
                 $('cur_password').observe('keypress', function(evt) {
                   if (evt.keyCode == 13) {
                     submitFunction();
                   }
                 });
    });
  }
    function submitFunction(){
      //validate and perform action.
      document.getElementById('password_failure').style.display = 'none';
      var currentPass = document.getElementById('cur_password').value.trim();
      var newPass = document.getElementById('new_password').value.trim();
      var confirmNewPass = document.getElementById('ver_password').value.trim();
      IsValid(currentPass, newPass, confirmNewPass,function(result){
        if(result){
          SaveToLocalStorage('objNewPassword',{'currentPass' : currentPass, 'newPass' : newPass});
          changePasswordPopup.hide();
        }
      });
    }

  });
})();
