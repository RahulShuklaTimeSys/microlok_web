var socket = io();
(function() {
	var board_time = null;
	var update_time_interval = null;
	var socket = io();
	socket.on('time', function(timeString) {
			board_time = new Date(timeString);
			computer_time = new Date();
			updateTime();
	});
	function updateTime() {
       	board_time.setSeconds(board_time.getSeconds()+1);
       	$('cursystime').value = (board_time.getMonth()+1) + "/"
       				+ board_time.getDate() + "/"
       				+ board_time.getFullYear() + " "
       				+ (board_time.getHours()<10?"0":"") + board_time.getHours() + ":"
       				+ (board_time.getMinutes()<10?"0":"") + board_time.getMinutes() + ":"
       				+ (board_time.getSeconds()<10?"0":"") + board_time.getSeconds();
        $('curpctime').value = (computer_time.getMonth()+1) + "/"
       				+ computer_time.getDate() + "/"
       				+ computer_time.getFullYear() + " "
       				+ (computer_time.getHours()<10?"0":"") + computer_time.getHours() + ":"
       				+ (computer_time.getMinutes()<10?"0":"") + computer_time.getMinutes() + ":"
       				+ (computer_time.getSeconds()<10?"0":"") + computer_time.getSeconds();
			       	$('cursystime').fire("time:changed");
              $('curpctime').fire("time:changed");

	}
	document.observe("dom:loaded", function() {
    var element = document.getElementById("sclist");
  	element.classList.add("active");
		var divcustomelement = document.getElementById("divcustomtime");
		var divpcelement = document.getElementById("divcurpctime");
		divcustomelement.classList.add("is-disabled");
		document.getElementById("setfromcurpctime").checked=true;
		document.getElementById("showcomptime").style.display = "block";
		$('customtime').getAttribute('readonly', true);
		var boardtime = $('cursystime');
    var pctime = $('curpctime');
		socket.emit('ntpserver', function(error,data){
			var p = document.getElementById('NTP_Address');
			p.value = "";
			for(var i=0;i<data.length;i++){
				p.value = p.value + data[i].Address + ",";
			}
		});
		socket.emit('timezone', function(error,data){
			var p = document.getElementById('TimeZone');
			if ( data == '-12' )
					p.selectedIndex = 0;
			else if ( data == '-11' )
					p.selectedIndex = 1;
			else if ( data == '-10' )
					p.selectedIndex = 2;
			else if ( data == '-9' )
					p.selectedIndex = 3;
			else if ( data == '-8' )
					p.selectedIndex = 4;
			else if ( data == '-7' )
					p.selectedIndex = 5;
			else if ( data == '-6' )
					p.selectedIndex = 6;
			else if ( data == '-5' )
					p.selectedIndex = 7;
			else if ( data == '-4' )
					p.selectedIndex = 8;
			else if ( data == '-3.5' )
					p.selectedIndex = 9;
			else if ( data == '-3' )
					p.selectedIndex = 10;
			else if ( data == '-2' )
					p.selectedIndex = 11;
			else if ( data == '-1' )
					p.selectedIndex = 12;
			else if ( data == '0' )
					p.selectedIndex = 13;
			else if ( data == '1' )
					p.selectedIndex = 14;
			else if ( data == '2' )
					p.selectedIndex = 15;
			else if ( data == '3' )
					p.selectedIndex = 16;
			else if ( data == '3.5' )
					p.selectedIndex = 17;
			else if ( data == '4' )
					p.selectedIndex = 18;
			else if ( data == '4.5' )
					p.selectedIndex = 19;
			else if ( data == '5' )
					p.selectedIndex = 20;
			else if ( data == '5.5' )
					p.selectedIndex = 21;
			else if ( data == '5.45' )
					p.selectedIndex = 22;
			else if ( data == '6' )
					p.selectedIndex = 23;
			else if ( data == '7' )
					p.selectedIndex = 24;
			else if ( data == '8' )
					p.selectedIndex = 25;
			else if ( data == '9' )
					p.selectedIndex = 26;
			else if ( data == '9.5' )
					p.selectedIndex = 27;
			else if ( data == '10' )
					p.selectedIndex = 28;
			else if ( data == '11' )
					p.selectedIndex = 29;
			else if ( data == '12' )
					p.selectedIndex = 30;
			else if ( data == '13' )
					p.selectedIndex = 31;
		});
		boardtime.observe("time:updated", function() {
		});
    pctime.observe("time:updated", function() {
		});
		jQuery('input[name="setmicroloktime"]').on('click', function() {
				if ($(this).value == 'pctime') {
					    document.getElementById("showcomptime").style.display = "block";
							document.getElementById("firstdiv").style.display = "none";
							document.getElementById("seconddiv").style.display = "none";
							document.getElementById("thirddiv").style.display = "none";
							document.getElementById("Time_Type1").checked=false;
							document.getElementById("Time_Type0").checked=false;
				}
				else {
					  document.getElementById("showcomptime").style.display = "none";
						document.getElementById("firstdiv").style.display = "block";
						document.getElementById("Time_Type1").checked=false;
						document.getElementById("Time_Type0").checked=false;
				}
		});
		jQuery('input[name="Time_Type"]').on('click', function() {
				if ($(this).value == 'Manual') {
					    document.getElementById("showcomptime").style.display = "none";
							document.getElementById("seconddiv").style.display = "block";
							document.getElementById("thirddiv").style.display = "none";
							divcustomelement.classList.remove("is-disabled");
							jQuery('#customtime').attr('readonly', false);
							jQuery('#customtime').datetimepicker({
								format: 'MM/DD/YYYY HH:mm:ss'
							});
				}
				else {
					  document.getElementById("showcomptime").style.display = "none";
						document.getElementById("firstdiv").style.display = "block";
						document.getElementById("seconddiv").style.display = "none";
						document.getElementById("thirddiv").style.display = "block";
						document.getElementById("NTP_Address").getAttribute("disabled", true)
				}
		});
		$('set_mlk_time').observe('click', function() {
			if (document.getElementById('setfromcurpctime').checked) {
  			checktime = $('curpctime').value;
				var curpcutctime = new Date(checktime).toISOString();
				SaveToLocalStorage("objDateTimeConfig" , curpcutctime);
				socket.emit('timeset', {'message': curpcutctime});
				alert("Date Updated.");
				}
			else if(document.getElementById('Time_Type1').checked){
					checktime = $('customtime').value;
					if(checktime == ""){
						alert("Please fill date/time");
						return;
					}
					else{
					var customutctime = new Date(checktime).toISOString();
					SaveToLocalStorage("objDateTimeConfig" , customutctime);
					socket.emit('timeset', {'message': customutctime});
					alert("Manually date updated.");
					}
				}
			else{
				  checktime = document.getElementById('TimeZone').value;
				  alert("NTP date-time updated.Please reboot the board to reflect the changes");
			}
		});
	});
})();
