(function() {
	var board_time = null;
	var update_time_interval = null;
	var socket = io();
	socket.on('time', function(timeString) {
			board_time = new Date(timeString);
			updateTime();
	});
	function updateTime() {		
       	board_time.setSeconds(board_time.getSeconds()+1);
       	$('board-time').value = (board_time.getMonth()+1) + "/"
       				+ board_time.getDate() + "/"
       				+ board_time.getFullYear() + " "
       				+ (board_time.getHours()<10?"0":"") + board_time.getHours() + ":"
       				+ (board_time.getMinutes()<10?"0":"") + board_time.getMinutes() + ":"
       				+ (board_time.getSeconds()<10?"0":"") + board_time.getSeconds();
			       	$('board-time').fire("time:changed");
	}

	function updateHeader(transport) {
		try {
			if (transport.responseJSON) {
				if (update_time_interval) {
					clearInterval(update_time_interval);
				}
					var appname = transport.responseJSON.appname;
			//	$('board-id').value = appname;
			if (transport.responseJSON.vopstatus.length > 0) {
				  var opmode = transport.responseJSON.vopstatus;
					$('operation-mode').value = opmode;
					if (opmode == "CPS Up") {
						$('operation-mode').className = "operation-mode-on";
					} else {
						$('operation-mode').value = "CPS Up";
						$('operation-mode').className = "operation-mode-off";
					}
				} else {
					$('operation-mode').className = "operation-mode-unknown";
				}
					var swvernode = transport.responseJSON.SWVer;
					var elementList = document.getElementsByName("sw-ver");
				if (swvernode.length > 0) {
					for(var i=0; i<elementList.length; 	i++){
						elementList[i].innerHTML = swvernode;
					}
				} else {
					for(var i=0; i<elementList.length; 	i++){
						elementList[i].innerHTML = "0.9.100";
					}
				}

			} else {
			//	$('board-id').value= "data receiving error";
				$('operation-mode').className = "operation-mode-unknown";
				$('operation-mode').value = "---";
				$('board-time').value = "---";
			}
		} catch (h) {
		//	$('board-id').value= "data receiving error";
			$('operation-mode').className = "operation-mode-unknown";
			$('operation-mode').value = "---";
			$('board-time').value = "---";
		}
	}

	function updateRequest() {
		//ajax_overlay.show();
		var nocache = new Date().getTime();
		new Ajax.Request('/system-info',
		{
			method: 'get',
			parameters: {
				'nocache': nocache
			},
			onSuccess: function(transport){
				updateHeader(transport);
				//ajax_overlay.hide();
			},
			onFailure: function(transport){
				//ajax_overlay.hide();
			}
		});

	}


	document.observe("dom:loaded", function() {
		// initially hide all containers for tab content
		//$$('div.tabcontent').invoke('hide');
		//alert('dom:loaded');
		updateRequest();
		var boardtime = $('board-time');
		boardtime.observe("time:updated", function() {
			updateRequest();
		});
	});
})();
