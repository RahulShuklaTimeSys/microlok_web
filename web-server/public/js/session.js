var sessionTimeout;
var socket = io();

	function showSessionTimeout() {
		var sTimeout, expSeconds = 30;
		var sessionExpPopup = new Popup('sess-exp-popup');
		sessionExpPopup.show({
			title : 'Warning',
			message : 'Your session will expire in <strong id="sessexp">30</strong> seconds.',
			buttons : [ {
				title : 'Refresh and continue session',
				action : function() {
					location.reload();
				}
			} ]
		});
		sTimeout = setInterval(function() {
			expSeconds--;
			if (expSeconds <= 0) {
				clearInterval(sTimeout);
				socket.emit('expire-logged-in-session', function(response){
					if(response.status == "1"){
						window.localStorage.clear();
						window.onbeforeunload = null;
					}
				});
				sessionExpPopup.show({
					title : 'Warning',
					message : 'Session Expired. Refreshing page in 10 seconds...',
					buttons : [ {
						title : 'Refresh Page',
						action : function() {
				     	window.location.href = '/';
						}
					} ]
				});
				(function() {
					window.location.href = '/';
				}).delay(10);
			} else if ($('sessexp')) {
				$('sessexp').update(expSeconds);
			}
		}, 1000);
	}

	sessionTimeout = showSessionTimeout.delay(300 - 30);
