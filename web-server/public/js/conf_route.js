(function() {
	var socket = io();
	socket.emit('router');

	function BindBoardData(confData){
		for(i=0;i<confData.data.length;i++){
			if(confData.data[i].key.indexOf("CPU_BASE")>-1){
				var objCPUComm = GetFromLocalStorage('objCPUComm') ? GetFromLocalStorage('objCPUComm'):(confData.cpu_data != "" ? confData.cpu_data : "");
				document.getElementById("chk_cpu_base_etha").checked = objCPUComm?(objCPUComm.configList.CPU_BASE.ETHA["A.ENABLE"]==true)?true:false:confData.data[i].value.ETHA["A.ENABLE"] == "1" ? true : false;
				document.getElementById("chk_cpu_base_ethb").checked = objCPUComm?(objCPUComm.configList.CPU_BASE.ETHB["A.ENABLE"]==true)?true:false:confData.data[i].value.ETHB["A.ENABLE"] == "1" ? true : false;
				document.getElementById("cpu_base_etha_local_address_1A1").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"].split('.')[0]:confData.data[i].value.ETHA["A.LOCAL.ADDRESS"].split('.')[0];
				document.getElementById("cpu_base_etha_local_address_1A2").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"].split('.')[1]:confData.data[i].value.ETHA["A.LOCAL.ADDRESS"].split('.')[1];
				document.getElementById("cpu_base_etha_local_address_1A3").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"].split('.')[2]:confData.data[i].value.ETHA["A.LOCAL.ADDRESS"].split('.')[2];
				document.getElementById("cpu_base_etha_local_address_1A4").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.LOCAL.ADDRESS"].split('.')[3]:confData.data[i].value.ETHA["A.LOCAL.ADDRESS"].split('.')[3];
				document.getElementById("cpu_base_etha_subnet_mask_1A1").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.SUBNET.MASK"].split('.')[0]:confData.data[i].value.ETHA["A.SUBNET.MASK"].split('.')[0];
				document.getElementById("cpu_base_etha_subnet_mask_1A2").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.SUBNET.MASK"].split('.')[1]:confData.data[i].value.ETHA["A.SUBNET.MASK"].split('.')[1];
				document.getElementById("cpu_base_etha_subnet_mask_1A3").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.SUBNET.MASK"].split('.')[2]:confData.data[i].value.ETHA["A.SUBNET.MASK"].split('.')[2];
				document.getElementById("cpu_base_etha_subnet_mask_1A4").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.SUBNET.MASK"].split('.')[3]:confData.data[i].value.ETHA["A.SUBNET.MASK"].split('.')[3];
				document.getElementById("cpu_base_etha_gateway_1A1").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.GATEWAY"].split('.')[0]:confData.data[i].value.ETHA["A.GATEWAY"].split('.')[0];
				document.getElementById("cpu_base_etha_gateway_1A2").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.GATEWAY"].split('.')[1]:confData.data[i].value.ETHA["A.GATEWAY"].split('.')[1];
				document.getElementById("cpu_base_etha_gateway_1A3").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.GATEWAY"].split('.')[2]:confData.data[i].value.ETHA["A.GATEWAY"].split('.')[2];
				document.getElementById("cpu_base_etha_gateway_1A4").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHA["A.GATEWAY"].split('.')[3]:confData.data[i].value.ETHA["A.GATEWAY"].split('.')[3];
				document.getElementById("cpu_base_ethb_local_address_1A1").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"].split('.')[0]:confData.data[i].value.ETHB["A.LOCAL.ADDRESS"].split('.')[0];
				document.getElementById("cpu_base_ethb_local_address_1A2").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"].split('.')[1]:confData.data[i].value.ETHB["A.LOCAL.ADDRESS"].split('.')[1];
				document.getElementById("cpu_base_ethb_local_address_1A3").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"].split('.')[2]:confData.data[i].value.ETHB["A.LOCAL.ADDRESS"].split('.')[2];
				document.getElementById("cpu_base_ethb_local_address_1A4").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.LOCAL.ADDRESS"].split('.')[3]:confData.data[i].value.ETHB["A.LOCAL.ADDRESS"].split('.')[3];
				document.getElementById("cpu_base_ethb_subnet_mask_1A1").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.SUBNET.MASK"].split('.')[0]:confData.data[i].value.ETHB["A.SUBNET.MASK"].split('.')[0];
				document.getElementById("cpu_base_ethb_subnet_mask_1A2").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.SUBNET.MASK"].split('.')[1]:confData.data[i].value.ETHB["A.SUBNET.MASK"].split('.')[1];
				document.getElementById("cpu_base_ethb_subnet_mask_1A3").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.SUBNET.MASK"].split('.')[2]:confData.data[i].value.ETHB["A.SUBNET.MASK"].split('.')[2];
				document.getElementById("cpu_base_ethb_subnet_mask_1A4").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.SUBNET.MASK"].split('.')[3]:confData.data[i].value.ETHB["A.SUBNET.MASK"].split('.')[3];
				document.getElementById("cpu_base_ethb_gateway_1A1").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.GATEWAY"].split('.')[0]:confData.data[i].value.ETHB["A.GATEWAY"].split('.')[0];
				document.getElementById("cpu_base_ethb_gateway_1A2").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.GATEWAY"].split('.')[1]:confData.data[i].value.ETHB["A.GATEWAY"].split('.')[1];
				document.getElementById("cpu_base_ethb_gateway_1A3").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.GATEWAY"].split('.')[2]:confData.data[i].value.ETHB["A.GATEWAY"].split('.')[2];
				document.getElementById("cpu_base_ethb_gateway_1A4").value = objCPUComm?objCPUComm.configList.CPU_BASE.ETHB["A.GATEWAY"].split('.')[3]:confData.data[i].value.ETHB["A.GATEWAY"].split('.')[3];
			}
		}

	}

	function updateRoutingTable(router_info) {
				try {
					if ( router_info ) {
						var status = router_info.status;
						if (status == 0) {
							/*Clear field*/
							$('route-log-field').value = "";
							var routes = router_info.route;
							for (var i=0;i<routes.length;i++) {
								/*Add route to field*/
								$("route-log-field").value += routes[i];
							}
						} else {
							ShowAlert("Wrong data received. Refresh page to retry.");
						}
					} else {
						ShowAlert("Wrong data received. Refresh page to retry.");
					}
				} catch (h) {
					ShowAlert("Incorrect data received. Refresh page to retry.");
				}
	}

	function GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4){
		var ETHA_ADDRESS = ethaadd1+"."+ethaadd2+"."+ethaadd3+"."+ethaadd4;
		var ETHA_MASK = ethamask1+"."+ethamask2+"."+ethamask3+"."+ethamask4;
		var ETHA_GATEWAY = ethagateway1+"."+ethagateway2+"."+ethagateway3+"."+ethagateway4;
		var ETHB_ADDRESS = ethbadd1+"."+ethbadd2+"."+ethbadd3+"."+ethbadd4;
		var ETHB_MASK = ethbmask1+"."+ethbmask2+"."+ethbmask3+"."+ethbmask4;
		var ETHB_GATEWAY = ethbgateway1+"."+ethbgateway2+"."+ethbgateway3+"."+ethbgateway4;
    var CPUCommObject = {
			"configList" : "",
			"type" : "non-vital",
			"name":"CPU Communication"
		};
		var configList = {
			"CPU_BASE":""
		};
		CPUCommObject.configList = configList;
		var CPU_BASE = {};
		CPU_BASE["TYPE"] = "CPU_BASE";
		CPU_BASE["ETHA"]={};
		CPU_BASE["ETHB"]={};
		CPUCommObject.configList.CPU_BASE = CPU_BASE;
		var ETHA = {};
		ETHA["A.ENABLE"]=ethachk;
		ETHA["A.LOCAL.ADDRESS"]=ETHA_ADDRESS;
		ETHA["A.SUBNET.MASK"]=ETHA_MASK;
		ETHA["A.GATEWAY"]=ETHA_GATEWAY;
		CPUCommObject.configList.CPU_BASE.ETHA = ETHA;
		var ETHB = {};
		ETHB["A.ENABLE"]=ethbchk;
		ETHB["A.LOCAL.ADDRESS"]=ETHB_ADDRESS;
		ETHB["A.SUBNET.MASK"]=ETHB_MASK;
		ETHB["A.GATEWAY"]=ETHB_GATEWAY;
		CPUCommObject.configList.CPU_BASE.ETHB = ETHB;
		return CPUCommObject;
	}

	document.observe("dom:loaded", function() {
		var element = document.getElementById("sclist");
		var checkbutton = document.getElementById('conf_save_changes_btn') ? false : true;
		if(checkbutton){
			document.getElementById('chk_cpu_base_etha').disabled = true;
			document.getElementById('chk_cpu_base_ethb').disabled = true;
			document.getElementById('cpu_base_etha_local_address_1A1').disabled = true;
			document.getElementById('cpu_base_etha_local_address_1A2').disabled = true;
			document.getElementById('cpu_base_etha_local_address_1A3').disabled = true;
			document.getElementById('cpu_base_etha_local_address_1A4').disabled = true;
			document.getElementById('cpu_base_etha_subnet_mask_1A1').disabled = true;
			document.getElementById('cpu_base_etha_subnet_mask_1A2').disabled = true;
			document.getElementById('cpu_base_etha_subnet_mask_1A3').disabled = true;
			document.getElementById('cpu_base_etha_subnet_mask_1A4').disabled = true;
			document.getElementById('cpu_base_etha_gateway_1A1').disabled = true;
			document.getElementById('cpu_base_etha_gateway_1A2').disabled = true;
			document.getElementById('cpu_base_etha_gateway_1A3').disabled = true;
			document.getElementById('cpu_base_etha_gateway_1A4').disabled = true;
			document.getElementById('cpu_base_ethb_local_address_1A1').disabled = true;
			document.getElementById('cpu_base_ethb_local_address_1A2').disabled = true;
			document.getElementById('cpu_base_ethb_local_address_1A3').disabled = true;
			document.getElementById('cpu_base_ethb_local_address_1A4').disabled = true;
			document.getElementById('cpu_base_ethb_subnet_mask_1A1').disabled = true;
			document.getElementById('cpu_base_ethb_subnet_mask_1A2').disabled = true;
			document.getElementById('cpu_base_ethb_subnet_mask_1A3').disabled = true;
			document.getElementById('cpu_base_ethb_subnet_mask_1A4').disabled = true;
			document.getElementById('cpu_base_ethb_gateway_1A1').disabled = true;
			document.getElementById('cpu_base_ethb_gateway_1A2').disabled = true;
			document.getElementById('cpu_base_ethb_gateway_1A3').disabled = true;
			document.getElementById('cpu_base_ethb_gateway_1A4').disabled = true;
    }
    else{
    $('conf_save_changes_btn').observe('click',function(){
      document.location = "submit_data";
    });
		}
		element.classList.add("active");
		document.body.style.overflowY = 'visible';
		// document.getElementById('chk_cpu_base_etha').disabled = document.getElementById('conf_save_changes_btn') ? false : true;
		// if(document.getElementById('chk_cpu_base_etha').disabled)
		// 	document.getElementById('div_cpu_base_etha').classList.add('is-disabled');
		socket.on('router_info', function(router_info) {
				updateRoutingTable(router_info);
		});
		//ajax_overlay.show();
		//$('route_submit_btn').observe('click', executeCommand);
		$('route_submit_btn').observe('click', function(){
			var cmdin = document.getElementById('route-command').value.toString();
			socket.emit('cmd', {message : cmdin});
		});

		socket.on('err_cmd', function(){
			$('route-log-field').value = "";
			ShowAlert("Command executing error: Invalid Command");
		});
		socket.emit('get-board-conf-data');
		socket.on('board-conf-data', function(objBoardConf){
			if(objBoardConf.status == "ok"){
				BindBoardData(objBoardConf);
			}
			else{
				ShowAlert(objBoardConf.data);
			}
		});
		socket.emit('get-serial-port-data', function(error,data){
			if(error){
        ShowAlert("No data found for Serial Ports");
      }
			else{
				for(i=0;i<data.configData.length;i++){
					if(i==0){
						var select = document.getElementById('baud_rate_1');
						var option = document.createElement("option");
						option.innerHTML = data.configData[i].BAUD;
						select.appendChild(option);
						document.getElementById('stop_bits_1').disabled = true;
		        if(document.getElementById('stop_bits_1').disabled)
		          document.getElementById('div_stop_bits_1').classList.add('is-disabled');
						document.getElementById('stop_bits_1').value=data.configData[i].STOPBITS;
						var select = document.getElementById('parity_1');
						var option = document.createElement("option");
						option.innerHTML = data.configData[i].PARITY;
						select.appendChild(option);
						document.getElementById('key_on_delay_1').disabled = true;
		        if(document.getElementById('key_on_delay_1').disabled)
		          document.getElementById('div_key_on_delay_1').classList.add('is-disabled');
						document.getElementById('key_on_delay_1').value=data.configData[i]["KEY.ON.DELAY"];
						document.getElementById('key_off_delay_1').disabled = true;
		        if(document.getElementById('key_off_delay_1').disabled)
		          document.getElementById('div_key_off_delay_1').classList.add('is-disabled');
						document.getElementById('key_off_delay_1').value=data.configData[i]["KEY.OFF.DELAY"];
						document.getElementById('grant_delay_1').disabled = true;
		        if(document.getElementById('grant_delay_1').disabled)
		          document.getElementById('div_grant_delay_1').classList.add('is-disabled');
						document.getElementById('grant_delay_1').value=data.configData[i]["GRANT.DELAY"];
						document.getElementById('point_1_chk').disabled = true;
		        if(document.getElementById('point_1_chk').disabled)
		          document.getElementById('div_point_1').classList.add('is-disabled');
						document.getElementById("point_1_chk").checked = data.configData[i]["POINT.POINT"]==1?true:false;
					}
					else if(i==1){
						var select = document.getElementById('baud_rate_2');
						var option = document.createElement("option");
						option.innerHTML = data.configData[i].BAUD;
						select.appendChild(option);
						document.getElementById('stop_bits_2').disabled =true;
		        if(document.getElementById('stop_bits_2').disabled)
		          document.getElementById('div_stop_bits_2').classList.add('is-disabled');
						document.getElementById('stop_bits_2').value=data.configData[i].STOPBITS;
						var select = document.getElementById('parity_2');
						var option = document.createElement("option");
						option.innerHTML = data.configData[i].PARITY;
						select.appendChild(option);
						document.getElementById('key_on_delay_2').disabled = true;
		        if(document.getElementById('key_on_delay_2').disabled)
		          document.getElementById('div_key_on_delay_2').classList.add('is-disabled');
						document.getElementById('key_on_delay_2').value=data.configData[i]["KEY.ON.DELAY"];
						document.getElementById('key_off_delay_2').disabled = true;
		        if(document.getElementById('key_off_delay_2').disabled)
		          document.getElementById('div_key_off_delay_2').classList.add('is-disabled');
						document.getElementById('key_off_delay_2').value=data.configData[i]["KEY.OFF.DELAY"];
						document.getElementById('grant_delay_2').disabled = true;
		        if(document.getElementById('grant_delay_2').disabled)
		          document.getElementById('div_grant_delay_2').classList.add('is-disabled');
						document.getElementById('grant_delay_2').value=data.configData[i]["GRANT.DELAY"];
						document.getElementById('point_2_chk').disabled = true;
		        if(document.getElementById('point_2_chk').disabled)
		          document.getElementById('div_point_2').classList.add('is-disabled');
						document.getElementById("point_2_chk").checked = data.configData[i]["POINT.POINT"]==1?true:false;
					}
					else if(i==2){
						var select = document.getElementById('baud_rate_3');
						var option = document.createElement("option");
						option.innerHTML = data.configData[i].BAUD;
						select.appendChild(option);
						document.getElementById('stop_bits_3').disabled = true;
		        if(document.getElementById('stop_bits_3').disabled)
		          document.getElementById('div_stop_bits_3').classList.add('is-disabled');
						document.getElementById('stop_bits_3').value=data.configData[i].STOPBITS;
						var select = document.getElementById('parity_3');
						var option = document.createElement("option");
						option.innerHTML = data.configData[i].PARITY;
						select.appendChild(option);
						document.getElementById('key_on_delay_3').disabled = true;
		        if(document.getElementById('key_on_delay_3').disabled)
		          document.getElementById('div_key_on_delay_3').classList.add('is-disabled');
						document.getElementById('key_on_delay_3').value=data.configData[i]["KEY.ON.DELAY"];
						document.getElementById('key_off_delay_3').disabled = true;
		        if(document.getElementById('key_off_delay_3').disabled)
		          document.getElementById('div_key_off_delay_3').classList.add('is-disabled');
						document.getElementById('key_off_delay_3').value=data.configData[i]["KEY.OFF.DELAY"];
						document.getElementById('grant_delay_3').disabled = true;
		        if(document.getElementById('grant_delay_3').disabled)
		          document.getElementById('div_grant_delay_3').classList.add('is-disabled');
						document.getElementById('grant_delay_3').value=data.configData[i]["GRANT.DELAY"];
						document.getElementById('point_3_chk').disabled = true;
		        if(document.getElementById('point_3_chk').disabled)
		          document.getElementById('div_point_3').classList.add('is-disabled');
						document.getElementById("point_3_chk").checked=data.configData[i]["POINT.POINT"]==1?true:false;
					}
					else if(i==3){
						var select = document.getElementById('baud_rate_4');
						var option = document.createElement("option");
						option.innerHTML = data.configData[i].BAUD;
						select.appendChild(option);
						document.getElementById('stop_bits_4').disabled = true;
		        if(document.getElementById('stop_bits_4').disabled)
		          document.getElementById('div_stop_bits_4').classList.add('is-disabled');
						document.getElementById('stop_bits_4').value=data.configData[i].STOPBITS;
						var select = document.getElementById('parity_4');
						var option = document.createElement("option");
						option.innerHTML = data.configData[i].PARITY;
						select.appendChild(option);
						document.getElementById('key_on_delay_4').disabled = true;
		        if(document.getElementById('key_on_delay_4').disabled)
		          document.getElementById('div_key_on_delay_4').classList.add('is-disabled');
						document.getElementById('key_on_delay_4').value=data.configData[i]["KEY.ON.DELAY"];
						document.getElementById('key_off_delay_4').disabled = true;
		        if(document.getElementById('key_off_delay_4').disabled)
		          document.getElementById('div_key_off_delay_4').classList.add('is-disabled');
						document.getElementById('key_off_delay_4').value=data.configData[i]["KEY.OFF.DELAY"];
						document.getElementById('grant_delay_4').disabled = true;
		        if(document.getElementById('grant_delay_4').disabled)
		          document.getElementById('div_grant_delay_4').classList.add('is-disabled');
						document.getElementById('grant_delay_4').value=data.configData[i]["GRANT.DELAY"];
						document.getElementById('point_4_chk').disabled = true;
		        if(document.getElementById('point_4_chk').disabled)
		          document.getElementById('div_point_4').classList.add('is-disabled');
						document.getElementById("point_4_chk").checked=data.configData[i]["POINT.POINT"]==1?true:false;
					}
				}
			}
		});
		$('chk_cpu_base_etha').addEventListener('change', function(){
		var ethachk = $('chk_cpu_base_etha').checked;
		var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('chk_cpu_base_ethb').addEventListener('change', function(){
		var ethachk = $('chk_cpu_base_etha').checked;
		var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_local_address_1A1').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethaadd1 = $(this).value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_local_address_1A2').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethaadd2 = $(this).value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_local_address_1A3').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethaadd3 = $(this).value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_local_address_1A4').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethaadd4 = $(this).value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_subnet_mask_1A1').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethamask1 = $(this).value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_subnet_mask_1A2').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethamask2 = $(this).value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_subnet_mask_1A3').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethamask3 = $(this).value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_subnet_mask_1A4').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
    var ethamask4 = $(this).value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_gateway_1A1').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
		var ethagateway1 = $(this).value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_gateway_1A2').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
		var ethagateway2 = $(this).value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_gateway_1A3').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
		var ethagateway3 = $(this).value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_etha_gateway_1A4').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
		var ethagateway4 = $(this).value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
    var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
		var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
		var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
		var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
		var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
		var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
		var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
		var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
		var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
		var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
		var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
		var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_local_address_1A1').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd1 = $(this).value;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_local_address_1A2').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $(this).value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_local_address_1A3').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd3 = $(this).value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_local_address_1A4').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd4 = $(this).value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_subnet_mask_1A1').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $(this).value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var ethamask1 =$('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_subnet_mask_1A2').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $(this).value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_subnet_mask_1A3').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $(this).value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_subnet_mask_1A4').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $(this).value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
    var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_gateway_1A1').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $(this).value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_gateway_1A2').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $(this).value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_gateway_1A3').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $(this).value;
			var ethbgateway4 = $('cpu_base_ethb_gateway_1A4').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
    var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
		$('cpu_base_ethb_gateway_1A4').observe('keyup', function(){
			var ethachk = $('chk_cpu_base_etha').checked;
			var ethbchk = $('chk_cpu_base_ethb').checked;
			var ethbadd2 = $('cpu_base_ethb_local_address_1A2').value;
			var ethbadd1 = $('cpu_base_ethb_local_address_1A1').value;
			var ethbadd3 = $('cpu_base_ethb_local_address_1A3').value;
			var ethbadd4 = $('cpu_base_ethb_local_address_1A4').value;
			var ethbmask1 = $('cpu_base_ethb_subnet_mask_1A1').value;
			var ethbmask2 = $('cpu_base_ethb_subnet_mask_1A2').value;
			var ethbmask3 = $('cpu_base_ethb_subnet_mask_1A3').value;
			var ethbmask4 = $('cpu_base_ethb_subnet_mask_1A4').value;
			var ethbgateway2 = $('cpu_base_ethb_gateway_1A2').value;
			var ethbgateway1 = $('cpu_base_ethb_gateway_1A1').value;
			var ethbgateway3 = $('cpu_base_ethb_gateway_1A3').value;
			var ethbgateway4 = $(this).value;
		var ethagateway4 = $('cpu_base_etha_gateway_1A4').value;
		var ethagateway1 = $('cpu_base_etha_gateway_1A1').value;
		var ethagateway2 = $('cpu_base_etha_gateway_1A2').value;
		var ethagateway3 = $('cpu_base_etha_gateway_1A3').value;
    var ethamask1 = $('cpu_base_etha_subnet_mask_1A1').value;
		var ethamask2 = $('cpu_base_etha_subnet_mask_1A2').value;
		var ethamask3 = $('cpu_base_etha_subnet_mask_1A3').value;
		var ethamask4 = $('cpu_base_etha_subnet_mask_1A4').value;
		var ethaadd1 = $('cpu_base_etha_local_address_1A1').value;
		var ethaadd2 = $('cpu_base_etha_local_address_1A2').value;
		var ethaadd3 = $('cpu_base_etha_local_address_1A3').value;
		var ethaadd4 = $('cpu_base_etha_local_address_1A4').value;
    var objCPUComm = GetCPUCommObject(ethachk,ethbchk,ethaadd1,ethaadd2,ethaadd3,ethaadd4,ethamask1,ethamask2,ethamask3,ethamask4,ethagateway1,ethagateway2,ethagateway3,ethagateway4,ethbadd1,ethbadd2,ethbadd3,ethbadd4,ethbmask1,ethbmask2,ethbmask3,ethbmask4,ethbgateway1,ethbgateway2,ethbgateway3,ethbgateway4);
    SaveToLocalStorage("objCPUComm" , objCPUComm);
    });
	});
})();
