(function() {
	var socket = io();
	var last_transport;
	function GetBoardConfigObject(data,confData,confnewData,check_enabled){
		var objBoardConfig = GetFromLocalStorage('objBoardConfig');
		var boardtype = data.selectedBoardData.TYPE;
		if(objBoardConfig){
			objBoardConfig.configList[boardtype]["A.ENABLE"] = check_enabled;
			objBoardConfig.configList[boardtype]["TYPE"] =	boardtype;
			return objBoardConfig;
	}
	else{
		var BoardConfigObject = {
			"configList" : "",
			"type" : "vital",
			"name":"Board Configuration"
		};
		var configList = {};
		configList[boardtype] = {};
		if(confnewData!=""){
			confnewData.configList[boardtype]["A.ENABLE"]=check_enabled;
			confnewData.configList[boardtype]["TYPE"] =	boardtype;
			BoardConfigObject.configList = confnewData.configList;
		}
		else{
		for(var i=0;i<confData.length;i++){
			if(confData[i].key.indexOf("CPU_BASE")>-1){}
			else{
			if(confData[i].value["TYPE"]==boardtype){
				configList[boardtype]["A.ENABLE"]=check_enabled;
				configList[boardtype]["TYPE"] =	boardtype;
			}
			else{
				var newboardtype = confData[i].value["TYPE"];
				configList[newboardtype] = {};
				configList[newboardtype]["A.ENABLE"]=confData[i].value["A.ENABLE"];
				configList[newboardtype]["TYPE"] =	confData[i].value["TYPE"];
			}
			}
		}
		BoardConfigObject.configList = configList;
		}
		return BoardConfigObject;
	}
}
	document.observe("dom:loaded", function() {
	  // RemoveFromLocalStorage("objBoardConfig");
		var element = document.getElementById("aclist");
		element.classList.add("active");
		socket.emit('get-board-conf-data');
		socket.on('board-conf-data', function(objBoardConf){
			if(objBoardConf.status == "ok"){
				last_transport = objBoardConf.data;
				if(objBoardConf.new_data!=null){
					last_transport_new = objBoardConf.new_data;
				}
				else{
					last_transport_new = "";
				}
				BindData(objBoardConf.data);
			}
			else{
				ShowAlert(objBoardConf.data);
			}
		});
    function BindData(confData){
			var select = $('boardselector');
			select.innerHTML = "";
			var option = document.createElement("option");
			option.innerHTML = "";
			select.appendChild(option);
			for (var i=0; i < confData.length; i++)
			{
				if(confData[i].key.indexOf("CPU_BASE")>-1){}
				else{
				var option = document.createElement("option");
				option.value = confData[i].key.replace('Board.','');
				option.innerHTML = confData[i].key.replace('Board.','').trim().split(".")[1];
				select.appendChild(option);
				}
			}
		}
		var checkbutton = document.getElementById('conf_save_changes_btn') ? false : true;
		if(checkbutton){
		}
		else{
			$('conf_save_changes_btn').observe('click',function(){
				document.location = "submit_data";
			});
		}

		function bind_board_data(selectedBoardData,selectedLink,confData,confnewData){
			document.getElementById('all_board_data').innerHTML = "";
				var container = document.getElementById('all_board_data');
				var node = document.createElement('div');
				node.innerHTML='<div style="margin: 0 10px 10px 0" class="field-group clearfix">'+
											 '<div style="margin: 10px 0;  text-align: center; padding-bottom: 13px;">'+
											 '<div style="padding:5px 0px 10px 10px; clear: both">'+
											 '<div>'+
											 '<label style="font-size:13px;border-bottom: #E8EEF1 1px solid;width:100%;padding-right:800px;padding-bottom:6px;" id="lbl'+selectedBoardData.TYPE+'">'+selectedLink.split(".")[1]+ '(' + selectedBoardData["TYPE"] + ')'+'</label>'+
											 '</div>'+
											 '<label style="width:143px;display:inline-block; margin-top: 7px; margin-right:10px; float: left;font-size:12px;font-weight:100;">address:300(8-bit VPA)</label>'+
											 '</div>'+
											 '</div>'+
											 '</div>'+
						 				 	 '<div class="field-group clearfix" style="margin: 0 10px 10px 0">'+
											 '<div style="margin: 10px 0;  text-align: center; padding-bottom: 13px;">'+
											 '<div style="padding:5px 0px 10px 10px; clear: both">'+
											 '<div>'+
											 '<label style="font-size:13px;border-bottom: #E8EEF1 1px solid;width:100%;padding-right:852px;padding-bottom:6px;">General</label>'+
											 '</div>'+
											 '<label style="width:58px; display:inline-block; margin-top: 3px; margin-right:10px; float: left;font-size:12px;font-weight:100;">Enable:</label>'+
											 '<img id="img'+selectedBoardData.TYPE+'" style="vertical-align:bottom; matgin-bottom:-2px;float: left; margin-right:10px" src="public/img/Warning.png">'+
											 '<div id="div'+selectedBoardData.TYPE+'" style="width: 0px; height:20px; float: left; margin: 0 5px; padding-right:5px">'+
											 '<input type="checkbox" id="chk'+selectedBoardData.TYPE+'" name="chk'+selectedBoardData.TYPE+'" style="margin-right:5px;float:left; margin-top:5px">'+
						 				 	 '</div>'+
											 '<div style="width: 120px; height:20px; float: left;">'+
											 '<label id="current'+selectedBoardData.TYPE+'" style="font-size:85%; margin-top: 5px; display:none;font-size:12px;font-weight:100;">(current: 4000)</label>'+
											 '<label id="default'+selectedBoardData.TYPE+'" style="font-size:85%;coImplementationIDlor:#580f0f; margin-top: 5px; display:inline-block;font-size:12px;font-weight:100;">(default: set)</label>'+
											 '</div>'+
											 '</div>'+
											 '</div>'+
											 '</div>';
				container.appendChild(node);
				node.onclick = selected_checkbox_value(this,confData,confnewData);
		}

		function BindBoardScreenData(selectedLink){
			if(selectedLink != ""){
				var confData = last_transport;
				if(last_transport_new!=""){
				var confnewData = last_transport_new;
				}
				else{
					var confnewData = "";
				}
				var index = confData.map(function(o) { return o.key; }).indexOf('Board.' +selectedLink);
				selectedBoardData = confData[index].value;
				bind_board_data(selectedBoardData,selectedLink,confData,confnewData);
		}
	}
		$('boardselector').observe('change',function(){
			selectedLink = this.value;
			BindBoardScreenData(selectedLink);
		});
		function selected_checkbox_value(data,confData,confnewData){
			if(confnewData!=""){
				var objBoardConfig = GetFromLocalStorage('objBoardConfig')? GetFromLocalStorage('objBoardConfig') : confnewData;
				if((objBoardConfig.configList!=undefined && objBoardConfig.configList[data.selectedBoardData.TYPE]!=undefined) && (objBoardConfig.configList[data.selectedBoardData.TYPE].TYPE == data.selectedBoardData.TYPE)){
				document.getElementById('chk'+data.selectedBoardData.TYPE+'').checked = objBoardConfig.configList[data.selectedBoardData.TYPE]["A.ENABLE"] == true ? true : false;
				}
			}
			else{
			var objBoardConfig = GetFromLocalStorage('objBoardConfig')? GetFromLocalStorage('objBoardConfig') : confData;
			if((objBoardConfig.configList!=undefined && objBoardConfig.configList[data.selectedBoardData.TYPE]!=undefined) && (objBoardConfig.configList[data.selectedBoardData.TYPE].TYPE == data.selectedBoardData.TYPE)){
			document.getElementById('chk'+data.selectedBoardData.TYPE+'').checked = objBoardConfig.configList[data.selectedBoardData.TYPE]["A.ENABLE"] == true ? true : false;
			}
			else{
				document.getElementById('chk'+data.selectedBoardData.TYPE+'').checked = data.selectedBoardData["A.ENABLE"]=="0" ? false:true;
			}
			}
			var checkbit = document.getElementById('chk'+data.selectedBoardData.TYPE+'');
			checkbit.addEventListener("change", function(e){
				var check_enabled = checkbit.checked;
				var objBoardConfig = GetBoardConfigObject(data,confData,confnewData,check_enabled);
				SaveToLocalStorage("objBoardConfig" , objBoardConfig);
			});
			document.getElementById('chk'+data.selectedBoardData.TYPE+'').disabled = document.getElementById('conf_save_changes_btn') ? false : true;
		}
	});
})();
