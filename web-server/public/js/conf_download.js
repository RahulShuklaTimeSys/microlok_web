(function() {

var socket = io();
var fileTodownload;
var popup = new Popup('download-progress');
popup.hide();

document.observe("dom:loaded", function() {
  var element = document.getElementById("smlist");
	element.classList.add("active");
  socket.emit('get_json_file_list', function(err, fileList) {
    if(err){
      popup.show({
              title : 'Error',
              message : ('<p style="text-align:center">' + err + '</p>')
                  });
    }
    else{
      var obj = JSON.parse(fileList);
      obj.sort(compare);
      var select = $('downloadtype');
      var option = document.createElement("option");
      option.value = '';
      option.innerHTML = '';
      select.appendChild(option);
      for (var i=0; i < obj.length; i++)
      {
        var option = document.createElement("option");
        //option.id = obj[i];
        option.value = obj[i].value;
        option.innerHTML = obj[i].text;
        select.appendChild(option);
      }
    }
  });

  $('downloadtype').observe('change',function() {
    setDownloadFile($('downloadtype').value);
  });

  $('download').observe('click',function() {
    if(fileTodownload != undefined && $('downloadtype').value != ''){
      downloadFile(fileTodownload, fileTodownload);
    }
    else{
      popup.show({
              title : 'Message',
              message : ('<p style="text-align:center">Please select a component to download.</p>')
                  });
    }
  });
});

function setDownloadFile(selectedValue){
    fileTodownload = $('downloadtype').value;
}

function compare(a,b) {
  if (a.text < b.text)
    return -1;
  if (a.text > b.text)
    return 1;
  return 0;
}

function downloadFile(name, originalFilename) {

    // Created stream for file to be streamed to and buffer to save chunks
    var stream = ss.createStream();
    fileBuffer = [];
    fileLength = 0;
    ss(socket).emit('file_download', stream, name, function (fileError, fileInfo) {

        if (fileError) {
          showError(fileError);
        } else {
            console.log(['File Found!', fileInfo]);
            //== Receive data
            ShowProgressPopUp('Please wait ...', stream);
            stream.on('data', function (chunk) {
                fileLength += chunk.length;
                var progress = Math.floor((fileLength / fileInfo.size) * 100);
                progress = Math.max(progress - 2, 1);
                //deferred.notify(progress);
                updateProgress(progress);
                fileBuffer.push(chunk);
            });

            stream.on('end', function () {
                ShowDownloadComplete('Download completed.');
                var filedata = new Uint8Array(fileLength),
                i = 0;
                //== Loop to fill the final array
                fileBuffer.forEach(function (buff) {
                    for (var j = 0; j < buff.length; j++) {
                        filedata[i] = buff[j];
                        i++;
                    }
                });
                downloadFileFromBlob([filedata], originalFilename);
            });

            stream.on('error', function() {
              this.end();
              showError("Download error, Please try again later.");
            });
        }
    });
}

// Function to detect Microsoft family browsers.
function isIEorEDGE(){
  if (/MSIE 10/i.test(navigator.userAgent)) {
     // This is internet explorer 10
     return true;
  }

  if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
      // This is internet explorer 9 or 11
      return true;
  }

  if (/Edge\/\d./i.test(navigator.userAgent)){
     // This is Microsoft Edge
     return true;
  }
  return false;
}

var downloadFileFromBlob = (function () {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    return function (data, fileName) {
        var blob = new Blob(data, {
                type : "octet/stream"
            });
        if(isIEorEDGE()){
            if(window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(blob, $('downloadtype').value);
            }
        }
        else {
            url = window.URL.createObjectURL(blob);
            a.href = url;
            a.download = fileName;
            var clickEvent = new MouseEvent("click", {
              "view": window,
              "bubbles": true,
              "cancelable": false
            });
            a.dispatchEvent(clickEvent);
        }
    };
}());

// Progress bar code.........

function ShowProgressPopUp(title, stream) {
  popup.show({
                title : title,
                message : ('<p style="text-align:center">'
                    + '<div id="myProgress"><div id="myBar"></div></div></p>'
                    + '<p style="margin-top:5px" id="demo">0%</p>'),
                buttons : [ {
                      title : 'Cancel Download',
                      action : function() {
                          stream.destroy();
                          popup.hide();
                      }
                } ]
              });
}

function updateProgress(progress){
  document.getElementById('myBar').style.width = progress + "%";
  document.getElementById("demo").innerHTML = progress * 1  + '%';
}

function ShowDownloadComplete(title){
  popup.show({
                title : title,
                message : ('<p style="text-align:center">'
                    + '<div id="myProgress"><div id="myBar"></div></div></p>'
                    + '<p style="margin-top:5px" id="demo">100%</p>'),
                buttons : [ {
                      title : 'Close',
                      action : function() {
                        location.reload()
                      }
                } ]
              });
              document.getElementById('myBar').style.width = 100 + "%";
              document.getElementById("demo").innerHTML = 100 + '%';
}



function showError(message){
  popup.show({
        title : 'Download error.',
        message : '<p style="text-align:center">' + message + '</p>',
        buttons : [ {
          title : 'Dismiss',
          action : function() {
            location.reload();
            window.onbeforeunload = null;
          }
        } ]
      });
}

})();
