// dummy database
var hash = require('./pass').hash;
var fs = require('fs');
var contents;
var jsonContent;
var users;

function CalculateHashSync(iteration, callback){
  if(iteration < Object.keys(users).length){
    var val = users[Object.keys(users)[iteration]];
    hash(val.pass, function(err, salt, hash){
      if (err) throw err;
      // store the salt & hash in the "db"
      users[val.pass].salt = salt;
      users[val.pass].hash = hash.toString();
      CalculateHashSync( iteration + 1, callback);
    });
  }
  else{
    callback();
  }
}

exports.authenticate = function authenticate(password, fn) {
  contents = fs.readFileSync("./static_data_json/users.json");
  jsonContent = JSON.parse(contents);
  users = jsonContent;
  if (!module.parent) console.log('authenticating %s', password);
  CalculateHashSync(0, function(){
    var user = users[password];
    if (!user) return fn(new Error('cannot find user'));
    hash(password, user.salt, function(err, hash){
      if (err) return fn(err);
      if (hash.toString() == user.hash) return fn(null, user);
      fn(new Error('invalid password'));
    })
  });
}
