SUMMARY = "This is the web server."
DESCRIPTION = "This installs the web server."
SECTION = "web-server"
LICENSE = "CLOSED"

DEPENDS = "sqlite3"
RDEPENDS_${PN} = "\
    nodejs \
    bash \
    sqlite3"

SRC_URI = " \
           file://package.json \
           file://config.js \
           file://readme \
           file://server.js \
           file://web-server.c \
           file://start.js \
           file://socket_events.js \
           file://setntp.sh \
           file://settz.sh \
           file://lib/login.js \
           file://lib/pass.js \
           file://public/css/jquery-ui.css \
           file://public/css/jquery-ui-timepicker-addon.css \
           file://public/css/style.css \
           file://public/css/bootstrap-datetimepicker.min.css \
           file://public/css/metisMenu.min.css \
           file://public/css/sb-admin-2.css \
           file://public/css/sbadmin2-sidebar-toggle.css \
           file://public/css/images/animated-overlay.gif \
           file://public/css/images/ui-bg_flat_0_aaaaaa_40x100.png \
           file://public/css/images/ui-bg_flat_75_ffffff_40x100.png \
           file://public/css/images/ui-bg_glass_55_fbf9ee_1x400.png \
           file://public/css/images/ui-bg_glass_65_ffffff_1x400.png \
           file://public/css/images/ui-bg_glass_75_dadada_1x400.png \
           file://public/css/images/ui-bg_glass_75_e6e6e6_1x400.png \
           file://public/css/images/ui-bg_glass_95_fef1ec_1x400.png \
           file://public/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png \
           file://public/css/images/ui-icons_222222_256x240.png \
           file://public/css/images/ui-icons_2e83ff_256x240.png \
           file://public/css/images/ui-icons_454545_256x240.png \
           file://public/css/images/ui-icons_888888_256x240.png \
           file://public/css/images/ui-icons_cd0a0a_256x240.png \
           file://public/css/vendor/bootstrap/js/bootstrap.min.js \
           file://public/css/vendor/bootstrap/js/bootstrap.js \
           file://public/css/vendor/bootstrap/css/bootstrap.min.css \
           file://public/css/vendor/bootstrap/css/bootstrap.css \
           file://public/css/vendor/bootstrap/css/bootstrap-datetimepicker.min.css \
           file://public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.eot \
           file://public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff \
           file://public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.svg \
           file://public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff2 \
           file://public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.ttf \
           file://public/css/vendor/font-awesome/css/font-awesome.min.css \
           file://public/css/vendor/font-awesome/css/font-awesome.css.map \
           file://public/css/vendor/font-awesome/css/font-awesome.css \
           file://public/css/vendor/font-awesome/less/animated.less \
           file://public/css/vendor/font-awesome/less/icons.less \
           file://public/css/vendor/font-awesome/less/screen-reader.less \
           file://public/css/vendor/font-awesome/less/bordered-pulled.less \
           file://public/css/vendor/font-awesome/less/larger.less \
           file://public/css/vendor/font-awesome/less/spinning.less \
           file://public/css/vendor/font-awesome/less/core.less \
           file://public/css/vendor/font-awesome/less/list.less \
           file://public/css/vendor/font-awesome/less/stacked.less \
           file://public/css/vendor/font-awesome/less/extras.less \
           file://public/css/vendor/font-awesome/less/mixins.less \
           file://public/css/vendor/font-awesome/less/variables.less \
           file://public/css/vendor/font-awesome/less/fixed-width.less \
           file://public/css/vendor/font-awesome/less/path.less \
           file://public/css/vendor/font-awesome/less/font-awesome.less \
           file://public/css/vendor/font-awesome/less/rotated-flipped.less \
           file://public/css/vendor/font-awesome/fonts/fontawesome-webfont.eot \
           file://public/css/vendor/font-awesome/fonts/FontAwesome.otf \
           file://public/css/vendor/font-awesome/fonts/fontawesome-webfont.svg \
           file://public/css/vendor/font-awesome/fonts/fontawesome-webfont.woff2 \
           file://public/css/vendor/font-awesome/fonts/fontawesome-webfont.woff \
           file://public/css/vendor/font-awesome/fonts/fontawesome-webfont.ttf \
           file://public/css/vendor/font-awesome/scss/_stacked.scss \
           file://public/css/vendor/font-awesome/scss/_spinning.scss \
           file://public/css/vendor/font-awesome/scss/_screen-reader.scss \
           file://public/css/vendor/font-awesome/scss/_rotated-flipped.scss \
           file://public/css/vendor/font-awesome/scss/_path.scss \
           file://public/css/vendor/font-awesome/scss/_mixins.scss \
           file://public/css/vendor/font-awesome/scss/_list.scss \
           file://public/css/vendor/font-awesome/scss/_larger.scss \
           file://public/css/vendor/font-awesome/scss/_icons.scss \
           file://public/css/vendor/font-awesome/scss/_fixed-width.scss \
           file://public/css/vendor/font-awesome/scss/_extras.scss \
           file://public/css/vendor/font-awesome/scss/_core.scss \
           file://public/css/vendor/font-awesome/scss/_bordered-pulled.scss \
           file://public/css/vendor/font-awesome/scss/_animated.scss \
           file://public/css/vendor/font-awesome/scss/_variables.scss \
           file://public/css/vendor/font-awesome/scss/font-awesome.scss \
           file://public/img/IECC_Footer.jpg \
           file://public/img/accept.png \
           file://public/img/ajax-load.gif \
           file://public/img/Ansaldo_STS_Htachi_neg.jpg \
           file://public/img/arrowinfo.png \
           file://public/img/arrow.png \
           file://public/img/arrowup.png \
           file://public/img/bg.png \
           file://public/img/error.png \
           file://public/img/exclamation.png \
           file://public/img/information.png \
           file://public/img/lightbg.gif \
           file://public/img/subsection-bg.png \
           file://public/img/top_bg.gif \
           file://public/img/top_bg.png \
           file://public/img/Warning.png \
	   file://public/img/warning.png \
           file://public/img/bitandvars/highlevel_go.png \
           file://public/img/bitandvars/highlevel_off.png \
           file://public/img/bitandvars/highlevel_on.png \
           file://public/img/bitandvars/highlevel_up.png \
           file://public/img/bitandvars/lowlevel_down.png \
           file://public/img/bitandvars/lowlevel_go.png \
           file://public/img/bitandvars/lowlevel_off.png \
           file://public/img/bitandvars/lowlevel_on.png \
           file://public/img/bitandvars/midlevel_go.png \
           file://public/img/bitandvars/midlevel_off.png \
           file://public/img/bitandvars/midlevel_on.png \
           file://public/img/bitandvars/signal_off.png \
           file://public/img/bitandvars/timeline.png \
           file://public/js/application_download.js \
           file://public/js/bitandvars.js \ 
           file://public/js/common_function.js \
           file://public/js/conf_boards.js \
	   file://public/js/conf_download.js \
	   file://public/js/conf_general.js \
	   file://public/js/config.js \
	   file://public/js/conf_links.js \
           file://public/js/conf_route.js \
           file://public/js/conf_snmp.js \
	   file://public/js/conf_timers.js \
	   file://public/js/conf_upload.js \
	   file://public/js/conf_userdatalog.js \
	   file://public/js/errorlog.js \
		   file://public/js/eventlog.js \
		   file://public/js/filexhr.js \
		   file://public/js/header_info.js \
		   file://public/js/init.js \
		   file://public/js/linkinfo.js \
		   file://public/js/popup.js \
		   file://public/js/session.js \
		   file://public/js/sessionlimit.js \
           file://public/js/socket.io-stream.js \
           file://public/js/submit_data.js \
		   file://public/js/sysinfo.js \
		   file://public/js/systime.js \
	 	   file://public/js/unload.js \
	 	   file://public/js/lib/bootstrap-datetimepicker.min.js \
		   file://public/js/lib/dd_belatedpng_0.0.8a-min.js \
           file://public/js/lib/effects.js \	   
           file://public/js/lib/ie6-png-fix.js \
           file://public/js/lib/jqnoconflict.js \
           file://public/js/lib/jquery-1.11.1.min.js \
           file://public/js/lib/jquery2.min.js \
           file://public/js/lib/jquery.min.js \
           file://public/js/lib/jquery-ui.min.js \
		   file://public/js/lib/jquery-ui-sliderAccess.js \
		   file://public/js/lib/jquery-ui-timepicker-addon-i18n.min.js \
		   file://public/js/lib/jquery-ui-timepicker-addon.js \
           file://public/js/lib/jszip.js \
           file://public/js/lib/metisMenu.min.js \
           file://public/js/lib/modernizr-1.7.min.js \
           file://public/js/lib/moment.min.js \
           file://public/js/lib/prototype.js \
           file://public/js/lib/sb-admin-2.js \
           file://public/js/lib/vendor/bootstrap/css/bootstrap.min.css \
           file://public/js/lib/vendor/bootstrap/css/bootstrap.css \
           file://public/js/lib/vendor/bootstrap/js/bootstrap.min.js \
           file://public/js/lib/vendor/bootstrap/js/bootstrap.js \
           file://public/js/lib/vendor/bootstrap/js/bootstrap-datetimepicker.min.js \
           file://public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff2 \
           file://public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff \
           file://public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.ttf \
           file://public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.svg \
           file://public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.eot \
           file://public/js/lib/vendor/font-awesome/css/font-awesome.min.css \
           file://public/js/lib/vendor/font-awesome/css/font-awesome.css.map \
           file://public/js/lib/vendor/font-awesome/css/font-awesome.css \
           file://public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.eot \
           file://public/js/lib/vendor/font-awesome/fonts/FontAwesome.otf \
           file://public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.woff2 \
           file://public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.woff \
           file://public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.ttf \
           file://public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.svg \
           file://public/js/lib/vendor/font-awesome/scss/_variables.scss \
           file://public/js/lib/vendor/font-awesome/scss/_stacked.scss \
           file://public/js/lib/vendor/font-awesome/scss/_spinning.scss \
           file://public/js/lib/vendor/font-awesome/scss/_screen-reader.scss \
           file://public/js/lib/vendor/font-awesome/scss/_rotated-flipped.scss \
           file://public/js/lib/vendor/font-awesome/scss/_path.scss \
           file://public/js/lib/vendor/font-awesome/scss/_mixins.scss \
           file://public/js/lib/vendor/font-awesome/scss/_list.scss \
           file://public/js/lib/vendor/font-awesome/scss/_larger.scss \
           file://public/js/lib/vendor/font-awesome/scss/_icons.scss \
           file://public/js/lib/vendor/font-awesome/scss/font-awesome.scss \
           file://public/js/lib/vendor/font-awesome/scss/_fixed-width.scss \
           file://public/js/lib/vendor/font-awesome/scss/_extras.scss \
           file://public/js/lib/vendor/font-awesome/scss/_core.scss \
           file://public/js/lib/vendor/font-awesome/scss/_bordered-pulled.scss \
           file://public/js/lib/vendor/font-awesome/scss/_animated.scss \
           file://public/js/lib/vendor/font-awesome/less/larger.less \
           file://public/js/lib/vendor/font-awesome/less/icons.less \
           file://public/js/lib/vendor/font-awesome/less/font-awesome.less \
           file://public/js/lib/vendor/font-awesome/less/fixed-width.less\
           file://public/js/lib/vendor/font-awesome/less/extras.less \
           file://public/js/lib/vendor/font-awesome/less/core.less \
           file://public/js/lib/vendor/font-awesome/less/bordered-pulled.less \
           file://public/js/lib/vendor/font-awesome/less/animated.less \
           file://public/js/lib/vendor/font-awesome/less/variables.less \
           file://public/js/lib/vendor/font-awesome/less/stacked.less \
           file://public/js/lib/vendor/font-awesome/less/spinning.less \
           file://public/js/lib/vendor/font-awesome/less/screen-reader.less \
           file://public/js/lib/vendor/font-awesome/less/rotated-flipped.less \
           file://public/js/lib/vendor/font-awesome/less/path.less \
           file://public/js/lib/vendor/font-awesome/less/mixins.less \
           file://public/js/lib/vendor/font-awesome/less/list.less \
		   file://routes/routes.js \
		   file://utils/logger.js \
		   file://utils/db_operations.js \
		   file://views/application_download.ejs \      
		   file://views/bitandvars.ejs \
		   file://views/block.html \
           file://views/conf_boards.ejs \
           file://views/conf_download.ejs \
           file://views/conf_general.ejs \
           file://views/conf_header.ejs \
           file://views/conf_links.ejs \
           file://views/conf_route.ejs \
           file://views/conf_timers.ejs \
           file://views/conf_upload.ejs \
           file://views/conf_userdatalog.ejs \
           file://views/conf_variables.ejs \
           file://views/errorlog.ejs \
           file://views/eventlog.ejs \
           file://views/footer.ejs \
           file://views/header.ejs \
           file://views/index.ejs \
           file://views/linkinfo.ejs \
           file://views/memorydump.ejs \
           file://views/messagemon.ejs \
           file://views/submit_data.ejs \
           file://views/systemlog.ejs \
           file://views/systime.ejs \
           file://views/userdatalog.ejs \
           file://node_modules.xx \
      	   file://database/microlok_eventlog_database.db \
      	   file://system_bash/network_routing.sh \
           file://init_script/web-server \
	       file://static_data_json/bitstore.json \
	       file://static_data_json/general_config_list.json \
           file://static_data_json/groupbits.json \
      	   file://static_data_json/system_information.json \
	       file://static_data_json/timer_config_list.json \
      	   file://static_data_json/users.json \
      	   file://static_data_json/ViProConfig.json \
      	   file://certificates/key.pem	\
      	   file://certificates/cert.pem \
           file://tmp/swversion \
           "
	   
S = "${WORKDIR}"

inherit update-rc.d
INITSCRIPT_NAME = "web-server"
INITSCRIPT_PARAMS = "defaults 69"

do_compile () {
    ${CC} web-server.c -o web-server
}

do_install () {
    INSTDIR="/home/www/";
    #create directories
    install -d ${D}${INSTDIR}
    install -d ${D}${INSTDIR}/public/css
    install -d ${D}${INSTDIR}/public/css/images
    install -d ${D}${INSTDIR}/public/css/vendor
    install -d ${D}${INSTDIR}/public/css/vendor/bootstrap
    install -d ${D}${INSTDIR}/public/css/vendor/bootstrap/css
    install -d ${D}${INSTDIR}/public/css/vendor/bootstrap/fonts
    install -d ${D}${INSTDIR}/public/css/vendor/bootstrap/js
    install -d ${D}${INSTDIR}/public/css/vendor/font-awesome
    install -d ${D}${INSTDIR}/public/css/vendor/font-awesome/css
    install -d ${D}${INSTDIR}/public/css/vendor/font-awesome/fonts
    install -d ${D}${INSTDIR}/public/css/vendor/font-awesome/less
    install -d ${D}${INSTDIR}/public/css/vendor/font-awesome/scss
    install -d ${D}${INSTDIR}/public/img
    install -d ${D}${INSTDIR}/public/img/bitandvars
    install -d ${D}${INSTDIR}/public/js
    install -d ${D}${INSTDIR}/public/js/lib
    install -d ${D}${INSTDIR}/public/js/lib/vendor
    install -d ${D}${INSTDIR}/public/js/lib/vendor/bootstrap
    install -d ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/css
    install -d ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/fonts
    install -d ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/js
    install -d ${D}${INSTDIR}/public/js/lib/vendor/font-awesome
    install -d ${D}${INSTDIR}/public/js/lib/vendor/font-awesome/css
    install -d ${D}${INSTDIR}/public/js/lib/vendor/font-awesome/fonts
    install -d ${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less
    install -d ${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss
    install -d ${D}${INSTDIR}/routes
    install -d ${D}${INSTDIR}/lib
    install -d ${D}${INSTDIR}/utils
    install -d ${D}${INSTDIR}/views
    install -d ${D}${INSTDIR}/system_bash
    install -d ${D}${INSTDIR}/static_data_json
    install -d ${D}${INSTDIR}/certificates
    install -d ${D}${INSTDIR}/database
    install -d ${D}${INSTDIR}/tmp
    install -d ${D}${sysconfdir}/init.d
    install -d ${D}${sysconfdir}/snmp
    install -d ${D}${bindir}

    #install files
    install -m 755 ${WORKDIR}/config.js                 ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/node_modules.xx		${D}${INSTDIR}
    install -m 755 ${WORKDIR}/package.json              ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/readme                    ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/server.js                 ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/socket_events.js          ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/start.js                  ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/setntp.sh                 ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/settz.sh                  ${D}${INSTDIR}
    install -m 755 ${WORKDIR}/web-server                ${D}${bindir}

    #install lib
    install -m 755 ${WORKDIR}/lib/login.js       ${D}${INSTDIR}/lib/
    install -m 755 ${WORKDIR}/lib/pass.js        ${D}${INSTDIR}/lib/

    #install css
    install -m 755 ${WORKDIR}/public/css/jquery-ui.css                                     ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/jquery-ui-timepicker-addon.css                    ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/bootstrap-datetimepicker.min.css                  ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/style.css                                         ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/metisMenu.min.css                                 ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/sb-admin-2.css                                    ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/sbadmin2-sidebar-toggle.css                       ${D}${INSTDIR}/public/css/
    install -m 755 ${WORKDIR}/public/css/images/animated-overlay.gif                       ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_flat_0_aaaaaa_40x100.png             ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_flat_75_ffffff_40x100.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_55_fbf9ee_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_65_ffffff_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_75_dadada_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_75_e6e6e6_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_glass_95_fef1ec_1x400.png            ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-bg_highlight-soft_75_cccccc_1x100.png   ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_222222_256x240.png                ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_2e83ff_256x240.png                ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_454545_256x240.png                ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_888888_256x240.png                ${D}${INSTDIR}/public/css/images/
    install -m 755 ${WORKDIR}/public/css/images/ui-icons_cd0a0a_256x240.png                ${D}${INSTDIR}/public/css/images/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/css/bootstrap.css    			   ${D}${INSTDIR}/public/css/vendor/bootstrap/css/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/css/bootstrap.min.css 		   ${D}${INSTDIR}/public/css/vendor/bootstrap/css/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/css/bootstrap-datetimepicker.min.css		    ${D}${INSTDIR}/public/css/vendor/bootstrap/css/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.svg         ${D}${INSTDIR}/public/css/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.eot         ${D}${INSTDIR}/public/css/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff2       ${D}${INSTDIR}/public/css/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff        ${D}${INSTDIR}/public/css/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/fonts/glyphicons-halflings-regular.ttf         ${D}${INSTDIR}/public/css/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/js/bootstrap.min.js        ${D}${INSTDIR}/public/css/vendor/bootstrap/js/
	install -m 755 ${WORKDIR}/public/css/vendor/bootstrap/js/bootstrap.js            ${D}${INSTDIR}/public/css/vendor/bootstrap/js/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/css/font-awesome.min.css       ${D}${INSTDIR}/public/css/vendor/font-awesome/css/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/css/font-awesome.css.map       ${D}${INSTDIR}/public/css/vendor/font-awesome/css/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/css/font-awesome.css           ${D}${INSTDIR}/public/css/vendor/font-awesome/css/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/variables.less           ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/stacked.less           ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/spinning.less           ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/screen-reader.less           ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/rotated-flipped.less          ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/path.less          	${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/mixins.less         	${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/list.less           	${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/larger.less         	${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/icons.less          	${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/font-awesome.less     ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/fixed-width.less      ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/extras.less           ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/core.less       		${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/bordered-pulled.less           ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/less/animated.less          ${D}${INSTDIR}/public/css/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/fonts/fontawesome-webfont.eot           ${D}${INSTDIR}/public/css/vendor/font-awesome/fonts/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/fonts/FontAwesome.otf           		 ${D}${INSTDIR}/public/css/vendor/font-awesome/fonts/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/fonts/fontawesome-webfont.svg           ${D}${INSTDIR}/public/css/vendor/font-awesome/fonts/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/fonts/fontawesome-webfont.woff2         ${D}${INSTDIR}/public/css/vendor/font-awesome/fonts/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/fonts/fontawesome-webfont.woff          ${D}${INSTDIR}/public/css/vendor/font-awesome/fonts/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/fonts/fontawesome-webfont.ttf           ${D}${INSTDIR}/public/css/vendor/font-awesome/fonts/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_stacked.scss          ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_spinning.scss         ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_screen-reader.scss    ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_rotated-flipped.scss  ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_path.scss             ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_mixins.scss           ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_list.scss             ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_larger.scss           ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_icons.scss            ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_fixed-width.scss      ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_extras.scss           ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_core.scss             ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_bordered-pulled.scss  ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_animated.scss         ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/_variables.scss        ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
    install -m 755 ${WORKDIR}/public/css/vendor/font-awesome/scss/font-awesome.scss      ${D}${INSTDIR}/public/css/vendor/font-awesome/scss/
   
    
    #install img files
    install -m 755 ${WORKDIR}/public/img/accept.png                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/ajax-load.gif                  ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/Ansaldo_STS_Htachi_neg.jpg     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/arrowinfo.png                  ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/arrow.png                      ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/arrowup.png                    ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/bg.png                         ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/error.png                      ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/exclamation.png                ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/information.png                ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/lightbg.gif                    ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/subsection-bg.png              ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/top_bg.gif                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/top_bg.png                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/Warning.png                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/warning.png                     ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/IECC_Footer.jpg                 ${D}${INSTDIR}/public/img/
    install -m 755 ${WORKDIR}/public/img/bitandvars/highlevel_go.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/highlevel_off.png   ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/highlevel_on.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/highlevel_up.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/lowlevel_down.png   ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/lowlevel_go.png     ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/lowlevel_off.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/lowlevel_on.png     ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/midlevel_go.png     ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/midlevel_off.png    ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/midlevel_on.png     ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/signal_off.png      ${D}${INSTDIR}/public/img/bitandvars/
    install -m 755 ${WORKDIR}/public/img/bitandvars/timeline.png        ${D}${INSTDIR}/public/img/bitandvars/

    #install js files
	install -m 755 ${WORKDIR}/public/js/application_download.js                      ${D}${INSTDIR}/public/js/  
    install -m 755 ${WORKDIR}/public/js/bitandvars.js                                ${D}${INSTDIR}/public/js/    
    install -m 755 ${WORKDIR}/public/js/common_function.js                           ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_boards.js                               ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_download.js                             ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_general.js                              ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/config.js                          	         ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_links.js                                ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_route.js                                ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_snmp.js                                 ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_timers.js                               ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_upload.js                               ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/conf_userdatalog.js                          ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/errorlog.js                          	     ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/eventlog.js                          	     ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/filexhr.js                                   ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/header_info.js                               ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/init.js                                      ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/linkinfo.js									 ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/popup.js                                     ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/session.js                                   ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/sessionlimit.js                              ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/socket.io-stream.js                          ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/submit_data.js								 ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/sysinfo.js                                   ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/systime.js									 ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/unload.js                                    ${D}${INSTDIR}/public/js/
    install -m 755 ${WORKDIR}/public/js/lib/bootstrap-datetimepicker.min.js          ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/dd_belatedpng_0.0.8a-min.js              ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/effects.js                               ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/ie6-png-fix.js                           ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jqnoconflict.js                          ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-1.11.1.min.js                     ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery2.min.js						     ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery.min.js                            ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-ui.min.js                         ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-ui-sliderAccess.js                ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-ui-timepicker-addon-i18n.min.js   ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jquery-ui-timepicker-addon.js            ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/jszip.js                                 ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/metisMenu.min.js						 ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/modernizr-1.7.min.js                     ${D}${INSTDIR}/public/js/lib/
	install -m 755 ${WORKDIR}/public/js/lib/moment.min.js							 ${D}${INSTDIR}/public/js/lib/
    install -m 755 ${WORKDIR}/public/js/lib/prototype.js                             ${D}${INSTDIR}/public/js/lib/
	install -m 755 ${WORKDIR}/public/js/lib/sb-admin-2.js							 ${D}${INSTDIR}/public/js/lib/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff2   ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.woff    ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.ttf	    ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.svg	    ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/fonts/glyphicons-halflings-regular.eot	    ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/css/bootstrap.min.css				${D}${INSTDIR}/public/js/lib/vendor/bootstrap/css/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/css/bootstrap.css				    ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/css/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/js/bootstrap.min.js				${D}${INSTDIR}/public/js/lib/vendor/bootstrap/js/	    
	install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/js/bootstrap.js				    ${D}${INSTDIR}/public/js/lib/vendor/bootstrap/js/	    
	install -m 755 ${WORKDIR}/public/js/lib/vendor/bootstrap/js/bootstrap-datetimepicker.min.js	${D}${INSTDIR}/public/js/lib/vendor/bootstrap/js/	    
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/css/font-awesome.css	    ${D}${INSTDIR}/public/js/lib/vendor/font-awesome/css/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/css/font-awesome.css.map	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/css/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/css/font-awesome.min.css	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/css/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.eot	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/fonts/FontAwesome.otf		    ${D}${INSTDIR}/public/js/lib/vendor/font-awesome/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.woff2	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.woff	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.ttf	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/fonts/fontawesome-webfont.svg	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/fonts/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_variables.scss	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_stacked.scss		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_spinning.scss		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_screen-reader.scss	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_rotated-flipped.scss	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_path.scss			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_mixins.scss		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_list.scss			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_larger.scss		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_icons.scss		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/font-awesome.scss	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_fixed-width.scss	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_extras.scss		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_core.scss			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_bordered-pulled.scss	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/
	install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/scss/_animated.scss			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/scss/    
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/larger.less			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/icons.less				${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/font-awesome.less		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/fixed-width.less		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/extras.less			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/core.less				${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/bordered-pulled.less	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/animated.less			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/variables.less			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/stacked.less			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/spinning.less			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/screen-reader.less		${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/rotated-flipped.less	${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/path.less				${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/mixins.less			${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    install -m 755 ${WORKDIR}/public/js/lib/vendor/font-awesome/less/list.less				${D}${INSTDIR}/public/js/lib/vendor/font-awesome/less/
    
    #install routes
    install -m 755 ${WORKDIR}/routes/routes.js ${D}${INSTDIR}/routes/

    #install utils
    install -m 755 ${WORKDIR}/utils/logger.js ${D}${INSTDIR}/utils/
    install -m 755 ${WORKDIR}/utils/db_operations.js ${D}${INSTDIR}/utils/

    #install certificates
    install -m 755 ${WORKDIR}/certificates/key.pem ${D}${INSTDIR}/certificates/
    install -m 755 ${WORKDIR}/certificates/cert.pem ${D}${INSTDIR}/certificates/

    #install static_data_json

    install -m 755 ${WORKDIR}/static_data_json/bitstore.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/general_config_list.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/groupbits.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/system_information.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/timer_config_list.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/users.json ${D}${INSTDIR}/static_data_json/
    install -m 755 ${WORKDIR}/static_data_json/ViProConfig.json ${D}${INSTDIR}/static_data_json/

    #install tmp location for swversion
    install -m 755 ${WORKDIR}/tmp/swversion ${D}${INSTDIR}/tmp

    #install system_bash
    install -m 755 ${WORKDIR}/system_bash/network_routing.sh ${D}${INSTDIR}/system_bash/

    #install views
	install -m 755 ${WORKDIR}/views/application_download.ejs    ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/bitandvars.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/block.html             ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_boards.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_download.ejs      ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_general.ejs       ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_header.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_links.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_route.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_timers.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_upload.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_userdatalog.ejs   ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/conf_variables.ejs     ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/errorlog.ejs           ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/eventlog.ejs           ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/footer.ejs             ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/header.ejs             ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/index.ejs              ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/linkinfo.ejs           ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/memorydump.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/messagemon.ejs         ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/submit_data.ejs        ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/systemlog.ejs		   ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/systime.ejs            ${D}${INSTDIR}/views/
    install -m 755 ${WORKDIR}/views/userdatalog.ejs        ${D}${INSTDIR}/views/


    #install database modules
    install -m 755 ${WORKDIR}/database/microlok_eventlog_database.db ${D}${INSTDIR}/database/

    #install node_modules files
    install -m 755 ${WORKDIR}/node_modules.xx ${D}${INSTDIR}


    #install startup script
    install -m 755 ${WORKDIR}/init_script/web-server ${D}${sysconfdir}/init.d

}
FILES_${PN} = "/"

PACKAGE_ARCH = "${MACHINE_ARCH}"
